var matrizVigenere = new Array(26);
var cancelado = false;
var velocidad = 1;
var azul = 0, negro = 1, amarillo = 2 ; verde = 3;

$.fn.scrollView = function () {
  return this.each(function () {
    $('html, body').animate({
      scrollTop: $(this).offset().top
    }, 1000);
  });
}

function mostrarPanelVigenere(){
	$("#pnl-Interactivo2").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelVigenere(){
	$("#pnl-Interactivo2").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);

	limpiaPanelCifradoVigenere();
	limpiaPanelDescifradoVigenere();
}

function crearTablaTritemioC(){
	var criptograma_length = $('#in-textoPlanoVigenere').val().length;

	for (var i = 0 ; i < 26 ; i++) {
		matrizVigenere[i] = new Array(26);

		for(var j = i; j < i + 26 ; j++){
			$("#VIrow2-"+i).append('<td id="VIcell2-'+i+'-'+ (j-i) +'">'+String.fromCharCode((j%26)+65)+'</td>');
			matrizVigenere[i][j-i] = String.fromCharCode((j%26)+65);
		}
	}

	for(var i = 0 ; i < criptograma_length ; i++){
		$('#textoCifradoVigenere').append('<label class="circulo" id="VI-Ccell1'+i+'"></label>');
	}

	$("#table-Vigenere1-2").css("text-align","center");
}

function crearTablaTritemioD(){
	var mensaje_claro_length = $('#in-textoPlanoCifradoVigenere').val().length;

	for (var i = 0 ; i < 26 ; i++) {
		matrizVigenere[i] = new Array(26);

		for(var j = i; j < i + 26 ; j++){
			$("#VIrow22-"+i).append('<td id="VIcell22-'+i+'-'+ (j-i) +'">'+String.fromCharCode((j%26)+65)+'</td>');
			matrizVigenere[i][j-i] = String.fromCharCode((j%26)+65);
		}
	}

	for(var i = 0 ; i < mensaje_claro_length ; i++){
		$('#textoDescifradoVigenere').append('<label class="circulo" id="VI-MCcell1'+i+'"></label>');
	}

	$("#table-Vigenere2-2").css("text-align","center");
}

function limpiaPanelCifradoVigenere(){
	for(var i = 0 ; i < 26 ; i++){
		$("#VIrow2-"+i).empty();
	}

	$("#VIrow1-0").empty();
	$("#VIrow1-1").empty();
	$("#VIrowblank").empty();
	$("#in-textoPlanoVigenere").val("");
	$("#in-claveCifradoVigenere").val("");
	$("#out-textoCifradoVigenere").val("");
	$("#textoCifradoVigenere").html("");

	if($('#VIdiv1').is(':visible')) {
		$("#VIdiv1").slideToggle(1000);
	}
}

function limpiaPanelDescifradoVigenere(){
	for(var i = 0 ; i < 26 ; i++){
		$("#VIrow22-"+i).empty();
	}

	$("#VIrow21-0").empty();
	$("#VIrow21-1").empty();
	$("#VIrowblank2").empty();
	$("#in-textoPlanoCifradoVigenere").val("");
	$("#in-claveDescifradoVigenere").val("");
	$("#out-textoDescifradoVigenere").val("");
	$("#textoDescifradoVigenere").html("");

	if($('#VIdiv2').is(':visible')) {
		$("#VIdiv2").slideToggle(1000);
	}
}

async function cifrarVigenere(){
	var plano = ($("#in-textoPlanoVigenere").val().toLowerCase().replace(/ /g, "")).split("");
    var clave = ($("#in-claveCifradoVigenere").val().toLowerCase()).split("");
    var texto_length = plano.length;
    var cadenaCifrado = "";
    
    limpiaPanelCifradoVigenere();
    $("#in-textoPlanoVigenere").val(plano.join(""));
    $("#in-claveCifradoVigenere").val(clave.join(""));

	$('#VIdiv1').html('A cada carácter del texto plano se le hace coincidir con un carácter de la llave, si ésta es más corta que el mensaje en claro se repite las veces que sea necesario.');
	$('#VIdiv1').slideToggle(1000);

	$('#btn-cancelarCifrarVigenere').scrollView();

	if(cancelado){
		return;
	}

	await sleep(7000);

	if(cancelado){
		return;
	}

	$("#table-Vigenere1-1").css("text-align","center");
	$("#VIrow1-0").append('<td>Texto Plano</td>');
	$("#VIrow1-1").append('<td>Clave</td>');
	$("#VIrowblank").append('<td></td>');

	for (var i = 0 ; i < texto_length && !cancelado; i++) {
		$("#VIrow1-0").append('<td id="VIcell1-0-'+i+'"></td>');
		$("#VIrow1-1").append('<td id="VIcell1-1-'+i+'"></td>');
		$("#VIrowblank").append('<td></td>');
	}

	if(cancelado){
		return;
	}

	await sleep(1000);

	if(cancelado){
		return;
	}

	$('#btn-cancelarCifrarVigenere').scrollView();

    for(var i = 0 ; i < plano.length && !cancelado ; i++){
    	$('#VIcell1-0-'+i).html(plano[i]);

    	parpadeo("#VIcell1-0-"+i, 0.5, azul);
		await sleep(500);
		removeparpadeo("#VIcell1-0-"+i, 0.5, azul);
    }

    for(var i = 0 ; i < plano.length && !cancelado ; i++){
    	$('#VIcell1-1-'+i).html(clave[i%clave.length]);

    	parpadeo('#VIcell1-1-'+i, 0.5, azul);
    	await sleep(500);
		removeparpadeo('#VIcell1-1-'+i, 0.5, azul);
    }

    if(cancelado){
		return;
	}

    await sleep(1000);

    if(cancelado){
		return;
	}

    //Cifrar
    $('#VIdiv1').slideToggle(1000);
    
    await sleep(1000);

    if(cancelado){
		return;
	}
    
    $('#VIdiv1').html('Se hace uso del cuadrado de Vigenère. La primer fila de la matriz corresponde a los caracteres de la clave y la primera columna a los caracteres del mensaje claro.');
	$('#VIdiv1').slideToggle(1000);

	if(cancelado){
		return;
	}

	await sleep(8000);

	if(cancelado){
		return;
	}
	
	crearTablaTritemioC();

	if(cancelado){
		return;
	}

	$('#VIdiv1').slideToggle(1000);
	
	await sleep(1000);

    $('#VIdiv1').html('El criptograma es aquel carácter que resulte de la intersección de la fila y la columna de donde se encuentren los caracteres de la clave y el texto plano respectivamente.');
	$('#VIdiv1').slideToggle(1000);

	if(cancelado){
		return;
	}

	await sleep(8000);

	if(cancelado){
		return;
	}

	$('#VIdiv1').scrollView();

    for(var i = 0 ; i < plano.length && !cancelado ; i++){
    	parpadeo('#VIcell1-1-'+i, 3*velocidad, azul); //Clave

    	for(var j = 0 ; j < 26 && !cancelado ; j++){
    		parpadeo('#VIcell2-'+j+"-"+(clave[i%clave.length].charCodeAt()-97), 3*velocidad, azul);
    	}
    	await sleep(1000*velocidad);

    	parpadeo('#VIcell1-0-'+i, 2*velocidad, amarillo); //Texto

    	for(var j = 0 ; j < 26 && !cancelado ; j++){
    		parpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+'-'+j, 2*velocidad, amarillo);
    	}

    	await sleep(1000*velocidad);

    	removeparpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+"-"+(clave[i%clave.length].charCodeAt()-97), 2*velocidad, amarillo);
    	removeparpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+"-"+(clave[i%clave.length].charCodeAt()-97), 3*velocidad, azul);
    	parpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+"-"+(clave[i%clave.length].charCodeAt()-97), 1*velocidad, verde);
    	parpadeo("#VI-Ccell1"+i, 1*velocidad, negro);
	    $("#VI-Ccell1"+i).html(matrizVigenere[plano[i].charCodeAt()-97][clave[i%clave.length].charCodeAt()-97]);
    	
    	cadenaCifrado = cadenaCifrado + matrizVigenere[plano[i].charCodeAt()-97][clave[i%clave.length].charCodeAt()-97];
	   
	    await sleep(1000*velocidad);

	    for(var j = 0 ; j < 26 && !cancelado ; j++){
    		removeparpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+'-'+j, 2*velocidad, amarillo);
    	}
    	
    	for(var j = 0 ; j < 26 && !cancelado ; j++){
    		removeparpadeo('#VIcell2-'+j+"-"+(clave[i%clave.length].charCodeAt()-97), 3*velocidad, azul);
    	}

    	removeparpadeo('#VIcell2-'+(plano[i].charCodeAt()-97)+"-"+(clave[i%clave.length].charCodeAt()-97), 1*velocidad, verde);
    	removeparpadeo('#VIcell1-1-'+i, 3*velocidad, azul); //Clave
    	removeparpadeo('#VIcell1-0-'+i, 2*velocidad, amarillo); //Texto
    	removeparpadeo("#VI-Ccell1"+i, 1*velocidad, negro);

    	await sleep(100);
    }

    if(cancelado){
		return;
	}

	$("#out-textoCifradoVigenere").val(cadenaCifrado);
	$("#btn-velocidadCVigenere").show();
    $("#btn-cifrarVigenere").show();
	$("#btn-cancelarCifrarVigenere").hide();

	if(!cancelado){
		$('#VIdiv1').slideToggle(1000);
		toastr.options.timeOut = "1000";
    	toastr['success']('Texto cifrado');
    }
}

async function descifrarVigenere(){
	var cifrado = ($("#in-textoPlanoCifradoVigenere").val().toUpperCase()).split("");
    var clave = ($("#in-claveDescifradoVigenere").val().toUpperCase()).split("");
    var texto_length = cifrado.length;
    var cadenaDescifrado = "";
    
    limpiaPanelDescifradoVigenere();
    $("#in-textoPlanoCifradoVigenere").val(cifrado.join(""));
    $("#in-claveDescifradoVigenere").val(clave.join(""));

    $('#VIdiv2').html('A cada carácter del criptograma se le hace coincidir con un carácter de la llave, si ésta es más corta que el mensaje en claro se repite las veces que sea necesario.');
	$('#VIdiv2').slideToggle(1000);

	if(cancelado){
		return;
	}

	$('#btn-cancelarDescifrarVigenere').scrollView();

	await sleep(7000);
	
	if(cancelado){
		return;
	}

	$("#table-Vigenere2-1").css("text-align","center");
	$("#VIrow21-0").append('<td>Criptograma</td>');
	$("#VIrow21-1").append('<td>Clave</td>');
	$("#VIrowblank2").append('<td></td>');

	for (var i = 0 ; i < texto_length ; i++) {
		$("#VIrow21-0").append('<td id="VIcell21-0-'+i+'"></td>');
		$("#VIrow21-1").append('<td id="VIcell21-1-'+i+'"></td>');
		$("#VIrowblank2").append('<td></td>');
	}
    
    if(cancelado){
		return;
	}

	$('#btn-cancelarDescifrarVigenere').scrollView();

    for(var i = 0 ; i < cifrado.length ; i++){
    	$('#VIcell21-0-'+i).html(cifrado[i]);

    	parpadeo("#VIcell21-0-"+i, 0.5, azul);
		await sleep(500);
		removeparpadeo("#VIcell21-0-"+i, 0.5, azul);
    }

    for(var i = 0 ; i < cifrado.length ; i++){
    	$('#VIcell21-1-'+i).html(clave[i%clave.length]);

    	parpadeo('#VIcell21-1-'+i, 0.5, azul);
		await sleep(500);
		removeparpadeo('#VIcell21-1-'+i, 0.5, azul);
    }

    if(cancelado){
		return;
	}

    await sleep(1000);

    if(cancelado){
		return;
	}

    //Descifrado
     $('#VIdiv2').slideToggle(1000);
    
    await sleep(1000);

    $('#VIdiv2').html('Se hace uso del cuadrado de Vigenère. La primer fila de la matriz corresponde a los caracteres de la clave y la primera columna a los caracteres del texto plano.');
	$('#VIdiv2').slideToggle(1000);

	if(cancelado){
		return;
	}

	await sleep(8000);
	
	if(cancelado){
		return;
	}

	crearTablaTritemioD();

	if(cancelado){
		return;
	}

	$('#VIdiv2').slideToggle(1000);
	
	await sleep(1000);

    $('#VIdiv2').html('Se busca en la columna de la letra de la clave el caracter del criptograma con el que coincide, la primera letra que esté en la fila del caracter es la letra del texto plano.');
	$('#VIdiv2').slideToggle(1000);

	if(cancelado){
		return;
	}

	await sleep(8000);
	
	if(cancelado){
		return;
	}

	$('#VIdiv2').scrollView();

    for(var i = 0 ; i < cifrado.length && !cancelado ; i++){
    	var pintar = true;

    	parpadeo('#VIcell21-1-'+i, 3*velocidad, azul);

    	for(j = 0 ; j < 26 && pintar == true ; j++){
    		parpadeo('#VIcell22-'+j+"-"+(clave[i%clave.length].charCodeAt()-65), 3*velocidad, azul);

	    	if(cifrado[i] == matrizVigenere[j][clave[i%clave.length].charCodeAt()-65]){
	    		pintar = false;
	    	}
	    }
    	
    	await sleep(1000*velocidad);
    	j--;
		
		parpadeo('#VIcell21-0-'+i, 2*velocidad, amarillo);
    	
    	for(var k = clave[i%clave.length].charCodeAt()-65 ; k >= 0 ; k--){
	    	parpadeo('#VIcell22-'+j+'-'+k, 2*velocidad, amarillo);
	    }
		
		await sleep(1000*velocidad);

		removeparpadeo('#VIcell22-'+j+"-0", 2*velocidad, amarillo);
		parpadeo('#VIcell22-'+j+"-0", 1*velocidad, verde);
		parpadeo("#VI-MCcell1"+i, 1*velocidad, negro);
    	$("#VI-MCcell1"+i).html(matrizVigenere[j][0].toLowerCase());
    	
    	cadenaDescifrado = cadenaDescifrado + matrizVigenere[j][0].toLowerCase();

	    await sleep(1000*velocidad);
		pintar = true
	    
	    removeparpadeo('#VIcell22-'+j+"-0", 1*velocidad, verde);
	    
    	for(j = 0 ; j < 26 && pintar == true ; j++){
	    	removeparpadeo('#VIcell22-'+j+"-"+(clave[i%clave.length].charCodeAt()-65), 3*velocidad, azul);

	    	if(cifrado[i] == matrizVigenere[j][clave[i%clave.length].charCodeAt()-65]){
	    		pintar = false;
	    	}
	    }

	    j--;

    	for(var k = clave[i%clave.length].charCodeAt()-65 ; k >= 0 ; k--){
	    	removeparpadeo('#VIcell22-'+j+'-'+k, 2*velocidad, amarillo);
	    }

	    removeparpadeo('#VIcell21-1-'+i, 3*velocidad, azul);
	    removeparpadeo('#VIcell21-0-'+i, 2*velocidad, amarillo);
	    removeparpadeo("#VI-MCcell1"+i, 1*velocidad, negro);

		await sleep(100);
    }

    if(cancelado){
		return;
	}

	$("#out-textoDescifradoVigenere").val(cadenaDescifrado);
	$("#btn-velocidadDVigenere").show();
    $("#btn-descifrarVigenere").show();
	$("#btn-cancelarDescifrarVigenere").hide();

	if(!cancelado){
		$('#VIdiv2').slideToggle(1000);
		toastr.options.timeOut = "1000";
    	toastr['success']('Texto cifrado');
    }
}

function validarEntradaTextoCVigenere(){
	var mensaje = "";
	var texto = $('#in-textoPlanoVigenere').val().replace(/ /g,"");

	if (texto.length < 1 || texto.length > 10) {
		mensaje = "El mensaje claro debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaLlaveCVigenere(){
	var mensaje = "";
	var clave = $('#in-claveCifradoVigenere').val();

	if(clave.indexOf(' ') >= 0){
		mensaje = "La llave no debe contener espacios.";
	}
	else if (clave.length < 1 || clave.length > 10) {
		mensaje = "La llave debe contener entre 1 y 10 caracteres.";
	}
	else if(!clave.match(/^[a-zA-Z]+$/)){
		mensaje = "La llave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaTextoDVigenere(){
	var mensaje = "";
	var texto = $('#in-textoPlanoCifradoVigenere').val();

	if(texto.indexOf(' ') >= 0){
		mensaje = "El criptograma no debe contener espacios.";
	}
	else if (texto.length < 1 || texto.length > 10) {
		mensaje = "El criptograma debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaLlaveDVigenere(){
	var mensaje = "";
	var clave = $('#in-claveDescifradoVigenere').val();

	if(clave.indexOf(' ') >= 0){
		mensaje = "La clave no debe contener espacios.";
	}
	else if (clave.length < 1 || clave.length > 10) {
		mensaje = "La clave debe contener entre 1 y 10 caracteres.";
	}
	else if(!clave.match(/^[a-zA-Z]+$/)){
		mensaje = "La clave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

$(document).ready(function(){
	$("#tipoVigenereC1").click(function(){
        $("#btn-cifrarVigenere").html('Cifrado Rápido');
        $("#btn-cifrarVigenere").val(1);
    });
    $("#tipoVigenereC2").click(function(){
        $("#btn-cifrarVigenere").html('Cifrado Normal');
        $("#btn-cifrarVigenere").val(2);
    });
    $("#tipoVigenereC3").click(function(){
        $("#btn-cifrarVigenere").html('Cifrado Lento&nbsp;');
        $("#btn-cifrarVigenere").val(3);
    });

    $("#tipoVigenereD1").click(function(){
        $("#btn-descifrarVigenere").html('Descifrado Rápido');
        $("#btn-descifrarVigenere").val(1);
    });
    $("#tipoVigenereD2").click(function(){
        $("#btn-descifrarVigenere").html('Descifrado Normal');
        $("#btn-descifrarVigenere").val(2);
    });
    $("#tipoVigenereD3").click(function(){
        $("#btn-descifrarVigenere").html('Descifrado Lento&nbsp;');
        $("#btn-descifrarVigenere").val(3);
    });

    $("#in-textoPlanoVigenere").keyup(function(){
        var mensaje = validarEntradaTextoCVigenere();

        if (mensaje.length != 0) {
            $("#textoPlanoVigenere-error").remove();
            $("#in-textoPlanoVigenere").parent().parent().append('<div id="textoPlanoVigenere-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoVigenere").addClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", true);
        } else{
            $("#textoPlanoVigenere-error").remove();
            $("#in-textoPlanoVigenere").removeClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", false);
        }
    });

    $("#in-claveCifradoVigenere").keyup(function(){
        var mensaje = validarEntradaLlaveCVigenere();

        if (mensaje.length != 0) {
            $("#claveCVigenere-error").remove();
            $("#in-claveCifradoVigenere").parent().parent().append('<div id="claveCVigenere-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-claveCifradoVigenere").addClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", true);
        } else{
            $("#claveCVigenere-error").remove();
            $("#in-claveCifradoVigenere").removeClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", false);
        }
    });

    $("#in-textoPlanoCifradoVigenere").keyup(function(){
        var mensaje = validarEntradaTextoDVigenere();

        $("#textoPlanoCifradoVigenere-info").remove();

        if (mensaje.length != 0) {
            $("#textoPlanoCifradoVigenere-error").remove();
            $("#in-textoPlanoCifradoVigenere").parent().parent().append('<div id="textoPlanoCifradoVigenere-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoCifradoVigenere").addClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", true);
        } else{
            $("#textoPlanoCifradoVigenere-error").remove();
            $("#in-textoPlanoCifradoVigenere").removeClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", false);
        }
    });

    $("#in-claveDescifradoVigenere").keyup(function(){
        var mensaje = validarEntradaLlaveDVigenere();

        $("#textoPlanoCifradoVigenere-info").remove();

        if (mensaje.length != 0) {
            $("#claveDVigenere-error").remove();
            $("#in-claveDescifradoVigenere").parent().parent().append('<div id="claveDVigenere-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-claveDescifradoVigenere").addClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", true);
        } else{
            $("#claveDVigenere-error").remove();
            $("#in-claveDescifradoVigenere").removeClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", false);
        }
    });

	$("#btn-cifrarVigenere").click(function(){
		var mensajetexto = validarEntradaTextoCVigenere();
		var mensajeclave = validarEntradaLlaveCVigenere();

		if(mensajetexto.length > 0){
			$("#textoPlanoVigenere-error").remove();
            $("#in-textoPlanoVigenere").parent().parent().append('<div id="textoPlanoVigenere-error" class="text-danger">&nbsp;'+mensajetexto+'</div>');
            $("#in-textoPlanoVigenere").addClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", true);
		}
		else if(mensajeclave.length > 0){
			$("#claveCVigenere-error").remove();
            $("#in-claveCifradoVigenere").parent().parent().append('<div id="claveCVigenere-error" class="text-danger">&nbsp;'+mensajeclave+'</div>');
            $("#in-claveCifradoVigenere").addClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", true);
		}
		else{
			$("#textoPlanoVigenere-error").remove();
            $("#claveCVigenere-error").remove();
            $("#in-textoPlanoVigenere").removeClass('input-error');
            $("#in-claveCifradoVigenere").removeClass('input-error');
            $("#btn-cifrarVigenere").attr("disabled", false);

            if($('#btn-cifrarVigenere').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-cifrarVigenere').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-velocidadCVigenere").hide();
            $("#btn-cifrarVigenere").hide();
            $("#btn-cancelarCifrarVigenere").show();
            cancelado = false;
            
            cifrarVigenere();
		}
	});

	$("#btn-cancelarCifrarVigenere").click(function(){
        cancelado = true;

        limpiaPanelCifradoVigenere();

        $("#btn-cifrarVigenere").show();
        $("#btn-velocidadCVigenere").show();
        $("#btn-cancelarCifrarVigenere").hide();
    });

    $("#btn-cancelarDescifrarVigenere").click(function(){
        cancelado = true;

        limpiaPanelDescifradoVigenere();

        $("#btn-descifrarVigenere").show();
        $("#btn-velocidadDTransposisionVigenere").show();
        $("#btn-cancelarDescifrarVigenere").hide();
    });

    $("#btn-copiarTextoVigenere").click(function(){
        if ($("#out-textoCifradoVigenere").val()==''){
            $("#textoPlanoCifradoVigenere-info").remove();
            $("#in-textoPlanoCifradoVigenere").parent().parent().append('<div id="textoPlanoCifradoVigenere-info" class="text-info">Primero debes cifrar un mensaje</div>');
        } else {
            $("#textoPlanoCifradoVigenere-info").remove();
            $("#in-textoPlanoCifradoVigenere").val($("#out-textoCifradoVigenere").val());
            $("#in-claveDescifradoVigenere").val($("#in-claveCifradoVigenere").val());
        }
    });

    $("#btn-descifrarVigenere").click(function(){
        var mensajetexto = validarEntradaTextoDVigenere();
		var mensajeclave = validarEntradaLlaveDVigenere();

		$("#textoPlanoCifradoVigenere-info").remove();

		if(mensajetexto.length > 0){
			$("#textoPlanoCifradoVigenere-error").remove();
            $("#in-textoPlanoCifradoVigenere").parent().parent().append('<div id="textoPlanoCifradoVigenere-error" class="text-danger">&nbsp;'+mensajetexto+'</div>');
            $("#in-textoPlanoCifradoVigenere").addClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", true);
		}
		else if(mensajeclave.length > 0){
			$("#claveDVigenere-error").remove();
            $("#in-claveDescifradoVigenere").parent().parent().append('<div id="claveDVigenere-error" class="text-danger">&nbsp;'+mensajeclave+'</div>');
            $("#in-claveDescifradoVigenere").addClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", true);
		}
		else{
			$("#textoPlanoCifradoVigenere-error").remove();
            $("#claveDVigenere-error").remove();
            $("#in-textoPlanoCifradoVigenere").removeClass('input-error');
            $("#in-claveDescifradoVigenere").removeClass('input-error');
            $("#btn-descifrarVigenere").attr("disabled", false);

            if($('#btn-descifrarVigenere').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-descifrarVigenere').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-velocidadDVigenere").hide();
            $("#btn-descifrarVigenere").hide();
            $("#btn-cancelarDescifrarVigenere").show();
            cancelado = false;
            
            descifrarVigenere();
		}
    });
});