function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var ic=0, idc=0;
var tCAdd=800, tDcAdd=800;
var tCRemove=300, tDcRemove=300;

function mostrarPanelCesar(){
	crearPanelCesar();
	$("#pnl-InteractivoCesar").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelCesar(){
	$("#pnl-InteractivoCesar").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelCesar();
}

function crearPanelCesar(){
	for (var i = 65; i <= 90; i++) {
		$("#textoPlanoCesarC").append('<label id="abcPlano'+i+'C" class="circulo">'+String.fromCharCode(i).toLowerCase()+'&nbsp;</label>');
		$("#textoCifradoCesarC").append('<label id="abcCifrado'+i+'C" class="circulo">'+String.fromCharCode(i)+'</label>');
		$("#textoPlanoCesarD").append('<label id="abcPlano'+i+'D" class="circulo">'+String.fromCharCode(i).toLowerCase()+'&nbsp;</label>');
		$("#textoCifradoCesarD").append('<label id="abcCifrado'+i+'D" class="circulo">'+String.fromCharCode(i)+'</label>');
	}
}

function limpiaPanelCesar(){
	$("#textoPlanoCesarC").empty();
	$("#textoCifradoCesarC").empty();
	$("#textoPlanoCesarD").empty();
	$("#textoCifradoCesarD").empty();
	$("#palabraCesarC").empty();
	$("#palabraCesarD").empty();
	$("#palabraCifradaCesarC").empty();
	$("#palabraDescifradaCesarD").empty();
	$("#in-txtPlanoCesar").val("");
	$("#out-txtCifradoCesar").val("");
	$("#in-txtCifradoCesar").val("");
	$("#out-txtPlanoCesar").val("");
	$("#infoAnimacionCiCesar").hide();
	$("#infoAnimacionDeCesar").hide();
	$("#in-txtPlanoCesar").removeClass('error');
	$("#txtPlanoCesar-error").remove();
	$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
	$("#btn-cifrarCesar").show();
	$("#btn-tipoCiCesar").show();
	$("#btn-cancelarCifrarCesar").hide();
	$("#btn-descifrarCesar").show();
	$("#btn-tipoDeCesar").show();
	$("#btn-cancelarDescifrarCesar").hide();
}

function pararAnimacionCesar(){
	ic=999;
	idc=999;
    $("#btn-copiarTextoCesar").removeAttr("disabled");
    $("#palabraCesarC").empty();
	$("#palabraCesarD").empty();
	$("#palabraCifradaCesarC").empty();
	$("#palabraDescifradaCesarD").empty();
	$("#infoAnimacionCiCesar").hide();
	$("#infoAnimacionDeCesar").hide();
	$("#in-txtPlanoCesar").removeClass('error');
	$("#txtPlanoCesar-error").remove();
	$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
	$("#btn-cifrarCesar").show();
	$("#btn-tipoCiCesar").show();
	$("#btn-cancelarCifrarCesar").hide();
	$("#btn-descifrarCesar").show();
	$("#btn-tipoDeCesar").show();
	$("#btn-cancelarDescifrarCesar").hide();
}

async function cifrarCesar(){
    var plano = ($("#in-txtPlanoCesar").val().toUpperCase()).split("");
    var cifrado = [];
    var cadenaCifrado;
    ic = 0;
    for (var i = 0; i <= plano.length-1; i++) {
        if(plano[i] == ' ') {
           plano.splice(i, 1);
        }
    }
    $("#infoAnimacionCiCesar").fadeIn();
    await sleep(tCAdd);
    for (var m = 0; m <= plano.length-1; m++) {
		$("#palabraCesarC").append('<label id="palabra'+m+'C" class="circulo">'+plano[m].toLowerCase()+'</label>');
    }
    for (var n = 0; n <= plano.length-1; n++) {
		$("#palabraCifradaCesarC").append('<label id="palabraCifrado'+n+'C" class="circulo">&nbsp;</label>');
    }
    while (ic <= plano.length-1) {
		if (plano[ic].charCodeAt()+3 <= 90){
			cifrado[ic] = String.fromCharCode(plano[ic].charCodeAt()+3);
			numeroElementoPlano = plano[ic].charCodeAt();
    		numeroElementoCifrado = plano[ic].charCodeAt()+3;
		} else {
			cifrado[ic] = String.fromCharCode((plano[ic].charCodeAt()+3)-26);
			numeroElementoPlano = plano[ic].charCodeAt();
    		numeroElementoCifrado = (plano[ic].charCodeAt()+3)-26;
		}
		$("#palabraCifrado"+ic+"C").html(cifrado[ic]).show();
    	
    	// ANIMATION
    	$("#infoCiCesar").html('<br><strong>'+cifrado[ic]+'</strong> es la letra <strong>'+plano[ic].toLowerCase()+'</strong> recorrida tres posiciones');
    	$("#palabra"+ic+"C").addClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"C").addClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"C").addClass('parpadeoNext');
    	$("#palabraCifrado"+ic+"C").addClass('parpadeoNext');
    	await sleep(tCAdd);
    	$("#palabra"+ic+"C").removeClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"C").removeClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"C").removeClass('parpadeoNext');
    	$("#palabraCifrado"+ic+"C").removeClass('parpadeoNext');
    	await sleep(tCRemove);
    	// END ANIMATION
    	ic++;
    }
    if (ic <= plano.length && ic!=999) {
	    cadenaCifrado = cifrado.join("");
	    $("#out-txtCifradoCesar").val(cadenaCifrado);
	    toastr.options.timeOut = "1000";
		toastr['success']('Texto cifrado');
		$("#btn-cifrarCesar").show();
		$("#btn-tipoCiCesar").show();
		$("#btn-cancelarCifrarCesar").hide();
	}
}

async function descifrarCesar(){
    var cifrado = ($("#in-txtCifradoCesar").val().toUpperCase()).split("");
    var plano = [];
    var cadenaDescifrado;
    idc = 0;
    for (var i = 0; i <= cifrado.length-1; i++) {
        if(cifrado[i] == ' ') {
           cifrado.splice(i, 1);
        }
    }
    $("#infoAnimacionDeCesar").fadeIn();
    await sleep(tCAdd);
    for (var m = 0; m <= cifrado.length-1; m++) {
		$("#palabraCesarD").append('<label id="palabra'+m+'D" class="circulo">'+cifrado[m]+'</label>');
    }
    for (var n = 0; n <= cifrado.length-1; n++) {
		$("#palabraDescifradaCesarD").append('<label id="palabraDescifrado'+n+'D" class="circulo">&nbsp;</label>');
    }
    while (idc <= cifrado.length-1) {
		if (cifrado[idc].charCodeAt()-3 >= 65){
			plano[idc] = String.fromCharCode(cifrado[idc].charCodeAt()-3).toLowerCase();
			numeroElementoCifrado = cifrado[idc].charCodeAt();
    		numeroElementoPlano = cifrado[idc].charCodeAt()-3;
		} else {
			plano[idc] = String.fromCharCode((cifrado[idc].charCodeAt()-3)+26).toLowerCase();
			numeroElementoCifrado = cifrado[idc].charCodeAt();
    		numeroElementoPlano = (cifrado[idc].charCodeAt()-3)+26;
		}
		$("#palabraDescifrado"+idc+"D").html(plano[idc]).show();
    	
    	// ANIMATION
    	$("#infoDeCesar").html('<br><strong>'+plano[idc]+'</strong> es la letra <strong>'+cifrado[idc]+'</strong> recorrida tres posiciones');
    	$("#palabra"+idc+"D").addClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"D").addClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"D").addClass('parpadeoNext');
    	$("#palabraDescifrado"+idc+"D").addClass('parpadeoNext');
    	await sleep(tDcAdd);
    	$("#palabra"+idc+"D").removeClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"D").removeClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"D").removeClass('parpadeoNext');
    	$("#palabraDescifrado"+idc+"D").removeClass('parpadeoNext');
    	await sleep(tDcRemove);
    	// END ANIMATION
    	idc++;
    }
    if (idc <= cifrado.length && idc!=999) {
	    cadenaDescifrado = plano.join("");
	    $("#out-txtPlanoCesar").val(cadenaDescifrado);
	    $("#btn-copiarTextoCesar").removeAttr("disabled");
	    toastr.options.timeOut = "1000";
		toastr['success']('Texto descifrado');
		$("#btn-descifrarCesar").show();
		$("#btn-tipoDeCesar").show();
		$("#btn-cancelarDescifrarCesar").hide();
	}
}

function validarEntradaCifradoCesar(){
	var mensaje = "";
	var texto = $('#in-txtPlanoCesar').val();
	if (texto.length < 1 || texto.length > 20) {
		mensaje = "El mensaje claro debe contener entre 1 y 20 caracteres.";
	} else if (!texto.match(/^[a-zA-Z \s]+$/)){
		mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}
	return mensaje;
}

function validarEntradaDescifradoCesar(){
	var mensaje = "";
	var texto = $('#in-txtCifradoCesar').val();
	if (texto.length < 1 || texto.length > 20) {
		mensaje = "El criptograma debe contener entre 1 y 20 caracteres.";
	} else if (!texto.match(/^[a-zA-Z \s]+$/)){
		mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}
	return mensaje;
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$("#btn-mostrarPanelCesar").click(function(){
		mostrarPanelCesar();
	});
	$("#btn-cerrarPanelCesar").click(function(){
		pararAnimacionCesar();
		cerrarPanelCesar();
	});
	$("#btn-teoriaCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#btn-fundamentosCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#btn-animacionCifradoCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#btn-animacionDesifradoCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#btn-cancelarCifrarCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#btn-cancelarDescifrarCesar").click(function(){
		pararAnimacionCesar();
	});
	$("#tipoCiCesar1").click(function(){
		$("#btn-cifrarCesar").html('Cifrado Rápido');
		$("#btn-cifrarCesar").val(1);
	});
	$("#tipoCiCesar2").click(function(){
		$("#btn-cifrarCesar").html('Cifrado Normal');
		$("#btn-cifrarCesar").val(2);
	});
	$("#tipoCiCesar3").click(function(){
		$("#btn-cifrarCesar").html('Cifrado Lento');
		$("#btn-cifrarCesar").val(3);
	});
	$("#tipoDeCesar1").click(function(){
		$("#btn-descifrarCesar").html('Descifrado Rápido');
		$("#btn-descifrarCesar").val(1);
	});
	$("#tipoDeCesar2").click(function(){
		$("#btn-descifrarCesar").html('Descifrado Normal');
		$("#btn-descifrarCesar").val(2);
	});
	$("#tipoDeCesar3").click(function(){
		$("#btn-descifrarCesar").html('Descifrado Lento');
		$("#btn-descifrarCesar").val(3);
	});

	$("#in-txtPlanoCesar").change(function(){
		$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
		$("#txtPlanoCesar-error").remove();
		if ($("#in-txtPlanoCesar").val()=='') {
			$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
			$("#txtPlanoCesar-error").remove();
		} else{
			var mensaje = validarEntradaCifradoCesar();
			if (mensaje.length == 0){
				$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
				$("#txtPlanoCesar-error").remove();
			} else {
				$("#in-txtPlanoCesar").parent().parent().addClass('has-error has-feedback');
				$("#in-txtPlanoCesar").parent().parent().append('<div id="txtPlanoCesar-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			}
		}
	});

	$("#in-txtCifradoCesar").change(function(){
		$("#in-txtCifradoCesar").parent().parent().removeClass('has-error has-feedback');
		$("#txtCifradoCesar-error").remove();
		if ($("#in-txtCifradoCesar").val()=='') {
			$("#in-txtCifradoCesar").parent().parent().removeClass('has-error has-feedback');
			$("#txtCifradoCesar-error").remove();
		} else{
			var mensaje = validarEntradaDescifradoCesar();
			if (mensaje.length == 0){
				$("#in-txtCifradoCesar").parent().parent().removeClass('has-error has-feedback');
				$("#txtCifradoCesar-error").remove();
			} else {
				$("#in-txtCifradoCesar").parent().parent().addClass('has-error has-feedback');
				$("#in-txtCifradoCesar").parent().parent().append('<div id="txtCifradoCesar-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			}
		}
	});

	$("#btn-cifrarCesar").click(function(){
		$("#in-txtPlanoCesar").parent().parent().removeClass('has-error has-feedback');
		$("#txtPlanoCesar-error").remove();
		$("#out-txtCifradoCesar").val("");
		var mensaje = validarEntradaCifradoCesar();
		if ($("#in-txtPlanoCesar").val()!='' && mensaje.length == 0){
			$("#palabraCesarC").empty();
			$("#palabraCifradaCesarC").empty();
			$("#infoAnimacionCiCesar").hide();
			$("#infoCiCesar").html('');
			$("#btn-cifrarCesar").hide();
			$("#btn-tipoCiCesar").hide();
			$("#btn-cancelarCifrarCesar").show();
			cifrarCesar();
		} else{
			$("#in-txtPlanoCesar").parent().parent().addClass('has-error has-feedback');
			$("#in-txtPlanoCesar").parent().parent().append('<div id="txtPlanoCesar-error" class="text-danger">&nbsp;'+mensaje+'</div>');
		}
	});

	$("#btn-copiarTextoCesar").click(function(){
		if ($("#out-txtCifradoCesar").val()==''){
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		} else {
			$("#in-txtCifradoCesar").val($("#out-txtCifradoCesar").val());
		}
	});

	$("#btn-descifrarCesar").click(function(){
		$("#in-txtCifradoCesar").parent().parent().removeClass('has-error has-feedback');
		$("#txtCifradoCesar-error").remove();
		$("#out-txtPlanoCesar").val("");
		var mensaje = validarEntradaDescifradoCesar();
		if ($("#in-txtCifradoCesar").val()!='' && mensaje.length == 0){
			$("#btn-copiarTextoCesar").attr("disabled","disabled");
			$("#palabraCesarD").empty();
			$("#palabraDescifradaCesarD").empty();
			$("#infoAnimacionDeCesar").hide();
			$("#infoDeCesar").html('');
			$("#btn-descifrarCesar").hide();
			$("#btn-tipoDeCesar").hide();
			$("#btn-cancelarDescifrarCesar").show();
			descifrarCesar();
		} else{
			$("#in-txtCifradoCesar").parent().parent().addClass('has-error has-feedback');
			$("#in-txtCifradoCesar").parent().parent().append('<div id="txtCifradoCesar-error" class="text-danger">&nbsp;'+mensaje+'</div>');
		}
	});
	    
});