var celdasSeleccionadasRejillaC = [];
var celdasSeleccionadasRejillaD = [];
var velocidadAnimacionCifrarRejilla= 1;
var velocidadAnimacionDescifrarRejilla= 1;
var seguirCifrandoRejilla= true;
var seguirDescifrandoRejilla= true;

function mostrarPanelRejillaC()
{
	//crearPanelAfin();
	$("#panelInteractivo-CifradoRejillaC").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);	
}

function cerrarPanelRejillaC()
{
	$("#panelInteractivo-CifradoRejillaC").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelRejillaC();
}

function limpiaPanelRejillaCifrado()
{	
	if($('#informacionRejilla1C').is(':visible'))
	{
		$("#informacionRejilla1C").slideToggle(500);
	}
	
	if($('#tablaTextoPlanoRejillaC').is(':visible'))
	{
		$("#tablaTextoPlanoRejillaC").slideToggle(500);
	}
	
	celdasSeleccionadasRejillaC= [];
	
	for(var i=1; i<=4; i++)
	{
		for(var j=1; j<=9; j++)
		{
			$("#Cuadrante"+i+"Valor"+j+"C").css("backgroundColor", "transparent");
			$("#Cuadrante"+i+"Valor"+j+"C").css("color", "black");
		}		
	}
	
	$("#informacionRejilla1C").empty();
	$("#tablaTextoPlanoRejillaC").empty();	
	$("#textoCifradoRejillaC3").empty();
	$("#textoPlanoRejillaC").val("");
	$("#textoCifradoRejillaC2").val("");
}

function limpiaPanelRejillaDescifrado()
{	
	if($('#informacionRejilla3D').is(':visible'))
	{
		$("#informacionRejilla3D").slideToggle(500);
	}
	
	if($('#tablaTextoCifradoRejillaD').is(':visible'))
	{
		$("#tablaTextoCifradoRejillaD").slideToggle(500);
	}
	
	celdaTextoCifradoRejillaD= [];
	
	for(var i=1; i<=4; i++)
	{
		for(var j=1; j<=9; j++)
		{
			$("#Cuadrante"+i+"Valor"+j+"D").css("backgroundColor", "transparent");
			$("#Cuadrante"+i+"Valor"+j+"D").css("color", "black");
		}		
	}
	
	$("#informacionRejilla3D").empty();
	$("#tablaTextoCifradoRejillaD").empty();		
	$("#textoCriptogramaRejillaDescifrado").val("");
	$("#textoDescifradoRejillaD2").val("");
}

function obtenerVelocidadAnimacionRejillaCifrar()
{
	if($('#btn-cifrarRejilla-cifrado').val() == 1)
	{
		velocidadAnimacionCifrarRejilla = 0.5;
	}
	else if($('#btn-cifrarRejilla-cifrado').val() == 2)
	{
		velocidadAnimacionCifrarRejilla = 1;
	}
	else
	{
		velocidadAnimacionCifrarRejilla = 2;
	}

	$("#btn-velocidadCRejilla").hide();
	$("#btn-cifrarRejilla-cifrado").hide();
	$("#btn-cancelarCifrarRejilla-cifrado").show();
	seguirCifrandoRejilla= true;
}

function obtenerVelocidadAnimacionRejillaDescifrar()
{
	if($('#btn-descifrarRejilla-descifrado').val() == 1)
	{
		velocidadAnimacionDescifrarRejilla = 0.5;
	}
	else if($('#btn-descifrarRejilla-descifrado').val() == 2)
	{
		velocidadAnimacionDescifrarRejilla = 1;
	}
	else
	{
		velocidadAnimacionDescifrarRejilla = 2;
	}

	$("#btn-velocidadDRejilla").hide();
	$("#btn-descifrarRejilla-descifrado").hide();
	$("#btn-cancelarDescifrarRejilla-descifrado").show();
	seguirDescifrandoAtbash= true;
}

function seleccionarCuadrante1Valor1C(elem)
{	
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{		
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor1C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor1C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor1C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{		
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor1C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor1C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor1C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor1C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor1C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor1C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor1C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor1C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor1C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor1C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor1C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor1C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor1C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor1C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor1C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor1C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor1C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor2C(elem)
{	
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{		
		if(celdasSeleccionadasRejillaC.length>0)
		{			
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor2C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{					
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor2C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor2C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor2C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor2C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor2C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor2C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor2C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor2C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor2C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor2C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor2C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor2C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor2C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor2C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor2C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor2C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor2C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor2C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor2C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor3C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor3C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor3C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor3C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor3C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor3C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor3C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor3C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor3C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor3C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor3C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor3C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor3C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor3C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor3C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor3C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor3C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor3C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor3C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor3C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor3C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor4C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor4C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor4C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor4C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor4C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor4C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor4C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor4C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor4C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor4C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor4C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor4C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor4C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor4C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor4C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor4C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor4C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor4C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor4C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor4C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor4C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor5C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor5C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor5C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor5C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor5C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor5C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor5C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor5C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor5C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor5C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor5C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor5C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor5C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor5C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor5C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor5C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor5C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor5C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor5C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor5C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor5C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor6C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor6C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor6C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor6C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor6C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor6C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor6C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor6C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor6C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor6C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor6C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor6C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor6C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor6C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor6C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor6C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor6C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor6C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor6C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor6C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor6C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor7C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor7C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor7C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor7C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor7C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor7C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor7C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor7C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor7C");
			i= celdasSeleccionadasRejillaC.length;
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor7C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor7C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor7C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor7C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor7C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor7C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor7C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor7C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor7C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor7C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor7C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor7C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor8C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor8C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor8C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor8C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor8C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor8C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor8C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor8C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor8C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor8C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor8C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor8C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor8C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor8C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor8C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor8C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor8C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor8C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor8C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor8C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor8C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor9C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor9C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante1Valor9C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante1Valor9C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor9C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor9C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor9C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante2Valor9C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante2Valor9C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor9C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor9C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante4Valor9C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante3Valor9C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante3Valor9C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor9C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor9C(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaC.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i].includes("Cuadrante1Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante2Valor9C")||celdasSeleccionadasRejillaC[i].includes("Cuadrante3Valor9C"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaC.length;
				}
				else if(i+1==celdasSeleccionadasRejillaC.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaC.push("Cuadrante4Valor9C");
					i= celdasSeleccionadasRejillaC.length;
					//console.log(celdasSeleccionadasRejillaC.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaC.push("Cuadrante4Valor9C");
			//console.log(celdasSeleccionadasRejillaC.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
		{
			if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor9C")
			{
				celdasSeleccionadasRejillaC.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaC.length;
				//console.log(celdasSeleccionadasRejillaC.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor1D(elem)
{	
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{		
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor1D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor1D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor1D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{		
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor1D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor1D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor1D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor1D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor1D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor1D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor1D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor1D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor1D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor1D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor1D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor1D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor1D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor1D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor1D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor1D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor1D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor2D(elem)
{	
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{		
		if(celdasSeleccionadasRejillaD.length>0)
		{			
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor2D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{					
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor2D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor2D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor2D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor2D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor2D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor2D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor2D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor2D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor2D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor2D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor2D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor2D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor2D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor2D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor2D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor2D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor2D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor2D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor2D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor3D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor3D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor3D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor3D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor3D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor3D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor3D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor3D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor3D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor3D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor3D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor3D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor3D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor3D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor3D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor3D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor3D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor3D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor3D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor3D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor3D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor4D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor4D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor4D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor4D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor4D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor4D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor4D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor4D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor4D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor4D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor4D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor4D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor4D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor4D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor4D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor4D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor4D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor4D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor4D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor4D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor4D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor5D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor5D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor5D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor5D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor5D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor5D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor5D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor5D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor5D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor5D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor5D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor5D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor5D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor5D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor5D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor5D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor5D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor5D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor5D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor5D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor5D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor6D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor6D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor6D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor6D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor6D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor6D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor6D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor6D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor6D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor6D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor6D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor6D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor6D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor6D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor6D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor6D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor6D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor6D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor6D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor6D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor6D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor7D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor7D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor7D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor7D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor7D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor7D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor7D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor7D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor7D");
			i= celdasSeleccionadasRejillaD.length;
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor7D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor7D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor7D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor7D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor7D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor7D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor7D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor7D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor7D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor7D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor7D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor7D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor8D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor8D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor8D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor8D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor8D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor8D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor8D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor8D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor8D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor8D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor8D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor8D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor8D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor8D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor8D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor8D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor8D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor8D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor8D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor8D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor8D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante1Valor9D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor9D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor9D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante1Valor9D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor9D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

function seleccionarCuadrante2Valor9D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor9D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor9D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante2Valor9D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor9D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}
	
function seleccionarCuadrante3Valor9D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante4Valor9D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor9D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante3Valor9D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor9D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}	

function seleccionarCuadrante4Valor9D(elem)
{
	if(elem.style.backgroundColor != "rgb(253, 253, 150)")
	{
		if(celdasSeleccionadasRejillaD.length>0)
		{
			for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
			{
				if(celdasSeleccionadasRejillaD[i].includes("Cuadrante1Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante2Valor9D")||celdasSeleccionadasRejillaD[i].includes("Cuadrante3Valor9D"))
				{
					//NO AGREGA NADA MUESTRA ERROR
					i= celdasSeleccionadasRejillaD.length;
				}
				else if(i+1==celdasSeleccionadasRejillaD.length)
				{
					elem.style.backgroundColor = "#FDFD96";
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor9D");
					i= celdasSeleccionadasRejillaD.length;
					//console.log(celdasSeleccionadasRejillaD.length);
				}
			}
				
		}
		else
		{
			elem.style.backgroundColor = "#FDFD96";
			celdasSeleccionadasRejillaD.push("Cuadrante4Valor9D");
			//console.log(celdasSeleccionadasRejillaD.length);
		}							
	}
	else
	{
		for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
		{
			if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor9D")
			{
				celdasSeleccionadasRejillaD.splice(i, 1);
				elem.style.backgroundColor = "transparent";
				i= celdasSeleccionadasRejillaD.length;
				//console.log(celdasSeleccionadasRejillaD.length);
			}
		}			
	}
}

async function cifrarRejilla()
{
	var textoPlano = ($("#textoPlanoRejillaC").val().toLowerCase().replace(/ /g,"")).split("");
	var posTextoPlano= 0;
	var numeroCelda= 1;
	var cifrado= [];
	var cadenaCifrado;
	var posArrayTextoCifrado= 0;
	var auxCeldasSeleccionadasRejillaC = celdasSeleccionadasRejillaC.slice();
	var proxElementos= [];
	
	obtenerVelocidadAnimacionRejillaCifrar();
	
	//limpiaPanelRejillaCifrado();
    $("#textoPlanoRejillaC").val(textoPlano.join(""));

	$("#informacionRejilla1C").append("Colocamos el mensaje claro de la siguiente forma: (Si no se llena toda la tabla, los cuadros faltantes se escriben letras al azar)");
	$("#informacionRejilla1C").slideToggle(500);
	posicion = $("#informacionRejilla1C").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(3100);
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
	
	$("#tablaTextoPlanoRejillaC").slideToggle(500);
	posicion = $("#tablaTextoPlanoRejillaC").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(500);
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
		
	for(var i=0; i<6; i++)
	{		
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f1").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f1").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f1").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}		
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f2").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f2").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f2").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f3").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f3").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f3").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f4").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f4").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f4").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f5").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f5").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f5").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		if(posTextoPlano<textoPlano.length)
		{
			if(textoPlano[posTextoPlano]!=' ')
			{
				$("#f6").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
			else
			{								
				while(textoPlano[posTextoPlano]==' ')
				{
					posTextoPlano++;
				}
				
				$("#f6").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+textoPlano[posTextoPlano]+'</td>');
				posTextoPlano++;
			}
		}
		else
		{
			var aleatorio = Math.round((Math.random()*25)+97);
			$("#f6").append('<td id="celdaTextoPlanoRejillaC'+numeroCelda+'">'+String.fromCharCode(aleatorio)+'</td>');
		}
		
		numeroCelda++;
		await sleep(100*velocidadAnimacionCifrarRejilla);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
			
	if(!seguirCifrandoRejilla)
	{
		return;
	}
			
	$("#informacionRejilla1C").slideToggle(500);
	$("#informacionRejilla1C").empty();
	await sleep(500);
	$("#informacionRejilla1C").append("Colocamos la rejilla con las celdas(huecos) seleccionadas sobre el mensaje claro:");
	$("#informacionRejilla1C").slideToggle(500);
	posicion = $("#informacionRejilla1C").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(2100);
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
	
	for(var i=1; i<=36; i++)
	{
		$("#celdaTextoPlanoRejillaC"+i).css("backgroundColor", "black");
		$("#celdaTextoPlanoRejillaC"+i).css("color", "black");
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}	
	
	for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
	{
		if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor1C")
		{
			$("#celdaTextoPlanoRejillaC1").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor1C")
		{
			$("#celdaTextoPlanoRejillaC6").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor1C")
		{
			$("#celdaTextoPlanoRejillaC36").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor1C")
		{
			$("#celdaTextoPlanoRejillaC31").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor2C")
		{
			$("#celdaTextoPlanoRejillaC2").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor2C")
		{
			$("#celdaTextoPlanoRejillaC12").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor2C")
		{
			$("#celdaTextoPlanoRejillaC35").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor2C")
		{
			$("#celdaTextoPlanoRejillaC25").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor3C")
		{
			$("#celdaTextoPlanoRejillaC3").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor3C")
		{
			$("#celdaTextoPlanoRejillaC18").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor3C")
		{
			$("#celdaTextoPlanoRejillaC34").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor3C")
		{
			$("#celdaTextoPlanoRejillaC19").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor4C")
		{
			$("#celdaTextoPlanoRejillaC7").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor4C")
		{
			$("#celdaTextoPlanoRejillaC5").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor4C")
		{
			$("#celdaTextoPlanoRejillaC30").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor4C")
		{
			$("#celdaTextoPlanoRejillaC32").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor5C")
		{
			$("#celdaTextoPlanoRejillaC8").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor5C")
		{
			$("#celdaTextoPlanoRejillaC11").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor5C")
		{
			$("#celdaTextoPlanoRejillaC29").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor5C")
		{
			$("#celdaTextoPlanoRejillaC26").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor6C")
		{
			$("#celdaTextoPlanoRejillaC9").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor6C")
		{
			$("#celdaTextoPlanoRejillaC17").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor6C")
		{
			$("#celdaTextoPlanoRejillaC28").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor6C")
		{
			$("#celdaTextoPlanoRejillaC20").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor7C")
		{
			$("#celdaTextoPlanoRejillaC13").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor7C")
		{
			$("#celdaTextoPlanoRejillaC4").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor7C")
		{
			$("#celdaTextoPlanoRejillaC24").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor7C")
		{
			$("#celdaTextoPlanoRejillaC33").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor8C")
		{
			$("#celdaTextoPlanoRejillaC14").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor8C")
		{
			$("#celdaTextoPlanoRejillaC10").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor8C")
		{
			$("#celdaTextoPlanoRejillaC23").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor8C")
		{
			$("#celdaTextoPlanoRejillaC27").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor9C")
		{
			$("#celdaTextoPlanoRejillaC15").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor9C")
		{
			$("#celdaTextoPlanoRejillaC16").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor9C")
		{
			$("#celdaTextoPlanoRejillaC22").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor9C")
		{
			$("#celdaTextoPlanoRejillaC21").css("backgroundColor", "#FDFD96");
		}
	
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	$("#informacionRejilla1C").slideToggle(500);
	$("#informacionRejilla1C").empty();
	await sleep(500);
	$("#informacionRejilla1C").append("Simulando la idea de la rejilla, las letras visibles (por los huecos) las anotamos como nuestro criptograma:");
	$("#informacionRejilla1C").slideToggle(800);
	posicion = $("#informacionRejilla1C").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(2500);
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}

	for(var i=1; i<=36; i++)
	{			
		if($("#celdaTextoPlanoRejillaC"+i).css('backgroundColor')=="rgb(253, 253, 150)")
		{			
			$("#celdaTextoPlanoRejillaC"+i).css("backgroundColor", "#77DD77");			
						
			cifrado[posArrayTextoCifrado] = $("#celdaTextoPlanoRejillaC"+i).text();
			
			cadenaCifrado = cifrado.join("");					
			
			$("#textoCifradoRejillaC3").append('<label id="abcCifradoRejilla'+posArrayTextoCifrado+'C" class="circulo">'+cifrado[posArrayTextoCifrado].toUpperCase()+'</label>');
			$("#abcCifradoRejilla"+posArrayTextoCifrado+"C").css("backgroundColor", "#77DD77");
			await sleep(250*velocidadAnimacionCifrarRejilla);
			
			$("#celdaTextoPlanoRejillaC"+i).css("backgroundColor", "#FDFD96");	
			$("#abcCifradoRejilla"+posArrayTextoCifrado+"C").css("backgroundColor", "transparent");
			posArrayTextoCifrado++;
		}
		else
		{
			$("#celdaTextoPlanoRejillaC"+i).css("color", "#FF6961");
			$("#celdaTextoPlanoRejillaC"+i).css("backgroundColor", "#FF6961");
			await sleep(20*velocidadAnimacionCifrarRejilla);
			$("#celdaTextoPlanoRejillaC"+i).css("color", "black");
			$("#celdaTextoPlanoRejillaC"+i).css("backgroundColor", "black");
			await sleep(20*velocidadAnimacionCifrarRejilla);
		}
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}		
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
	
	for(var i=0; i<3; i++)
	{
		$("#informacionRejilla1C").slideToggle(500);
		$("#informacionRejilla1C").empty();
		await sleep(500);
		$("#informacionRejilla1C").append("Giramos 90 grados nuestra rejilla hacia la derecha...");
		$("#informacionRejilla1C").slideToggle(500);
		await sleep(2000);
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
		
		for(var j=1; j<=36; j++)
		{
			$("#celdaTextoPlanoRejillaC"+j).css("backgroundColor", "black");
			$("#celdaTextoPlanoRejillaC"+j).css("color", "black");
			
			if(!seguirCifrandoRejilla)
			{
				return;
			}
		}			
		
		for(var j=0; j<auxCeldasSeleccionadasRejillaC.length; j++)
		{
			if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor1C"||auxCeldasSeleccionadasRejillaC[j]=="C1")
			{
				proxElementos.push("C6");
				$("#celdaTextoPlanoRejillaC6").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor1C"||auxCeldasSeleccionadasRejillaC[j]=="C6")
			{
				proxElementos.push("C36");
				$("#celdaTextoPlanoRejillaC36").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor1C"||auxCeldasSeleccionadasRejillaC[j]=="C36")
			{
				proxElementos.push("C31");
				$("#celdaTextoPlanoRejillaC31").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor1C"||auxCeldasSeleccionadasRejillaC[j]=="C31")
			{
				proxElementos.push("C1");
				$("#celdaTextoPlanoRejillaC1").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor2C"||auxCeldasSeleccionadasRejillaC[j]=="C2")
			{
				proxElementos.push("C12");
				$("#celdaTextoPlanoRejillaC12").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor2C"||auxCeldasSeleccionadasRejillaC[j]=="C12")
			{
				proxElementos.push("C35");
				$("#celdaTextoPlanoRejillaC35").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor2C"||auxCeldasSeleccionadasRejillaC[j]=="C35")
			{
				proxElementos.push("C25");
				$("#celdaTextoPlanoRejillaC25").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor2C"||auxCeldasSeleccionadasRejillaC[j]=="C25")
			{
				proxElementos.push("C2");
				$("#celdaTextoPlanoRejillaC2").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor3C"||auxCeldasSeleccionadasRejillaC[j]=="C3")
			{
				proxElementos.push("C18");
				$("#celdaTextoPlanoRejillaC18").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor3C"||auxCeldasSeleccionadasRejillaC[j]=="C18")
			{
				proxElementos.push("C34");
				$("#celdaTextoPlanoRejillaC34").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor3C"||auxCeldasSeleccionadasRejillaC[j]=="C34")
			{
				proxElementos.push("C19");
				$("#celdaTextoPlanoRejillaC19").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor3C"||auxCeldasSeleccionadasRejillaC[j]=="C19")
			{
				proxElementos.push("C3");
				$("#celdaTextoPlanoRejillaC3").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor4C"||auxCeldasSeleccionadasRejillaC[j]=="C7")
			{
				proxElementos.push("C5");
				$("#celdaTextoPlanoRejillaC5").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor4C"||auxCeldasSeleccionadasRejillaC[j]=="C5")
			{
				proxElementos.push("C30");
				$("#celdaTextoPlanoRejillaC30").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor4C"||auxCeldasSeleccionadasRejillaC[j]=="C30")
			{
				proxElementos.push("C32");
				$("#celdaTextoPlanoRejillaC32").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor4C"||auxCeldasSeleccionadasRejillaC[j]=="C32")
			{
				proxElementos.push("C7");
				$("#celdaTextoPlanoRejillaC7").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor5C"||auxCeldasSeleccionadasRejillaC[j]=="C8")
			{
				proxElementos.push("C11");
				$("#celdaTextoPlanoRejillaC11").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor5C"||auxCeldasSeleccionadasRejillaC[j]=="C11")
			{
				proxElementos.push("C29");
				$("#celdaTextoPlanoRejillaC29").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor5C"||auxCeldasSeleccionadasRejillaC[j]=="C29")
			{
				proxElementos.push("C26");
				$("#celdaTextoPlanoRejillaC26").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor5C"||auxCeldasSeleccionadasRejillaC[j]=="C26")
			{
				proxElementos.push("C8");
				$("#celdaTextoPlanoRejillaC8").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor6C"||auxCeldasSeleccionadasRejillaC[j]=="C9")
			{
				proxElementos.push("C17");
				$("#celdaTextoPlanoRejillaC17").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor6C"||auxCeldasSeleccionadasRejillaC[j]=="C17")
			{
				proxElementos.push("C28");
				$("#celdaTextoPlanoRejillaC28").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor6C"||auxCeldasSeleccionadasRejillaC[j]=="C28")
			{
				proxElementos.push("C20");
				$("#celdaTextoPlanoRejillaC20").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor6C"||auxCeldasSeleccionadasRejillaC[j]=="C20")
			{
				proxElementos.push("C9");
				$("#celdaTextoPlanoRejillaC9").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor7C"||auxCeldasSeleccionadasRejillaC[j]=="C13")
			{
				proxElementos.push("C4");
				$("#celdaTextoPlanoRejillaC4").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor7C"||auxCeldasSeleccionadasRejillaC[j]=="C4")
			{
				proxElementos.push("C24");
				$("#celdaTextoPlanoRejillaC24").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor7C"||auxCeldasSeleccionadasRejillaC[j]=="C24")
			{
				proxElementos.push("C33");
				$("#celdaTextoPlanoRejillaC33").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor7C"||auxCeldasSeleccionadasRejillaC[j]=="C33")
			{
				proxElementos.push("C13");
				$("#celdaTextoPlanoRejillaC13").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor8C"||auxCeldasSeleccionadasRejillaC[j]=="C14")
			{
				proxElementos.push("C10");
				$("#celdaTextoPlanoRejillaC10").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor8C"||auxCeldasSeleccionadasRejillaC[j]=="C10")
			{
				proxElementos.push("C23");
				$("#celdaTextoPlanoRejillaC23").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor8C"||auxCeldasSeleccionadasRejillaC[j]=="C23")
			{
				proxElementos.push("C27");
				$("#celdaTextoPlanoRejillaC27").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor8C"||auxCeldasSeleccionadasRejillaC[j]=="C27")
			{
				proxElementos.push("C14");
				$("#celdaTextoPlanoRejillaC14").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante1Valor9C"||auxCeldasSeleccionadasRejillaC[j]=="C15")
			{
				proxElementos.push("C16");
				$("#celdaTextoPlanoRejillaC16").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante2Valor9C"||auxCeldasSeleccionadasRejillaC[j]=="C16")
			{
				proxElementos.push("C22");
				$("#celdaTextoPlanoRejillaC22").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante3Valor9C"||auxCeldasSeleccionadasRejillaC[j]=="C22")
			{
				proxElementos.push("C21");
				$("#celdaTextoPlanoRejillaC21").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaC[j]=="Cuadrante4Valor9C"||auxCeldasSeleccionadasRejillaC[j]=="C21")
			{
				proxElementos.push("C15");
				$("#celdaTextoPlanoRejillaC15").css("backgroundColor", "#FDFD96");
			}
		
			if(!seguirCifrandoRejilla)
			{
				return;
			}
		}
		
		auxCeldasSeleccionadasRejillaC = proxElementos.slice();		
		proxElementos= [];
		
		for(var j=1; j<=36; j++)
		{
			if($("#celdaTextoPlanoRejillaC"+j).css('backgroundColor')=="rgb(253, 253, 150)")
			{			
				$("#celdaTextoPlanoRejillaC"+j).css("backgroundColor", "#77DD77");				
							
				cifrado[posArrayTextoCifrado] = $("#celdaTextoPlanoRejillaC"+j).text();
				
				cadenaCifrado = cifrado.join("");				
				
				$("#textoCifradoRejillaC3").append('<label id="abcCifradoRejilla'+posArrayTextoCifrado+'C" class="circulo">'+cifrado[posArrayTextoCifrado].toUpperCase()+'</label>');
				$("#abcCifradoRejilla"+posArrayTextoCifrado+"C").css("backgroundColor", "#77DD77");
				await sleep(250*velocidadAnimacionCifrarRejilla);							
				
				$("#celdaTextoPlanoRejillaC"+j).css("backgroundColor", "#FDFD96");
				$("#abcCifradoRejilla"+posArrayTextoCifrado+"C").css("backgroundColor", "transparent");
				posArrayTextoCifrado++;
			}
			else
			{
				$("#celdaTextoPlanoRejillaC"+j).css("color", "#FF6961");
				$("#celdaTextoPlanoRejillaC"+j).css("backgroundColor", "#FF6961");
				await sleep(20*velocidadAnimacionCifrarRejilla);
				$("#celdaTextoPlanoRejillaC"+j).css("color", "black");
				$("#celdaTextoPlanoRejillaC"+j).css("backgroundColor", "black");
				await sleep(20*velocidadAnimacionCifrarRejilla);
			}
			
			if(!seguirCifrandoRejilla)
			{
				return;
			}
		}
		
		if(!seguirCifrandoRejilla)
		{
			return;
		}
	}
	
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
	
	$("#informacionRejilla1C").slideToggle(500);
	$("#informacionRejilla1C").empty();
	await sleep(500);	
	$("#informacionRejilla1C").append("Si volvemos a girar la rejilla 90 grados hacia la derecha llega a la posición inicial por lo que entonces el cifrado ha terminado.");
	$("#informacionRejilla1C").slideToggle(500);
	posicion = $("#informacionRejilla1C").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(4000);
	
	if(!seguirCifrandoRejilla)
	{
		return;
	}
	
	posicion = $("#textoCifradoRejillaC2").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	
	$("#btn-velocidadCRejilla").show();
	$("#btn-cifrarRejilla-cifrado").show();
	$("#btn-cancelarCifrarRejilla-cifrado").hide();
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto descifrado');
	$("#textoCifradoRejillaC2").val(cadenaCifrado.toUpperCase());			
}

async function descifrarRejilla()
{
	var textoCifradoRejilla = ($("#textoCriptogramaRejillaDescifrado").val().toUpperCase().replace(/ /g,"")).split("");
	var numeroCelda= 1;
	var auxCeldasSeleccionadasRejillaD = celdasSeleccionadasRejillaD.slice();
	var proxElementos= [];
	var posTextoCifradoRejilla= 0;
	var textoDescifrado= [];
	var cadenaDescifrada;
	
	obtenerVelocidadAnimacionRejillaDescifrar();
	
	//limpiaPanelRejillaCifrado();
    $("#textoCriptogramaRejillaDescifrado").val(textoCifradoRejilla.join(""));
	
	$("#informacionRejilla1D").append("Dibujamos una tabla de 6x6 celdas:");
	$("#informacionRejilla1D").slideToggle(500);
	posicion = $("#informacionRejilla1D").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(1200);
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
	
	$("#tablaTextoCifradoRejillaD").slideToggle(500);
	posicion = $("#tablaTextoCifradoRejillaD").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(500);	
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
		
	for(var i=0; i<6; i++)
	{		
		
		$("#fDR1").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		$("#fDR2").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;			
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		$("#fDR3").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		$("#fDR4").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		$("#fDR5").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	for(var i=0; i<6; i++)
	{			
		$("#fDR6").append('<td id="celdaTextoCifradoRejillaD'+numeroCelda+'"></td>');		
		
		numeroCelda++;
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	$("#informacionRejilla1D").slideToggle(500);
	$("#informacionRejilla1D").empty();
	await sleep(500);
	$("#informacionRejilla1D").append("Colocamos la rejilla encima de la nueva tabla:");
	$("#informacionRejilla1D").slideToggle(500);
	posicion = $("#informacionRejilla1D").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(1550);
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
	
	for(var i=1; i<=36; i++)
	{
		$("#celdaTextoCifradoRejillaD"+i).css("backgroundColor", "black");
		$("#celdaTextoCifradoRejillaD"+i).css("color", "black");
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}	
	
	for(var i=0; i<celdasSeleccionadasRejillaD.length; i++)
	{
		if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor1D")
		{
			$("#celdaTextoCifradoRejillaD1").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor1D")
		{
			$("#celdaTextoCifradoRejillaD6").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor1D")
		{
			$("#celdaTextoCifradoRejillaD36").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor1D")
		{
			$("#celdaTextoCifradoRejillaD31").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor2D")
		{
			$("#celdaTextoCifradoRejillaD2").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor2D")
		{
			$("#celdaTextoCifradoRejillaD12").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor2D")
		{
			$("#celdaTextoCifradoRejillaD35").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor2D")
		{
			$("#celdaTextoCifradoRejillaD25").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor3D")
		{
			$("#celdaTextoCifradoRejillaD3").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor3D")
		{
			$("#celdaTextoCifradoRejillaD18").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor3D")
		{
			$("#celdaTextoCifradoRejillaD34").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor3D")
		{
			$("#celdaTextoCifradoRejillaD19").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor4D")
		{
			$("#celdaTextoCifradoRejillaD7").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor4D")
		{
			$("#celdaTextoCifradoRejillaD5").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor4D")
		{
			$("#celdaTextoCifradoRejillaD30").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor4D")
		{
			$("#celdaTextoCifradoRejillaD32").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor5D")
		{
			$("#celdaTextoCifradoRejillaD8").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor5D")
		{
			$("#celdaTextoCifradoRejillaD11").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor5D")
		{
			$("#celdaTextoCifradoRejillaD29").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor5D")
		{
			$("#celdaTextoCifradoRejillaD26").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor6D")
		{
			$("#celdaTextoCifradoRejillaD9").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor6D")
		{
			$("#celdaTextoCifradoRejillaD17").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor6D")
		{
			$("#celdaTextoCifradoRejillaD28").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor6D")
		{
			$("#celdaTextoCifradoRejillaD20").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor7D")
		{
			$("#celdaTextoCifradoRejillaD13").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor7D")
		{
			$("#celdaTextoCifradoRejillaD4").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor7D")
		{
			$("#celdaTextoCifradoRejillaD24").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor7D")
		{
			$("#celdaTextoCifradoRejillaD33").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor8D")
		{
			$("#celdaTextoCifradoRejillaD14").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor8D")
		{
			$("#celdaTextoCifradoRejillaD10").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor8D")
		{
			$("#celdaTextoCifradoRejillaD23").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor8D")
		{
			$("#celdaTextoCifradoRejillaD27").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante1Valor9D")
		{
			$("#celdaTextoCifradoRejillaD15").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante2Valor9D")
		{
			$("#celdaTextoCifradoRejillaD16").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante3Valor9D")
		{
			$("#celdaTextoCifradoRejillaD22").css("backgroundColor", "#FDFD96");
		}
		else if(celdasSeleccionadasRejillaD[i]=="Cuadrante4Valor9D")
		{
			$("#celdaTextoCifradoRejillaD21").css("backgroundColor", "#FDFD96");
		}
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}	
	
	for(var i=0; i<4; i++)
	{
		if(i==0)
		{
			$("#informacionRejilla1D").slideToggle(500);		
			$("#informacionRejilla1D").empty();
			await sleep(500);
			$("#informacionRejilla1D").append("Por cada hueco de la rejilla, recorriendo fila por fila, vamos escribiendo letra por letra del criptograma:");
			$("#informacionRejilla1D").slideToggle(500);
			posicion = $("#informacionRejilla1D").offset().top;
			$("html, body").animate({scrollTop: posicion}, 1000);		
			await sleep(2900);
		}
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
		
		for(var j=1; j<=36; j++)
		{
			if($("#celdaTextoCifradoRejillaD"+j).css('backgroundColor')=="rgb(253, 253, 150)")
			{
				 $("#celdaTextoCifradoRejillaD"+j).html(textoCifradoRejilla[posTextoCifradoRejilla]);
				 posTextoCifradoRejilla++;
				 await sleep(250*velocidadAnimacionDescifrarRejilla);
			}
			
			if(!seguirDescifrandoRejilla)
			{
				return;
			}
		}			
		
		for(var j=1; j<=36; j++)
		{
			$("#celdaTextoCifradoRejillaD"+j).css("backgroundColor", "black");
			$("#celdaTextoCifradoRejillaD"+j).css("color", "black");
			
			if(!seguirDescifrandoRejilla)
			{
				return;
			}
		}			
		
		for(var j=0; j<auxCeldasSeleccionadasRejillaD.length; j++)
		{
			if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor1D"||auxCeldasSeleccionadasRejillaD[j]=="C1")
			{
				proxElementos.push("C6");
				$("#celdaTextoCifradoRejillaD6").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor1D"||auxCeldasSeleccionadasRejillaD[j]=="C6")
			{
				proxElementos.push("C36");
				$("#celdaTextoCifradoRejillaD36").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor1D"||auxCeldasSeleccionadasRejillaD[j]=="C36")
			{
				proxElementos.push("C31");
				$("#celdaTextoCifradoRejillaD31").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor1D"||auxCeldasSeleccionadasRejillaD[j]=="C31")
			{
				proxElementos.push("C1");
				$("#celdaTextoCifradoRejillaD1").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor2D"||auxCeldasSeleccionadasRejillaD[j]=="C2")
			{
				proxElementos.push("C12");
				$("#celdaTextoCifradoRejillaD12").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor2D"||auxCeldasSeleccionadasRejillaD[j]=="C12")
			{
				proxElementos.push("C35");
				$("#celdaTextoCifradoRejillaD35").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor2D"||auxCeldasSeleccionadasRejillaD[j]=="C35")
			{
				proxElementos.push("C25");
				$("#celdaTextoCifradoRejillaD25").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor2D"||auxCeldasSeleccionadasRejillaD[j]=="C25")
			{
				proxElementos.push("C2");
				$("#celdaTextoCifradoRejillaD2").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor3D"||auxCeldasSeleccionadasRejillaD[j]=="C3")
			{
				proxElementos.push("C18");
				$("#celdaTextoCifradoRejillaD18").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor3D"||auxCeldasSeleccionadasRejillaD[j]=="C18")
			{
				proxElementos.push("C34");
				$("#celdaTextoCifradoRejillaD34").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor3D"||auxCeldasSeleccionadasRejillaD[j]=="C34")
			{
				proxElementos.push("C19");
				$("#celdaTextoCifradoRejillaD19").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor3D"||auxCeldasSeleccionadasRejillaD[j]=="C19")
			{
				proxElementos.push("C3");
				$("#celdaTextoCifradoRejillaD3").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor4D"||auxCeldasSeleccionadasRejillaD[j]=="C7")
			{
				proxElementos.push("C5");
				$("#celdaTextoCifradoRejillaD5").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor4D"||auxCeldasSeleccionadasRejillaD[j]=="C5")
			{
				proxElementos.push("C30");
				$("#celdaTextoCifradoRejillaD30").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor4D"||auxCeldasSeleccionadasRejillaD[j]=="C30")
			{
				proxElementos.push("C32");
				$("#celdaTextoCifradoRejillaD32").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor4D"||auxCeldasSeleccionadasRejillaD[j]=="C32")
			{
				proxElementos.push("C7");
				$("#celdaTextoCifradoRejillaD7").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor5D"||auxCeldasSeleccionadasRejillaD[j]=="C8")
			{
				proxElementos.push("C11");
				$("#celdaTextoCifradoRejillaD11").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor5D"||auxCeldasSeleccionadasRejillaD[j]=="C11")
			{
				proxElementos.push("C29");
				$("#celdaTextoCifradoRejillaD29").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor5D"||auxCeldasSeleccionadasRejillaD[j]=="C29")
			{
				proxElementos.push("C26");
				$("#celdaTextoCifradoRejillaD26").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor5D"||auxCeldasSeleccionadasRejillaD[j]=="C26")
			{
				proxElementos.push("C8");
				$("#celdaTextoCifradoRejillaD8").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor6D"||auxCeldasSeleccionadasRejillaD[j]=="C9")
			{
				proxElementos.push("C17");
				$("#celdaTextoCifradoRejillaD17").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor6D"||auxCeldasSeleccionadasRejillaD[j]=="C17")
			{
				proxElementos.push("C28");
				$("#celdaTextoCifradoRejillaD28").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor6D"||auxCeldasSeleccionadasRejillaD[j]=="C28")
			{
				proxElementos.push("C20");
				$("#celdaTextoCifradoRejillaD20").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor6D"||auxCeldasSeleccionadasRejillaD[j]=="C20")
			{
				proxElementos.push("C9");
				$("#celdaTextoCifradoRejillaD9").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor7D"||auxCeldasSeleccionadasRejillaD[j]=="C13")
			{
				proxElementos.push("C4");
				$("#celdaTextoCifradoRejillaD4").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor7D"||auxCeldasSeleccionadasRejillaD[j]=="C4")
			{
				proxElementos.push("C24");
				$("#celdaTextoCifradoRejillaD24").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor7D"||auxCeldasSeleccionadasRejillaD[j]=="C24")
			{
				proxElementos.push("C33");
				$("#celdaTextoCifradoRejillaD33").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor7D"||auxCeldasSeleccionadasRejillaD[j]=="C33")
			{
				proxElementos.push("C13");
				$("#celdaTextoCifradoRejillaD13").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor8D"||auxCeldasSeleccionadasRejillaD[j]=="C14")
			{
				proxElementos.push("C10");
				$("#celdaTextoCifradoRejillaD10").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor8D"||auxCeldasSeleccionadasRejillaD[j]=="C10")
			{
				proxElementos.push("C23");
				$("#celdaTextoCifradoRejillaD23").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor8D"||auxCeldasSeleccionadasRejillaD[j]=="C23")
			{
				proxElementos.push("C27");
				$("#celdaTextoCifradoRejillaD27").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor8D"||auxCeldasSeleccionadasRejillaD[j]=="C27")
			{
				proxElementos.push("C14");
				$("#celdaTextoCifradoRejillaD14").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante1Valor9D"||auxCeldasSeleccionadasRejillaD[j]=="C15")
			{
				proxElementos.push("C16");
				$("#celdaTextoCifradoRejillaD16").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante2Valor9D"||auxCeldasSeleccionadasRejillaD[j]=="C16")
			{
				proxElementos.push("C22");
				$("#celdaTextoCifradoRejillaD22").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante3Valor9D"||auxCeldasSeleccionadasRejillaD[j]=="C22")
			{
				proxElementos.push("C21");
				$("#celdaTextoCifradoRejillaD21").css("backgroundColor", "#FDFD96");
			}
			else if(auxCeldasSeleccionadasRejillaD[j]=="Cuadrante4Valor9D"||auxCeldasSeleccionadasRejillaD[j]=="C21")
			{
				proxElementos.push("C15");
				$("#celdaTextoCifradoRejillaD15").css("backgroundColor", "#FDFD96");
			}
			
			if(!seguirDescifrandoRejilla)
			{
				return;
			}
		}
		
		auxCeldasSeleccionadasRejillaD = proxElementos.slice();		
		proxElementos= [];
		
		if(i<3)
		{
			$("#informacionRejilla1D").slideToggle(500);		
			$("#informacionRejilla1D").empty();
			await sleep(500);
			$("#informacionRejilla1D").append("Giramos 90 grados hacia la derecha la rejilla...");
			$("#informacionRejilla1D").slideToggle(500);
			posicion = $("#informacionRejilla1D").offset().top;
			$("html, body").animate({scrollTop: posicion}, 1000);
			await sleep(1750);
		}
		else
		{
			$("#informacionRejilla1D").slideToggle(500);		
			$("#informacionRejilla1D").empty();
			await sleep(500);
			$("#informacionRejilla1D").append("Volvemos a girar la rejilla 90 grados hacia la derecha y llegamos a la posición inicial por lo que entonces el descifrado ha terminado.");
			$("#informacionRejilla1D").slideToggle(500);
			posicion = $("#informacionRejilla1D").offset().top;
			$("html, body").animate({scrollTop: posicion}, 1000);
			await sleep(4000);
		}
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
	
	$("#informacionRejilla1D").slideToggle(500);		
	$("#informacionRejilla1D").empty();
	await sleep(500);
	$("#informacionRejilla1D").append("Quitamos la rejilla y tenemos nuestro mensaje claro:");
	$("#informacionRejilla1D").slideToggle(500);
	posicion = $("#informacionRejilla1D").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	await sleep(1750);
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
	
	for(var i=1; i<=36; i++)
	{
		$("#celdaTextoCifradoRejillaD"+i).css("backgroundColor", "#FDFD96");
		$("#celdaTextoCifradoRejillaD"+i).css("color", "black");
		textoDescifrado[i-1] = $("#celdaTextoCifradoRejillaD"+i).text();
		cadenaDescifrada = textoDescifrado.join("");
		
		if(!seguirDescifrandoRejilla)
		{
			return;
		}
	}
	
	posicion = $("#textoDescifradoRejillaD2").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	
	if(!seguirDescifrandoRejilla)
	{
		return;
	}
	
	$("#btn-velocidadDRejilla").show();
	$("#btn-descifrarRejilla-descifrado").show();
	$("#btn-cancelarDescifrarRejilla-descifrado").hide();
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto descifrado');
	$("#textoDescifradoRejillaD2").val(cadenaDescifrada.toLowerCase());
}

function validarEntradaCifradoRejilla()
{
	var mensaje = "";
	var texto = $('#textoPlanoRejillaC').val().replace(/ /g,"");

	if (texto.length < 1 || texto.length > 36) {
		mensaje = "El mensaje claro debe contener entre 1 y 36 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El mensaje claro sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}

	return mensaje;
}

function validarRejillaCifrado()
{
	var mensaje= "";
	
	if(celdasSeleccionadasRejillaC.length<9)
	{
		mensaje= "Debes seleccionar 9 celdas para formar la rejilla."
	}
	
	return mensaje;
}

function validarEntradaDescifradoRejilla()
{
	var mensaje = "";
	var texto = $('#textoCriptogramaRejillaDescifrado').val().replace(/ /g,"");
	
	if (texto.length!=36)
	{
		mensaje = "El criptograma debe contener 36 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El criptograma sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}

	return mensaje;
}

function validarRejillaDescifrado()
{
	var mensaje= "";
	
	if(celdasSeleccionadasRejillaD.length<9)
	{
		mensaje= "Debes seleccionar 9 celdas para formar la rejilla."
	}
	
	return mensaje;
}

$(document).ready(function()
{
	$("#CifradoRapidoRejilla").click(function(){
		$("#btn-cifrarRejilla-cifrado").html('Cifrado Rápido');
		$("#btn-cifrarAtbash-cifrado").val(1);
	});
	$("#CifradoNormalRejilla").click(function(){
		$("#btn-cifrarRejilla-cifrado").html('Cifrado Normal');
		$("#btn-cifrarRejilla-cifrado").val(2);
	});
	$("#CifradoLentoRejilla").click(function(){
		$("#btn-cifrarRejilla-cifrado").html('Cifrado Lento');
		$("#btn-cifrarRejilla-cifrado").val(3);
	});
	
	$("#DescifradoRapidoRejilla").click(function(){
		$("#btn-descifrarRejilla-descifrado").html('Descifrado Rápido');
		$("#btn-descifrarRejilla-descifrado").val(1);
	});
	$("#DescifradoNormalRejilla").click(function(){
		$("#btn-descifrarRejilla-descifrado").html('Descifrado Normal');
		$("#btn-descifrarRejilla-descifrado").val(2);
	});
	$("#DescifradoLentoRejilla").click(function(){
		$("#btn-descifrarRejilla-descifrado").html('Descifrado Lento');
		$("#btn-descifrarRejilla-descifrado").val(3);
	});
	
	$("#textoPlanoRejillaC").keyup(function()
	{
		var mensaje = validarEntradaCifradoRejilla();

		if (mensaje.length != 0) {
			$("#textoPlanoRejillaC-error").remove();
			$("#textoPlanoRejillaC").parent().parent().append('<div id="textoPlanoRejillaC-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoPlanoRejillaC").parent().parent().addClass('has-error has-feedback');
			$("#btn-cifrarRejilla-cifrado").attr("disabled", true);
		} else{
			$("#textoPlanoRejillaC-error").remove();
			$("#textoPlanoRejillaC").parent().parent().removeClass('has-error has-feedback');
			$("#btn-cifrarRejilla-cifrado").attr("disabled", false);
		}
	});
	
	$("#textoCriptogramaRejillaDescifrado").keyup(function(){
		var res = validarEntradaDescifradoRejilla();	
		
		if (res.length != 0)
		{
			$("#textoCriptogramaRejillaDescifrado-error").remove();
			$("#textoCriptogramaRejillaDescifrado").parent().parent().append('<div id="textoCriptogramaRejillaDescifrado-error" class="text-danger">&nbsp;'+res+'</div>');
			$("#textoCriptogramaRejillaDescifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-descifrarRejilla-descifrado").attr("disabled", true);
		} else
		{
			$("#textoCriptogramaRejillaDescifrado-error").remove();
			$("#textoCriptogramaRejillaDescifrado").parent().parent().removeClass('has-error has-feedback');
			$("#btn-descifrarRejilla-descifrado").attr("disabled", false);
		}
	});
	
	$("#btn-cifrarRejilla-cifrado").click(function()
	{
		var mensaje= validarEntradaCifradoRejilla();
		var mensaje2= validarRejillaCifrado();
		
		if(mensaje.length!=0)
		{
			$("#textoPlanoRejillaC-error").remove();
			$("#textoPlanoRejillaC").parent().parent().append('<div id="textoPlanoRejillaC-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoPlanoRejillaC").parent().parent().addClass('has-error has-feedback');
			$("#btn-cifrarRejilla-cifrado").attr("disabled", true);
		}
		else
		{
			if(mensaje2.length!=0)
			{
				$("#textoPlanoRejillaC-error").remove();
				$("#textoPlanoRejillaC").parent().parent().append('<div id="textoPlanoRejillaC-error" class="text-danger">&nbsp;'+mensaje2+'</div>');
				$("#textoPlanoRejillaC").parent().parent().addClass('has-error has-feedback');			
			}
			else
			{
				$("#textoPlanoRejillaC-error").remove();
				$("#textoPlanoRejillaC").parent().parent().removeClass('has-error has-feedback');
				cifrarRejilla();
			}					
		}		
	});
	
	$("#btn-descifrarRejilla-descifrado").click(function()
	{
		var mensaje= validarEntradaDescifradoRejilla();
		var mensaje2= validarRejillaDescifrado();
		
		if(mensaje.length!=0)
		{
			$("#textoCriptogramaRejillaDescifrado-error").remove();
			$("#textoCriptogramaRejillaDescifrado").parent().parent().append('<div id="textoCriptogramaRejillaDescifrado-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoCriptogramaRejillaDescifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-descifrarRejilla-descifrado").attr("disabled", true);
		}
		else
		{
			if(mensaje2.length!=0)
			{
				$("#textoCriptogramaRejillaDescifrado-error").remove();
			$("#textoCriptogramaRejillaDescifrado").parent().parent().append('<div id="textoCriptogramaRejillaDescifrado-error" class="text-danger">&nbsp;'+mensaje2+'</div>');
			$("#textoCriptogramaRejillaDescifrado").parent().parent().addClass('has-error has-feedback');			
			}
			else
			{
				$("#textoCriptogramaRejillaDescifrado-error").remove();
				$("#textoCriptogramaRejillaDescifrado").parent().parent().removeClass('has-error has-feedback');
				descifrarRejilla();
			}					
		}		
	});
	
	$("#btn-cancelarCifrarRejilla-cifrado").click(function()
	{
		seguirCifrandoRejilla= false;
		
		limpiaPanelRejillaCifrado();

		$("#btn-velocidadCRejilla").show();
		$("#btn-cifrarRejilla-cifrado").show();
		$("#btn-cancelarCifrarRejilla-cifrado").hide();
	});
	
	$("#btn-cancelarDescifrarRejilla-descifrado").click(function()
	{
		seguirDescifrandoRejilla= false;
		
		limpiaPanelRejillaDescifrado();

		$("#btn-velocidadDRejilla").show();
		$("#btn-descifrarRejilla-descifrado").show();
		$("#btn-cancelarDescifrarRejilla-descifrado").hide();
	});
	
	$("#btn-copiarTextoRejilla").click(function()
	{
		if ($("#textoCifradoRejillaC2").val()=='')
		{
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		}
		else
		{
			$("#textoCriptogramaRejillaDescifrado").val($("#textoCifradoRejillaC2").val());
			
			for(var i=0; i<celdasSeleccionadasRejillaC.length; i++)
			{
				if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor1C")
				{
					$("#Cuadrante1Valor1D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor1D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor1C")
				{
					$("#Cuadrante2Valor1D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor1D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor1C")
				{
					$("#Cuadrante3Valor1D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor1D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor1C")
				{
					$("#Cuadrante4Valor1D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor1D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor2C")
				{
					$("#Cuadrante1Valor2D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor2D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor2C")
				{
					$("#Cuadrante2Valor2D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor2D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor2C")
				{
					$("#Cuadrante3Valor2D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor2D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor2C")
				{
					$("#Cuadrante4Valor2D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor2D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor3C")
				{
					$("#Cuadrante1Valor3D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor3D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor3C")
				{
					$("#Cuadrante2Valor3D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor3D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor3C")
				{
					$("#Cuadrante3Valor3D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor3D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor3C")
				{
					$("#Cuadrante4Valor3D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor3D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor4C")
				{
					$("#Cuadrante1Valor4D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor4D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor4C")
				{
					$("#Cuadrante2Valor4D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor4D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor4C")
				{
					$("#Cuadrante3Valor4D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor4D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor4C")
				{
					$("#Cuadrante4Valor4D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor4D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor5C")
				{
					$("#Cuadrante1Valor5D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor5D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor5C")
				{
					$("#Cuadrante2Valor5D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor5D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor5C")
				{
					$("#Cuadrante3Valor5D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor5D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor5C")
				{
					$("#Cuadrante4Valor5D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor5D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor6C")
				{
					$("#Cuadrante1Valor6D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor6D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor6C")
				{
					$("#Cuadrante2Valor6D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor6D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor6C")
				{
					$("#Cuadrante3Valor6D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor6D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor6C")
				{
					$("#Cuadrante4Valor6D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor6D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor7C")
				{
					$("#Cuadrante1Valor7D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor7D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor7C")
				{
					$("#Cuadrante2Valor7D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor7D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor7C")
				{
					$("#Cuadrante3Valor7D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor7D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor7C")
				{
					$("#Cuadrante4Valor7D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor7D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor8C")
				{
					$("#Cuadrante1Valor8D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor8D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor8C")
				{
					$("#Cuadrante2Valor8D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor8D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor8C")
				{
					$("#Cuadrante3Valor8D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor8D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor8C")
				{
					$("#Cuadrante4Valor8D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor8D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante1Valor9C")
				{
					$("#Cuadrante1Valor9D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante1Valor9D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante2Valor9C")
				{
					$("#Cuadrante2Valor9D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante2Valor9D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante3Valor9C")
				{
					$("#Cuadrante3Valor9D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante3Valor9D");
				}
				else if(celdasSeleccionadasRejillaC[i]=="Cuadrante4Valor9C")
				{
					$("#Cuadrante4Valor9D").css("backgroundColor", "#FDFD96");
					celdasSeleccionadasRejillaD.push("Cuadrante4Valor9D");
				}						
			
				if(!seguirCifrandoRejilla)
				{
					return;
				}
			}
		}
	});	   
});