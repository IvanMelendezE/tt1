function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var ic=0, idc=0;
var tCAdd=1000, tDcAdd=1000;
var tCRemove=500, tDcRemove=500;

function mostrarPanelPlayfair(){
	crearPanelPlayfair();
	$("#pnl-InteractivoPlayfair").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelPlayfair(){
	$("#pnl-InteractivoPlayfair").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelPlayfair();
}

function crearPanelPlayfair(){
	for (var i = 1; i <= 5; i++) {
		$("#m"+i+"c").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"d").append('<td><label class="circulo">|</label></td>');
		for (var j = 1; j <= 5; j++) {
			$("#m"+i+"c").append('<td><label id="m'+i+j+'c" class="circulo" fila="'+i+'" columna="'+j+'">[]</label></td>');
			$("#m"+i+"d").append('<td><label id="m'+i+j+'d" class="circulo" fila="'+i+'" columna="'+j+'">[]</label></td>');
		}
		$("#m"+i+"c").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"d").append('<td><label class="circulo">|</label></td>');
	}
}

function limpiaPanelPlayfair(){
	$("#textoPlanoPlayfairC").empty();
	$("#textoCifradoPlayfairC").empty();
	$("#textoPlanoPlayfairD").empty();
	$("#textoCifradoPlayfairD").empty();
	$("#in-txtPlanoPlayfair").val("");
	$("#out-txtCifradoPlayfair").val("");
	$("#in-txtCifradoPlayfair").val("");
	$("#out-txtPlanoPlayfair").val("");
	$("#in-keyPlayfairC").val("");
	$("#in-keyPlayfairD").val("");
    $("#infoAnimacionCiPlayfair").hide();
    $("#infoAnimacionCiPlayfairExtra").hide();
    $("#infoAnimacionDePlayfair").hide();
    $("#infoAnimacionDePlayfairExtra").hide();
    $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
    $("#txtPlanoPlayfair-error").remove();
    $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
    $("#keyPlayfairC-error").remove();
    $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoPlayfair-error").remove();
    $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
    $("#keyPlayfairD-error").remove();
    $("#btn-cifrarPlayfair").show();
    $("#btn-tipoCiPlayfair").show();
    $("#btn-cancelarCifrarPlayfair").hide();
    $("#btn-descifrarPlayfair").show();
    $("#btn-tipoDePlayfair").show();
    $("#btn-cancelarDescifrarPlayfair").hide();
	for (var i = 1; i <= 5; i++) {
		$("#m"+i+"c").empty();
		$("#m"+i+"d").empty();
	}
}

function pararAnimacionPlayfair(){
    ic=999;
    idc=999;
    $("#btn-copiarTextoPlayfair").removeAttr("disabled");
    $("#textoPlanoPlayfairC").empty();
    $("#textoCifradoPlayfairC").empty();
    $("#textoPlanoPlayfairD").empty();
    $("#textoCifradoPlayfairD").empty();
    $("#infoAnimacionCiPlayfair").hide();
    $("#infoAnimacionCiPlayfairExtra").hide();
    $("#infoAnimacionDePlayfair").hide();
    $("#infoAnimacionDePlayfairExtra").hide();
    $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
    $("#txtPlanoPlayfair-error").remove();
    $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
    $("#keyPlayfairC-error").remove();
    $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoPlayfair-error").remove();
    $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
    $("#keyPlayfairD-error").remove();
    $("#btn-cifrarPlayfair").show();
    $("#btn-tipoCiPlayfair").show();
    $("#btn-cancelarCifrarPlayfair").hide();
    $("#btn-descifrarPlayfair").show();
    $("#btn-tipoDePlayfair").show();
    $("#btn-cancelarDescifrarPlayfair").hide();
    for (var i = 1; i <= 5; i++) {
        $("#m"+i+"c").empty();
        $("#m"+i+"d").empty();
    }
    crearPanelPlayfair();
}

async function cifrarPlayfair(){
    var planoC = ($("#in-txtPlanoPlayfair").val().toUpperCase()).split("");
    var key = ($("#in-keyPlayfairC").val().toUpperCase()).split("");
    var cifrado = [];
    var matriz = [];
    var cadenaCifrado;
    var filaL1, filaL2, filaL1Cifrada, filaL2Cifrada;
    var columnaL1, columnaL2, columnaL1Cifrada, columnaL2Cifrada;
    var inmC = 1;
    var aux = 0;
    ic = 0;
    for (var i = 0; i <= planoC.length-1; i++) {
        if(planoC[i] == ' ') {
           planoC.splice(i, 1);
        }
    }
    for (var i = 0; i <= key.length-1; i++) {
        if(key[i] == ' ') {
           key.splice(i, 1);
        }
    }
    $("#infoAnimacionCiPlayfair").fadeIn();
    for (var i = 0; i <= planoC.length-1; i++) {
        if (typeof planoC[i+1] != 'undefined') {
            if (planoC[i]==planoC[i+1]) {
                planoC.splice(i+1, 0, 'X');
                $("#infoAnimacionCiPlayfairExtra").html('<i class="fa fa-info-circle"></i>&nbsp;Para evitar errores, insertar un carácter sin significado (<strong>x</strong>) entre las letras repertidas');
                aux=1;
            }
        }
    }
    if (aux==1) {
        $("#infoAnimacionCiPlayfairExtra").fadeIn();
        await sleep(tCAdd);
    }
    if (planoC.length%2==1) {
        planoC[planoC.length]='X';
        $("#infoAnimacionCiPlayfairExtra").append('<br>&nbsp;El texto no es impar, por ello se debe insertar un carácter sin significado al final (<strong>x</strong>)');
    }
    for (var m = 0; m <= planoC.length-1; m++) {
		$("#textoPlanoPlayfairC").append('<label id="abcPlano'+m+'C" class="circulo">'+planoC[m].toLowerCase()+'</label>');
    }
    for (var n = 0; n <= planoC.length-1; n++) {
		$("#textoCifradoPlayfairC").append('<label id="abcCifrado'+n+'C" class="circulo">&nbsp;</label>');
    }
    // ANIMATION
    $("#textoPlanoPlayfairC").addClass('parpadeo');
    await sleep(tCAdd);
    $("#textoPlanoPlayfairC").removeClass('parpadeo');
    await sleep(tCRemove);
    // END ANIMATION
    for (var k = 0; k <= key.length-1; k++) {
    	if (jQuery.inArray(key[k],matriz)==-1) {
            if (key[k]=='J'){key[k]='I';}
    		matriz[inmC] = key[k];
    		inmC++;
    	}
    }
    for (var l = 65; l <= 90; l++) {
    	if (jQuery.inArray(String.fromCharCode(l),matriz)==-1 && String.fromCharCode(l)!='J') {
    		matriz[inmC] = String.fromCharCode(l);
    		inmC++;
    	}
    }
    inmC = 1;
    for (var i = 1; i <=5; i++) {
    	for (var j = 1; j <= 5; j++) {
    		$("#m"+i+j+"c").html(matriz[inmC]);
    		inmC++;
    	}
    }
    while (ic <= planoC.length-1) {
		if (planoC[ic] == 'J'){
			planoC[ic]='I';
		}
		if (planoC[ic+1] == 'J'){
			planoC[ic+1]='I';
		}
		filaL1 = $("#matrizPlano label:contains("+planoC[ic]+")").attr("fila");
		columnaL1 = $("#matrizPlano label:contains("+planoC[ic]+")").attr("columna");
		filaL2 = $("#matrizPlano label:contains("+planoC[ic+1]+")").attr("fila")
		columnaL2 = $("#matrizPlano label:contains("+planoC[ic+1]+")").attr("columna");
		if (filaL1 == filaL2 && columnaL1 != columnaL2) {
			columnaL1Cifrada=Number(columnaL1)+1;
			filaL1Cifrada=filaL1;
			columnaL2Cifrada=Number(columnaL2)+1;
			filaL2Cifrada=filaL2;
			if (columnaL1Cifrada>5) {columnaL1Cifrada=columnaL1Cifrada-5;}
			if (columnaL2Cifrada>5) {columnaL2Cifrada=columnaL2Cifrada-5;}
		} else if (filaL1 != filaL2 && columnaL1 == columnaL2) {
			filaL1Cifrada=Number(filaL1)+1;
			columnaL1Cifrada=columnaL1;
			filaL2Cifrada=Number(filaL2)+1;
			columnaL2Cifrada=columnaL2;
			if (filaL1Cifrada>5) {filaL1Cifrada=filaL1Cifrada-5;}
			if (filaL2Cifrada>5) {filaL2Cifrada=filaL2Cifrada-5;}
		} else if (filaL1 != filaL2 && columnaL1 != columnaL2) {
			filaL1Cifrada=filaL1;
			columnaL1Cifrada=columnaL2;
			filaL2Cifrada=filaL2;
			columnaL2Cifrada=columnaL1;
		}
		cifrado[ic]=$("#m"+filaL1Cifrada+columnaL1Cifrada+"c").text();
		cifrado[ic+1]=$("#m"+filaL2Cifrada+columnaL2Cifrada+"c").text();
        if (filaL1 == filaL2 && columnaL1 != columnaL2) {
            $("#infoCiPlayfair").html('<br>Por la <strong>Regla No.1</strong> <strong>'+planoC[ic].toLowerCase()+'</strong> y <strong>'+planoC[ic+1].toLowerCase()+'</strong> se encuentran en la misma fila, por lo tanto se escogen <strong>'+cifrado[ic]+'</strong> y <strong>'+cifrado[ic+1]+'</strong> que estan situados a su derecha');
        } else if (filaL1 != filaL2 && columnaL1 == columnaL2) {
            $("#infoCiPlayfair").html('<br>Por la <strong>Regla No.2</strong> <strong>'+planoC[ic].toLowerCase()+'</strong> y <strong>'+planoC[ic+1].toLowerCase()+'</strong> se encuentran en la misma columna, por lo tanto se escogen <strong>'+cifrado[ic]+'</strong> y <strong>'+cifrado[ic+1]+'</strong> que estan situados debajo');
        } else if (filaL1 != filaL2 && columnaL1 != columnaL2) {
            $("#infoCiPlayfair").html('<br>Por la <strong>Regla No.3</strong> <strong>'+planoC[ic].toLowerCase()+'</strong> y <strong>'+planoC[ic+1].toLowerCase()+'</strong> se encuentran en distintas filas y columnas, por lo tanto se escogen <strong>'+cifrado[ic]+'</strong> y <strong>'+cifrado[ic+1]+'</strong> que estan situados en la diagonal opuesta');
        }
		$("#abcCifrado"+ic+"C").html(cifrado[ic]).show();
		$("#abcCifrado"+(ic+1)+"C").html(cifrado[ic+1]).show();
    	
    	// ANIMATION
    	$("#abcPlano"+ic+"C").addClass('parpadeo');
    	$("#abcPlano"+(ic+1)+"C").addClass('parpadeo');
    	$("#matrizPlano label:contains("+planoC[ic]+")").addClass("parpadeo");
		$("#matrizPlano label:contains("+planoC[ic+1]+")").addClass("parpadeo");
		$("#m"+filaL1Cifrada+columnaL1Cifrada+"c").addClass("parpadeoNext");
		$("#m"+filaL2Cifrada+columnaL2Cifrada+"c").addClass("parpadeoNext");
    	$("#abcCifrado"+ic+"C").addClass('parpadeoNext');
    	$("#abcCifrado"+(ic+1)+"C").addClass('parpadeoNext');
    	await sleep(tCAdd);
    	$("#abcPlano"+ic+"C").removeClass('parpadeo');
    	$("#abcPlano"+(ic+1)+"C").removeClass('parpadeo');
    	$("#matrizPlano label:contains("+planoC[ic]+")").removeClass("parpadeo");
		$("#matrizPlano label:contains("+planoC[ic+1]+")").removeClass("parpadeo");
		$("#m"+filaL1Cifrada+columnaL1Cifrada+"c").removeClass("parpadeoNext");
		$("#m"+filaL2Cifrada+columnaL2Cifrada+"c").removeClass("parpadeoNext");
    	$("#abcCifrado"+ic+"C").removeClass('parpadeoNext');
    	$("#abcCifrado"+(ic+1)+"C").removeClass('parpadeoNext');
    	await sleep(tCRemove);
    	// END ANIMATION
    	ic=ic+2;
    }
    if (ic <= planoC.length && ic!=999) {
        cadenaCifrado = cifrado.join("");
        $("#out-txtCifradoPlayfair").val(cadenaCifrado);
        toastr.options.timeOut = "1000";
    	toastr['success']('Texto cifrado');
        $("#btn-cifrarPlayfair").show();
        $("#btn-tipoCiPlayfair").show();
        $("#btn-cancelarCifrarPlayfair").hide();
    }
}

async function descifrarPlayfair(){
    var cifradoD = ($("#in-txtCifradoPlayfair").val().toUpperCase()).split("");
    var key = ($("#in-keyPlayfairD").val().toUpperCase()).split("");
    var plano = [];
    var matriz = [];
    var cadenaDecifrado;
    var filaL1, filaL2, filaL1Decifrada, filaL2Decifrada;
    var columnaL1, columnaL2, columnaL1Decifrada, columnaL2Decifrada;
    var inmD = 1;
    idc = 0;
    for (var i = 0; i <= cifradoD.length-1; i++) {
        if(cifradoD[i] == ' ') {
           cifradoD.splice(i, 1);
        }
    }
    for (var i = 0; i <= key.length-1; i++) {
        if(key[i] == ' ') {
           key.splice(i, 1);
        }
    }
    $("#infoAnimacionDePlayfair").fadeIn();
    for (var m = 0; m <= cifradoD.length-1; m++) {
		$("#textoCifradoPlayfairD").append('<label id="abcCifrado'+m+'D" class="circulo">'+cifradoD[m]+'</label>');
    }
    for (var n = 0; n <= cifradoD.length-1; n++) {
		$("#textoPlanoPlayfairD").append('<label id="abcPlano'+n+'D" class="circulo">&nbsp;</label>');
    }
    // ANIMATION
    $("#textoCifradoPlayfairD").addClass('parpadeo');
    await sleep(tDcAdd);
    $("#textoCifradoPlayfairD").removeClass('parpadeo');
    await sleep(tDcRemove);
    // END ANIMATION
    for (var k = 0; k <= key.length-1; k++) {
    	if (jQuery.inArray(key[k],matriz)==-1) {
            if (key[k]=='J'){key[k]='I';}
    		matriz[inmD] = key[k];
    		inmD++;
    	}
    }
    for (var l = 65; l <= 90; l++) {
    	if (jQuery.inArray(String.fromCharCode(l),matriz)==-1 && String.fromCharCode(l)!='J') {
    		matriz[inmD] = String.fromCharCode(l);
    		inmD++;
    	}
    }
    inmD = 1;
    for (var i = 1; i <=5; i++) {
    	for (var j = 1; j <= 5; j++) {
    		$("#m"+i+j+"d").html(matriz[inmD]);
    		inmD++;
    	}
    }
    while (idc <= cifradoD.length-1) {
    	if (cifradoD[idc] == 'J'){
			cifradoD[idc]='I';
		}
		if (cifradoD[idc+1] == 'J'){
			cifradoD[idc+1]='I';
		}
		filaL1 = $("#matrizCifrado label:contains("+cifradoD[idc]+")").attr("fila");
		columnaL1 = $("#matrizCifrado label:contains("+cifradoD[idc]+")").attr("columna");
		filaL2 = $("#matrizCifrado label:contains("+cifradoD[idc+1]+")").attr("fila")
		columnaL2 = $("#matrizCifrado label:contains("+cifradoD[idc+1]+")").attr("columna");
		if (filaL1 == filaL2 && columnaL1 != columnaL2) {
			columnaL1Decifrada=Number(columnaL1)-1;
			filaL1Decifrada=filaL1;
			columnaL2Decifrada=Number(columnaL2)-1;
			filaL2Decifrada=filaL2;
			if (columnaL1Decifrada<1) {columnaL1Decifrada=columnaL1Decifrada+5;}
			if (columnaL2Decifrada<1) {columnaL2Decifrada=columnaL2Decifrada+5;}
		} else if (filaL1 != filaL2 && columnaL1 == columnaL2) {
			filaL1Decifrada=Number(filaL1)-1;
			columnaL1Decifrada=columnaL1;
			filaL2Decifrada=Number(filaL2)-1;
			columnaL2Decifrada=columnaL2;
			if (filaL1Decifrada<1) {filaL1Decifrada=filaL1Decifrada+5;}
			if (filaL2Decifrada<1) {filaL2Decifrada=filaL2Decifrada+5;}
		} else if (filaL1 != filaL2 && columnaL1 != columnaL2) {
			filaL1Decifrada=filaL1;
			columnaL1Decifrada=columnaL2;
			filaL2Decifrada=filaL2;
			columnaL2Decifrada=columnaL1;
		}
		plano[idc]=$("#m"+filaL1Decifrada+columnaL1Decifrada+"d").text().toLowerCase();
		plano[idc+1]=$("#m"+filaL2Decifrada+columnaL2Decifrada+"d").text().toLowerCase();
        if (filaL1 == filaL2 && columnaL1 != columnaL2) {
            $("#infoDePlayfair").html('<br>Por la <strong>Regla No.1</strong> <strong>'+cifradoD[idc]+'</strong> y <strong>'+cifradoD[idc+1]+'</strong> se encuentran en la misma fila, por lo tanto se escogen <strong>'+plano[idc]+'</strong> y <strong>'+plano[idc+1]+'</strong> que estan situados a su derecha');
        } else if (filaL1 != filaL2 && columnaL1 == columnaL2) {
            $("#infoDePlayfair").html('<br>Por la <strong>Regla No.2</strong> <strong>'+cifradoD[idc]+'</strong> y <strong>'+cifradoD[idc+1]+'</strong> se encuentran en la misma columna, por lo tanto se escogen <strong>'+plano[idc]+'</strong> y <strong>'+plano[idc+1]+'</strong> que estan situados debajo');
        } else if (filaL1 != filaL2 && columnaL1 != columnaL2) {
            $("#infoDePlayfair").html('<br>Por la <strong>Regla No.3</strong> <strong>'+cifradoD[idc]+'</strong> y <strong>'+cifradoD[idc+1]+'</strong> se encuentran en distintas filas y columnas, por lo tanto se escogen <strong>'+plano[idc]+'</strong> y <strong>'+plano[idc+1]+'</strong> que estan situados en la diagonal opuesta');
        }
		$("#abcPlano"+idc+"D").html(plano[idc]).show();
		$("#abcPlano"+(idc+1)+"D").html(plano[idc+1]).show();
    	
    	// ANIMATION
    	$("#abcCifrado"+idc+"D").addClass('parpadeo');
    	$("#abcCifrado"+(idc+1)+"D").addClass('parpadeo');
    	$("#matrizCifrado label:contains("+cifradoD[idc]+")").addClass("parpadeo");
		$("#matrizCifrado label:contains("+cifradoD[idc+1]+")").addClass("parpadeo");
		$("#m"+filaL1Decifrada+columnaL1Decifrada+"d").addClass("parpadeoNext");
		$("#m"+filaL2Decifrada+columnaL2Decifrada+"d").addClass("parpadeoNext");
    	$("#abcPlano"+idc+"D").addClass('parpadeoNext');
    	$("#abcPlano"+(idc+1)+"D").addClass('parpadeoNext');
    	await sleep(tDcAdd);
    	$("#abcCifrado"+idc+"D").removeClass('parpadeo');
    	$("#abcCifrado"+(idc+1)+"D").removeClass('parpadeo');
    	$("#matrizCifrado label:contains("+cifradoD[idc]+")").removeClass("parpadeo");
		$("#matrizCifrado label:contains("+cifradoD[idc+1]+")").removeClass("parpadeo");
		$("#m"+filaL1Decifrada+columnaL1Decifrada+"d").removeClass("parpadeoNext");
		$("#m"+filaL2Decifrada+columnaL2Decifrada+"d").removeClass("parpadeoNext");
    	$("#abcPlano"+idc+"D").removeClass('parpadeoNext');
    	$("#abcPlano"+(idc+1)+"D").removeClass('parpadeoNext');
    	await sleep(tDcRemove);
    	// END ANIMATION
        idc=idc+2;
    }
    if (idc <= cifradoD.length && idc!=999) {
        for (var i = 0; i <= plano.length-1; i++) {
            if(plano[i] == 'x') {
               plano.splice(i, 1);
               $("#infoAnimacionDePlayfairExtra").show();
            }
        }
        cadenaDecifrado = plano.join("");
        $("#out-txtPlanoPlayfair").val(cadenaDecifrado);
        $("#btn-copiarTextoPlayfair").removeAttr("disabled");
        toastr.options.timeOut = "1000";
    	toastr['success']('Texto descifrado');
        $("#btn-descifrarPlayfair").show();
        $("#btn-tipoDePlayfair").show();
        $("#btn-cancelarDescifrarPlayfair").hide();
    }
}

function validarEntradaCifradoPlayfair(){
    var mensaje = "";
    var texto = $('#in-txtPlanoPlayfair').val();
    if (texto.length < 1 || texto.length > 20) {
        mensaje = "El mensaje claro debe contener entre 1 y 20 caracteres.";
    } else if (!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaCifradoPlayfairLlave(){
    var mensaje = "";
    var clave = $('#in-keyPlayfairC').val();
    if (clave.length < 1 || clave.length > 25) {
        mensaje = "La llave debe contener entre 1 y 25 caracteres.";
    } else if(!clave.match(/^[a-zA-Z \s]+$/)){
        mensaje = "La llave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaDescifradoPlayfair(){
    var mensaje = "";
    var texto = $('#in-txtCifradoPlayfair').val();
    if (texto.length < 1 || texto.length > 20) {
        mensaje = "El criptograma debe contener entre 1 y 20 caracteres.";
    } else if (!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaDescifradoPlayfairLlave(){
    var mensaje = "";
    var clave = $('#in-keyPlayfairD').val();
    if (clave.length < 1 || clave.length > 25) {
        mensaje = "La llave debe contener entre 1 y 25 caracteres.";
    } else if (!clave.match(/^[a-zA-Z \s]+$/)){
        mensaje = "La llave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$("#btn-mostrarPanelPlayfair").click(function(){
		mostrarPanelPlayfair();
	});
	$("#btn-cerrarPanelPlayfair").click(function(){
        pararAnimacionPlayfair();
		cerrarPanelPlayfair();
	});
    $("#btn-teoriaPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#btn-fundamentosPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#btn-animacionCifradoPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#btn-animacionDesifradoPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#btn-cancelarCifrarPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#btn-cancelarDescifrarPlayfair").click(function(){
        pararAnimacionPlayfair();
    });
    $("#tipoCiPlayfair1").click(function(){
        $("#btn-cifrarPlayfair").html('Cifrado Rápido');
        $("#btn-cifrarPlayfair").val(1);
    });
    $("#tipoCiPlayfair2").click(function(){
        $("#btn-cifrarPlayfair").html('Cifrado Normal');
        $("#btn-cifrarPlayfair").val(2);
    });
    $("#tipoCiPlayfair3").click(function(){
        $("#btn-cifrarPlayfair").html('Cifrado Lento');
        $("#btn-cifrarPlayfair").val(3);
    });
    $("#tipoDePlayfair1").click(function(){
        $("#btn-descifrarPlayfair").html('Descifrado Rápido');
        $("#btn-descifrarPlayfair").val(1);
    });
    $("#tipoDePlayfair2").click(function(){
        $("#btn-descifrarPlayfair").html('Descifrado Normal');
        $("#btn-descifrarPlayfair").val(2);
    });
    $("#tipoDePlayfair3").click(function(){
        $("#btn-descifrarPlayfair").html('Descifrado Lento');
        $("#btn-descifrarPlayfair").val(3);
    });

    $("#in-txtPlanoPlayfair").change(function(){
        $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoPlayfair-error").remove();
        if ($("#in-txtPlanoPlayfair").val()=='') {
            $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
            $("#txtPlanoPlayfair-error").remove();
        } else{
            var mensaje = validarEntradaCifradoPlayfair();
            if (mensaje.length == 0){
                $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
                $("#txtPlanoPlayfair-error").remove();
            } else {
                $("#in-txtPlanoPlayfair").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoPlayfair").parent().parent().append('<div id="txtPlanoPlayfair-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-keyPlayfairC").change(function(){
        $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
        $("#keyPlayfairC-error").remove();
        if ($("#in-keyPlayfairC").val()=='') {
            $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
            $("#keyPlayfairC-error").remove();
        } else{
            var mensaje = validarEntradaCifradoPlayfairLlave();
            if (mensaje.length == 0){
                $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
                $("#keyPlayfairC-error").remove();
            } else {
                $("#in-keyPlayfairC").parent().parent().addClass('has-error has-feedback');
                $("#in-keyPlayfairC").parent().parent().append('<div id="keyPlayfairC-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-txtCifradoPlayfair").change(function(){
        $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoPlayfair-error").remove();
        if ($("#in-txtCifradoPlayfair").val()=='') {
            $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
            $("#txtCifradoPlayfair-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoPlayfair();
            if (mensaje.length == 0){
                $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
                $("#txtCifradoPlayfair-error").remove();
            } else {
                $("#in-txtCifradoPlayfair").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoPlayfair").parent().parent().append('<div id="txtCifradoPlayfair-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-keyPlayfairD").change(function(){
        $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
        $("#keyPlayfairD-error").remove();
        if ($("#in-keyPlayfairD").val()=='') {
            $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
            $("#keyPlayfairD-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoPlayfairLlave();
            if (mensaje.length == 0){
                $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
                $("#keyPlayfairD-error").remove();
            } else {
                $("#in-keyPlayfairD").parent().parent().addClass('has-error has-feedback');
                $("#in-keyPlayfairD").parent().parent().append('<div id="keyPlayfairD-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

	$("#btn-cifrarPlayfair").click(function(){
        $("#in-txtPlanoPlayfair").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoPlayfair-error").remove();
        $("#in-keyPlayfairC").parent().parent().removeClass('has-error has-feedback');
        $("#keyPlayfairC-error").remove();
        $("#out-txtCifradoPlayfair").val("");
        var mensaje = validarEntradaCifradoPlayfair();
        var llave = validarEntradaCifradoPlayfairLlave();
		if ($("#in-txtPlanoPlayfair").val()!='' && $("#in-keyPlayfairC").val()!='' && mensaje.length == 0 && llave.length == 0){
			$("#textoPlanoPlayfairC").empty();
			$("#textoCifradoPlayfairC").empty();
            $("#infoAnimacionCiPlayfair").hide();
            $("#infoCiPlayfair").html('');
            $("#infoAnimacionCiPlayfairExtra").hide();
            $("#btn-cifrarPlayfair").hide();
            $("#btn-tipoCiPlayfair").hide();
            $("#btn-cancelarCifrarPlayfair").show();
			cifrarPlayfair();
		} else{
            if (mensaje.length != 0) {
                $("#in-txtPlanoPlayfair").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoPlayfair").parent().parent().append('<div id="txtPlanoPlayfair-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
            if (llave.length != 0) {
                $("#in-keyPlayfairC").parent().parent().append('<div id="keyPlayfairC-error" class="text-danger">&nbsp;'+llave+'</div>');
                $("#in-keyPlayfairC").parent().parent().addClass('has-error has-feedback');
            }
        }
	});

	$("#btn-copiarTextoPlayfair").click(function(){
		if ($("#out-txtCifradoPlayfair").val()==''){
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		} else {
			$("#in-txtCifradoPlayfair").val($("#out-txtCifradoPlayfair").val());
			$("#in-keyPlayfairD").val($("#in-keyPlayfairC").val());
		}
	});

	$("#btn-descifrarPlayfair").click(function(){
        $("#in-txtCifradoPlayfair").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoPlayfair-error").remove();
        $("#in-keyPlayfairD").parent().parent().removeClass('has-error has-feedback');
        $("#keyPlayfairD-error").remove();
        $("#out-txtPlanoPlayfair").val("");
        var mensaje = validarEntradaDescifradoPlayfair();
        var llave = validarEntradaDescifradoPlayfairLlave();
		if ($("#in-txtCifradoPlayfair").val()!='' && $("#in-keyPlayfairD").val()!='' && mensaje.length == 0 && llave.length == 0){
			$("#btn-copiarTextoPlayfair").attr("disabled","disabled");
			$("#textoCifradoPlayfairD").empty();
			$("#textoPlanoPlayfairD").empty();
            $("#infoAnimacionDePlayfair").hide();
            $("#infoDePlayfair").html('');
            $("#infoAnimacionDePlayfairExtra").hide();
            $("#btn-descifrarPlayfair").hide();
            $("#btn-tipoDePlayfair").hide();
            $("#btn-cancelarDescifrarPlayfair").show();
			descifrarPlayfair();
		} else{
            if (mensaje.length != 0) {
                $("#in-txtCifradoPlayfair").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoPlayfair").parent().parent().append('<div id="txtCifradoPlayfair-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
            if (llave.length != 0) {
                $("#in-keyPlayfairD").parent().parent().addClass('has-error has-feedback');
                $("#in-keyPlayfairD").parent().parent().append('<div id="keyPlayfairD-error" class="text-danger">&nbsp;'+llave+'</div>');
            }
        }
	});
	    
});