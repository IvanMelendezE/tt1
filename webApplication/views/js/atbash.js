var velocidadAnimacionCifrarAtbash= 1;
var velocidadAnimacionDescifrarAtbash= 1;
var seguirCifrandoAtbash= true;
var seguirDescifrandoAtbash= true;

function mostrarPanelAtbash()
{	
	$("#panelInteractivo-CifradoAtbash").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);	
}

function cerrarPanelAtbash()
{
	$("#panelInteractivo-CifradoAtbash").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	
	limpiaPanelAtbashCifrado();
	limpiaPanelAtbashDescifrado();
}

function limpiaPanelAtbashCifrado()
{	
	if($('#texto1CifradoAtbash').is(':visible'))
	{
		$("#texto1CifradoAtbash").slideToggle(500);
	}
	$("#texto1CifradoAtbash").empty();
	$("#filaMensajeClaro-cifrado").empty();
	$("#filaAlfabetoInverso-cifrado").empty();
	$("#textoCifradoAtbashC").empty();
	$("#text-mensajeClaroAtbash-cifrado").val("");
	$("#text-mensajeCifradoAtbash-cifrado").val("");
}

function limpiaPanelAtbashDescifrado()
{
	if($('#texto1DescifradoAtbash').is(':visible'))
	{
		$("#texto1DescifradoAtbash").slideToggle(500);
	}
	$("#texto1DescifradoAtbash").empty();
	$("#filaCriptogramaAtbashDescifrado").empty();
	$("#filaAlfabetoInvertidoAtbashDescifrado").empty();
	$("#textoDescifradoAtbashD").empty();
	$("#text-criptogramaAtbash-descifrado").val("");
	$("#text-mensajeDescifradoAtbash-descifrado").val("");
}

function obtenerVelocidadAnimacionAtbashCifrar()
{
	if($('#btn-cifrarAtbash-cifrado').val() == 1)
	{
		velocidadAnimacionCifrarAtbash = 0.5;
	}
	else if($('#btn-cifrarAtbash-cifrado').val() == 2)
	{
		velocidadAnimacionCifrarAtbash = 1;
	}
	else
	{
		velocidadAnimacionCifrarAtbash = 2;
	}

	$("#btn-velocidadCAtbash").hide();
	$("#btn-cifrarAtbash-cifrado").hide();
	$("#btn-cancelarCifrarAtbash-cifrado").show();
	seguirCifrandoAtbash= true;
}

function obtenerVelocidadAnimacionAtbashDescifrar()
{
	if($('#btn-descifrarAtbash-descifrado').val() == 1)
	{
		velocidadAnimacionDescifrarAtbash = 0.5;
	}
	else if($('#btn-descifrarAtbash-descifrado').val() == 2)
	{
		velocidadAnimacionDescifrarAtbash = 1;
	}
	else
	{
		velocidadAnimacionDescifrarAtbash = 2;
	}

	$("#btn-velocidadDAtbash").hide();
	$("#btn-descifrarAtbash-descifrado").hide();
	$("#btn-cancelarDescifrarAtbash-descifrado").show();
	seguirDescifrandoAtbash= true;
}

async function cifrarAtbash()
{	
	var plano = ($("#text-mensajeClaroAtbash-cifrado").val().toLowerCase().replace(/ /g,"")).split("");
    var cifrado = [];
    var cadenaCifrado;
	
	obtenerVelocidadAnimacionAtbashCifrar();
	
	limpiaPanelAtbashCifrado();
    $("#text-mensajeClaroAtbash-cifrado").val(plano.join(""));
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
	$("#texto1CifradoAtbash").append("Para este cifrado primero tomamos en cuenta el alfabeto latino internacional moderno:");
	$("#texto1CifradoAtbash").slideToggle(500);
	await sleep(1500);
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
		
	for (var i = 97; i <= 122; i++)
	{		
		$("#filaMensajeClaro-cifrado").append('<td id="columnaMensajeClaro-cifrado'+i+'">'+String.fromCharCode(i)+'</td>');
		await sleep(20*velocidadAnimacionCifrarAtbash);
		
		if(!seguirCifrandoAtbash)
		{
			return;
		}
	}
	
	await sleep(1500);
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
	$("#texto1CifradoAtbash").slideToggle(500);
	await sleep(500);
	$("#texto1CifradoAtbash").empty();
	$("#texto1CifradoAtbash").append("Invertimos el orden de las letras del alfabeto:");
	$("#texto1CifradoAtbash").slideToggle(500);
	await sleep(1750);
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
	for(var j= 90; j>=65; j--)
	{
		$("#filaAlfabetoInverso-cifrado").append('<td id="columnaAlfabetoInverso-cifrado'+(j+32)+'">'+String.fromCharCode(j)+'</td>');
		await sleep(20*velocidadAnimacionCifrarAtbash);
		
		if(!seguirCifrandoAtbash)
		{
			return;
		}
	}
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
	$("#texto1CifradoAtbash").slideToggle(500);
	await sleep(500);
	$("#texto1CifradoAtbash").empty();
	$("#texto1CifradoAtbash").append("Para cifrar tomamos letra por letra del mensaje a cifrar y la sustituimos por la letra que le corresponda en el alfabeto invertido:");
	$("#texto1CifradoAtbash").slideToggle(500);		
	await sleep(4300);	
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
    for (var i = 0; i <= plano.length-1; i++) 
	{
    	if(plano[i] != ' ')
		{
			var j= 122; //Para leer el abecedario al reves
			numeroElemntoPlano= plano[i].charCodeAt();
			
			for (var k = 97; k < numeroElemntoPlano; k++) //buscamos la letra ingresada en el abecedario
			{
				j--;
			}
				
			numeroElemntoCifrado = j;
			cifrado[i] = String.fromCharCode(j);			
    		
    		cadenaCifrado = cifrado.join("");	    	
						
			$("#textoCifradoAtbashC").append('<label id="labelsAtbashCifrado'+i+'" class="circulo">'+String.fromCharCode(j).toUpperCase()+'</label>');
			
			if(!seguirCifrandoAtbash)
			{
				return;
			}
	    	
	    	// ANIMATION	    	
			$("#columnaMensajeClaro-cifrado"+numeroElemntoPlano).css("backgroundColor", "#AEC6FC");
	    	$("#columnaAlfabetoInverso-cifrado"+numeroElemntoCifrado).css("backgroundColor", "#AEC6FC");
			$("#labelsAtbashCifrado"+i).css("backgroundColor", "#AEC6FC");						
	    	await sleep(1000*velocidadAnimacionCifrarAtbash);
			
			if(!seguirCifrandoAtbash)
			{
				return;
			}
			
	    	$("#columnaMensajeClaro-cifrado"+numeroElemntoPlano).css("backgroundColor", "transparent");
	    	$("#columnaAlfabetoInverso-cifrado"+numeroElemntoCifrado).css("backgroundColor", "transparent");
			$("#labelsAtbashCifrado"+i).css("backgroundColor", "transparent");				    	
			
			if(!seguirCifrandoAtbash)
			{
				return;
			}
	    	// END ANIMATION

    	}		
    }	

	posicion = $("#text-mensajeCifradoAtbash-cifrado").offset().top;
	$("html, body").animate({
		scrollTop: posicion
	}, 500);
	
	if(!seguirCifrandoAtbash)
	{
		return;
	}
	
	$("#btn-velocidadCAtbash").show();
	$("#btn-cifrarAtbash-cifrado").show();
	$("#btn-cancelarCifrarAtbash-cifrado").hide();
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto cifrado');
	$("#text-mensajeCifradoAtbash-cifrado").val(cadenaCifrado.toUpperCase());
}

async function descifrarAtbash()
{
	var cifrado = ($("#text-criptogramaAtbash-descifrado").val().toUpperCase().replace(/ /g,"")).split("");
    var plano = [];
    var cadenaDescifrada;
	var numeroElementoCifrado;
	var numeroElementoPlano;
		
	obtenerVelocidadAnimacionAtbashDescifrar();
		
	limpiaPanelAtbashDescifrado();
	
    $("#text-criptogramaAtbash-descifrado").val(cifrado.join(""));
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
	$("#texto1DescifradoAtbash").append("Para descifrar tomamos en cuenta el alfabeto latino internacional moderno:");
	$("#texto1DescifradoAtbash").slideToggle(500);
	await sleep(1500);
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
		
	for (var i = 65; i <= 90; i++)
	{		
		$("#filaCriptogramaAtbashDescifrado").append('<td id="columnaCriptogramaAtbashDescifrado'+i+'">'+String.fromCharCode(i)+'</td>');
		await sleep(20*velocidadAnimacionDescifrarAtbash);
		
		if(!seguirDescifrandoAtbash)
		{
			return;
		}
	}
	
	await sleep(1500);
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
	$("#texto1DescifradoAtbash").slideToggle(500);
	await sleep(500);
	$("#texto1DescifradoAtbash").empty();
	$("#texto1DescifradoAtbash").append("Invertimos el orden de las letras del alfabeto:");
	$("#texto1DescifradoAtbash").slideToggle(500);
	await sleep(1750);
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
	for(var j= 122; j>=97; j--)
	{
		$("#filaAlfabetoInvertidoAtbashDescifrado").append('<td id="columnaAlfabetoInvertidoAtbashDescifrado'+(j-32)+'">'+String.fromCharCode(j)+'</td>');
		await sleep(20*velocidadAnimacionDescifrarAtbash);
		
		if(!seguirDescifrandoAtbash)
		{
			return;
		}
	}
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
	$("#texto1DescifradoAtbash").slideToggle(500);
	await sleep(500);
	$("#texto1DescifradoAtbash").empty();
	$("#texto1DescifradoAtbash").append("Para descifrar tomamos letra por letra del mensaje a descifrar y la sustituimos por la letra que le corresponda en el alfabeto invertido:");
	$("#texto1DescifradoAtbash").slideToggle(500);		
	await sleep(4300);	
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
    for (var i = 0; i <= cifrado.length-1; i++) 
	{
    	if(cifrado[i] != ' ')
		{
			var j= 90; //Para leer el abecedario al reves
			numeroElementoCifrado= cifrado[i].charCodeAt();
			
			for (var k = 65; k < numeroElementoCifrado; k++) //buscamos la letra ingresada en el abecedario
			{
				j--;
			}
				
			numeroElementoPlano = j;
			plano[i] = String.fromCharCode(j);			
    		
    		cadenaDescifrada = plano.join("");	    	
						
			$("#textoDescifradoAtbashD").append('<label id="labelsAtbashDescifrado'+i+' class="circulo">'+String.fromCharCode(j).toLowerCase()+'</label>');
			
			if(!seguirDescifrandoAtbash)
			{
				return;
			}
	    	
	    	// ANIMATION	    	
			$("#columnaCriptogramaAtbashDescifrado"+numeroElementoCifrado).css("backgroundColor", "#AEC6FC");
	    	$("#columnaAlfabetoInvertidoAtbashDescifrado"+numeroElementoPlano).css("backgroundColor", "#AEC6FC");
			$("#labelsAtbashDescifrado"+i).css("backgroundColor", "#AEC6FC");						
	    	await sleep(1000*velocidadAnimacionDescifrarAtbash);
			
			if(!seguirDescifrandoAtbash)
			{
				return;
			}
			
	    	$("#columnaCriptogramaAtbashDescifrado"+numeroElementoCifrado).css("backgroundColor", "transparent");
	    	$("#columnaAlfabetoInvertidoAtbashDescifrado"+numeroElementoPlano).css("backgroundColor", "transparent");
			$("#labelsAtbashDescifrado"+i).css("backgroundColor", "transparent");				    	
			
			if(!seguirDescifrandoAtbash)
			{
				return;
			}
	    	// END ANIMATION

    	}		
    }	

	posicion = $("#text-mensajeDescifradoAtbash-descifrado").offset().top;
	$("html, body").animate({
		scrollTop: posicion
	}, 500);
	
	if(!seguirDescifrandoAtbash)
	{
		return;
	}
	
	$("#btn-velocidadDAtbash").show();
	$("#btn-descifrarAtbash-descifrado").show();
	$("#btn-cancelarDescifrarAtbash-descifrado").hide();
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto descifrado');
	$("#text-mensajeDescifradoAtbash-descifrado").val(cadenaDescifrada.toLowerCase());
}

function validarEntradaCifradoAtbash()
{
	var mensaje = "";
	var texto = $('#text-mensajeClaroAtbash-cifrado').val().replace(/ /g,"");

	if (texto.length < 1 || texto.length > 10) {
		mensaje = "El mensaje claro debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El mensaje claro sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}

	return mensaje;
}

function validarEntradaDescifradoAtbash()
{
	var mensaje = "";
	var texto = $('#text-criptogramaAtbash-descifrado').val().replace(/ /g,"");
	
	if (texto.length < 1 || texto.length > 10)
	{
		mensaje = "El criptograma debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El criptograma sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}

	return mensaje;
}

$(document).ready(function()
{
	$("#CifradoRapidoAtbash").click(function(){
		$("#btn-cifrarAtbash-cifrado").html('Cifrado Rápido');
		$("#btn-cifrarAtbash-cifrado").val(1);
	});
	$("#CifradoNormalAtbash").click(function(){
		$("#btn-cifrarAtbash-cifrado").html('Cifrado Normal');
		$("#btn-cifrarAtbash-cifrado").val(2);
	});
	$("#CifradoLentoAtbash").click(function(){
		$("#btn-cifrarAtbash-cifrado").html('Cifrado Lento');
		$("#btn-cifrarAtbash-cifrado").val(3);
	});
	
	$("#DescifradoRapidoAtbash").click(function(){
		$("#btn-descifrarAtbash-descifrado").html('Descifrado Rápido');
		$("#btn-descifrarAtbash-descifrado").val(1);
	});
	$("#DescifradoNormalAtbash").click(function(){
		$("#btn-descifrarAtbash-descifrado").html('Descifrado Normal');
		$("#btn-descifrarAtbash-descifrado").val(2);
	});
	$("#DescifradoLentoAtbash").click(function(){
		$("#btn-descifrarAtbash-descifrado").html('Descifrado Lento');
		$("#btn-descifrarAtbash-descifrado").val(3);
	});
	
	$("#text-mensajeClaroAtbash-cifrado").keyup(function(){
		var mensaje = validarEntradaCifradoAtbash();

		if (mensaje.length != 0) {
			$("#textoPlanoAtbash-error").remove();
			$("#text-mensajeClaroAtbash-cifrado").parent().parent().append('<div id="textoPlanoAtbash-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#text-mensajeClaroAtbash-cifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-cifrarAtbash-cifrado").attr("disabled", true);
		} else{
			$("#textoPlanoAtbash-error").remove();
			$("#text-mensajeClaroAtbash-cifrado").parent().parent().removeClass('has-error has-feedback');
			$("#btn-cifrarAtbash-cifrado").attr("disabled", false);
		}
	});
	
	$("#text-criptogramaAtbash-descifrado").keyup(function(){
		var res = validarEntradaDescifradoAtbash();	
		
		if (res.length != 0)
		{
			$("#criptogramaAtbash-error").remove();
			$("#text-criptogramaAtbash-descifrado").parent().parent().append('<div id="criptogramaAtbash-error" class="text-danger">&nbsp;'+res+'</div>');
			$("#text-criptogramaAtbash-descifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-descifrarAtbash-descifrado").attr("disabled", true);
		} else
		{
			$("#criptogramaAtbash-error").remove();
			$("#text-criptogramaAtbash-descifrado").parent().parent().removeClass('has-error has-feedback');
			$("#btn-descifrarAtbash-descifrado").attr("disabled", false);
		}
	});
	
	$("#btn-cifrarAtbash-cifrado").click(function()
	{
		var mensaje= validarEntradaCifradoAtbash();
		
		if(mensaje.length!=0)
		{
			$("#textoPlanoAtbash-error").remove();
			$("#text-mensajeClaroAtbash-cifrado").parent().parent().append('<div id="textoPlanoAtbash-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#text-mensajeClaroAtbash-cifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-cifrarAtbash-cifrado").attr("disabled", true);
		}
		else
		{
			cifrarAtbash();
		}		
	});
	
	$("#btn-descifrarAtbash-descifrado").click(function()
	{
		var mensaje= validarEntradaDescifradoAtbash();
		
		if(mensaje.length!=0)
		{
			$("#criptogramaAtbash-error").remove();
			$("#text-criptogramaAtbash-descifrado").parent().parent().append('<div id="criptogramaAtbash-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#text-criptogramaAtbash-descifrado").parent().parent().addClass('has-error has-feedback');
			$("#btn-descifrarAtbash-descifrado").attr("disabled", true);
		}
		else
		{
			descifrarAtbash();
		}		
	});
	
	$("#btn-cancelarCifrarAtbash-cifrado").click(function()
	{
		seguirCifrandoAtbash= false;
		
		limpiaPanelAtbashCifrado();

		$("#btn-velocidadCAtbash").show();
		$("#btn-cifrarAtbash-cifrado").show();
		$("#btn-cancelarCifrarAtbash-cifrado").hide();
	});
	
	$("#btn-cancelarDescifrarAtbash-descifrado").click(function()
	{		
		seguirDescifrandoAtbash= false;
		
		limpiaPanelAtbashDescifrado();

		$("#btn-velocidadDAtbash").show();
		$("#btn-descifrarAtbash-descifrado").show();
		$("#btn-cancelarDescifrarAtbash-descifrado").hide();
	});
	
	$("#btn-copiarTextoAtbash").click(function()
	{
		if ($("#text-mensajeCifradoAtbash-cifrado").val()=='')
		{
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		}
		else
		{
			$("#text-criptogramaAtbash-descifrado").val($("#text-mensajeCifradoAtbash-cifrado").val());
		}
	});
});