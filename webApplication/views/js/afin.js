var velocidadAnimacionCifraraAfin= 1;
var velocidadAnimacionDescifrarAfin= 1;
var seguirCifrandoAfin= true;
var seguirDescifrandoAfin= true;

function mostrarPanelAfin()
{
	//crearPanelAfin();
	$("#panelInteractivo-CifradoAfin").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);	
}

function cerrarPanelAfin()
{
	$("#panelInteractivo-CifradoAfin").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelAfinCifrado();
	limpiaPanelAfinDescifrado();
}

function limpiaPanelAfinCifrado()
{
	if($('#informacionAfin1C').is(':visible'))
	{
		$("#informacionAfin1C").slideToggle(500);
	}
	
	if($('#informacionAfin2C').is(':visible'))
	{
		$("#informacionAfin2C").slideToggle(500);
	}
	
	if($('#informacionAfin3C').is(':visible'))
	{
		$("#informacionAfin3C").slideToggle(500);
	}
	
	if($('#tablaAlfabetoAfin2C').is(':visible'))
	{
		$("#tablaAlfabetoAfin2C").slideToggle(500);
		$("#tablaAlfabetoAfin2C").empty();
	}	
	
	$("#informacionAfin1C").empty();
	$("#informacionAfin2C").empty();
	$("#informacionAfin3C").empty();	
	$("#filaMensajeClaroCifrado").empty();
	$("#filaAlfabetoAfinCifrado").empty();	
	$("#filaNumeroAlfabetoAfinCifrado").empty();
	$("#fila2AlfabetoAfinCifrado").empty();
	$("#fila2NumeroAlfabetoAfinCifrado").empty();
	$("#textoCifradoAfinC").empty();
	$("#textoMensajePlanoAfinCifrado").val("");
	$("#textoMensajeCifradoAfinCifrado").val("");
}

function limpiaPanelAfinDescifrado()
{
	if($('#informacionAfin1D').is(':visible'))
	{
		$("#informacionAfin1D").slideToggle(500);
	}
	
	if($('#informacionAfin2D').is(':visible'))
	{
		$("#informacionAfin2D").slideToggle(500);
	}
	
	if($('#informacionAfin3D').is(':visible'))
	{
		$("#informacionAfin3D").slideToggle(500);
	}
	
	if($('#tablaAlfabetoAfin2D').is(':visible'))
	{
		$("#tablaAlfabetoAfin2D").slideToggle(500);
		$("#tablaAlfabetoAfin2D").empty();
	}	
	
	$("#informacionAfin1D").empty();
	$("#informacionAfin2D").empty();
	$("#informacionAfin3D").empty();	
	$("#filaCriptogramaAfinDescifrado").empty();
	$("#filaAlfabetoAfinDescifrado").empty();	
	$("#filaNumeroAlfabetoAfinDescifrado").empty();
	$("#fila2AlfabetoAfinDescifrado").empty();
	$("#fila2NumeroAlfabetoAfinDescifrado").empty();
	$("#textoDescifradoAfinD").empty();
	$("#textoCriptogramaAfinDescifrado").val("");
	$("#textoMensajeClaroAfinDescifrado").val("");
}

function obtenerVelocidadAnimacionAfinCifrar()
{
	if($('#btn-cifrarAfin-cifrado').val() == 1)
	{
		velocidadAnimacionCifraraAfin = 0.5;
	}
	else if($('#btn-cifrarAfin-cifrado').val() == 2)
	{
		velocidadAnimacionCifraraAfin = 1;
	}
	else
	{
		velocidadAnimacionCifraraAfin = 2;
	}

	$("#btn-velocidadCAfin").hide();
	$("#btn-cifrarAfin-cifrado").hide();
	$("#btn-cancelarCifrarAfin-cifrado").show();
	seguirCifrandoAfin= true;
}

function obtenerVelocidadAnimacionAfinDescifrar()
{
	if($('#btn-descifrarAfin-descifrado').val() == 1)
	{
		velocidadAnimacionDescifrarAfin = 0.5;
	}
	else if($('#btn-descifrarAfin-descifrado').val() == 2)
	{
		velocidadAnimacionDescifrarAfin = 1;
	}
	else
	{
		velocidadAnimacionDescifrarAfin = 2;
	}

	$("#btn-velocidadDAfin").hide();
	$("#btn-descifrarAfin-descifrado").hide();
	$("#btn-cancelarDescifrarAfin-descifrado").show();
	seguirDescifrandoAfin= true;
}

function tieneInversoMultiplicativo(valorA)
{	
	var a= valorA;
	var n= 26;	
	var u, v, x1, x2, q, r, x;
	var inversoA;
	
	if(n<2)
	{
		//NO TIENE INVERSO
		inversoA= "No tiene inverso";
	}
	else
	{		
		u= a;
		v= n;
		x1= 1;
		x2= 0;
		
		while(u!=1&&u!=0)
		{			
			q= v/u;
			q= parseInt(q);			
			r= v-q*u;
			x= x2-q*x1;
			v= u;
			u= r;
			x2= x1;
			x1= x;
		}
		
		if(u==0)
		{
			//NO TIENE INVERSO
			inversoA= "No tiene inverso";
		}
		else
		{
			if(x1>0||x1==0)
			{
				inversoA= x1%n;
			}
			else
			{
				inversoA= n-((-1)*x1%n);
			}
		}
	}
	
	return inversoA;
}

async function cifrarAfin()
{
	var textoPlano = ($("#textoMensajePlanoAfinCifrado").val().toLowerCase().replace(/ /g,"")).split("");
	var valorA= ($("#valorAafinC")).val();
	var valorB= ($("#valorBafinC")).val();
	var valorLetraCifrada;
	var cifrado = [];
    var cadenaCifrado;
	var numeroLetra= 0, numeroElementoPlano;
	var numeroLetra2= 0;
	
	obtenerVelocidadAnimacionAfinCifrar();
	
	limpiaPanelAfinCifrado();
    $("#textoMensajePlanoAfinCifrado").val(textoPlano.join(""));
	
	if(!seguirCifrandoAfin)
	{
		return;
	}
	
    $("#informacionAfin1C").append("Mensaje claro a cifrar:");
	$("#informacionAfin1C").slideToggle(500);
	await sleep(1000);	
	
	if(!seguirCifrandoAfin)
	{
		return;
	}
		
	for (var i = 0; i <= textoPlano.length-1; i++)
	{
		if(textoPlano[i]!=' ')
		{
			$("#filaMensajeClaroCifrado").append('<td id="columnaMensajeClaroCifradoAfin'+i+'">'+String.fromCharCode(textoPlano[i].charCodeAt())+'</td>');
			await sleep(20*velocidadAnimacionCifraraAfin);
		}
		
		if(!seguirCifrandoAfin)
		{
			return;
		}
	}	
	
	$("#informacionAfin2C").append("Para este cifrado trabajaremos con el alfabeto latino internacional moderno:");
	$("#informacionAfin2C").slideToggle(500);
	await sleep(1750);	
	
	if(!seguirCifrandoAfin)
	{
		return;
	}
	
	for(var j= 97; j<=122; j++)
	{
		$("#filaAlfabetoAfinCifrado").append('<td id="columnaAlfabetoAfinCifrado'+j+'">'+String.fromCharCode(j)+'</td>');
		$("#filaNumeroAlfabetoAfinCifrado").append('<td id="columnaNumeroAlfabetoAfinCifrado'+numeroLetra+'">'+numeroLetra+'</td>');
		numeroLetra++;
		await sleep(20*velocidadAnimacionCifraraAfin);
		
		if(!seguirCifrandoAfin)
		{
			return;
		}
	}	
	
	numeroLetra= 0;
	
	for(var j= 65; j<=90; j++)
	{
		$("#fila2AlfabetoAfinCifrado").append('<td id="columna2AlfabetoAfinCifrado'+j+'">'+String.fromCharCode(j)+'</td>');
		$("#fila2NumeroAlfabetoAfinCifrado").append('<td id="columna2NumeroAlfabetoAfinCifrado'+numeroLetra+'">'+numeroLetra+'</td>');
		numeroLetra++;
		
		if(!seguirCifrandoAfin)
		{
			return;
		}
	}
	
	numeroLetra= 0;
	
	for(var i=0; i<=textoPlano.length-1; i++)
	{
		numeroElementoPlano= textoPlano[i].charCodeAt();
		
		if(i==0) //Solo lo hace la primera vez
		{
			$("#informacionAfin2C").slideToggle(500);
			await sleep(500);
			$("#informacionAfin2C").empty();
			$("#informacionAfin2C").append("Buscamos letra por letra del mensaje que se quiere cifrar y obtenemos el valor que le corresponda de acuerdo al alfabeto:");
			$("#informacionAfin2C").slideToggle(500);
			posicion = $("#informacionAfin2C").offset().top;
			$("html, body").animate({scrollTop: posicion}, 1000);
			await sleep(3000);
		}
		
		if(!seguirCifrandoAfin)
		{
			return;
		}
		
		$("#columnaMensajeClaroCifradoAfin"+i).css("backgroundColor", "#AEC6FC");
		
		numeroLetra= 0;
		
		for(var j=97; j<=122; j++)
		{
			$("#columnaAlfabetoAfinCifrado"+j).css("backgroundColor", "#77DD77");
			$("#columnaNumeroAlfabetoAfinCifrado"+numeroLetra).css("backgroundColor", "#77DD77");
			await sleep(75*velocidadAnimacionCifraraAfin);
			
			if(!seguirCifrandoAfin)
			{
				return;
			}
			
			if(numeroElementoPlano==j)
			{
				$("#columnaAlfabetoAfinCifrado"+j).css("backgroundColor", "#FDFD96");
				$("#columnaNumeroAlfabetoAfinCifrado"+numeroLetra).css("backgroundColor", "#FDFD96");
								
				if(i==0) //Solo lo hace la primera vez
				{
					$("#informacionAfin3C").append("Cambiamos en la fórmula: y= (ax + b) MOD m, los valores según corresponda (ver Teoría) para obtener el valor de cada letra cifrada del criptograma:");
					$("#informacionAfin3C").slideToggle(500);
					posicion = $("#informacionAfin3C").offset().top;
					$("html, body").animate({scrollTop: posicion}, 1000);
					await sleep(4000);
				}
				
				if(!seguirCifrandoAfin)
				{
					return;
				}
				
				valorLetraCifrada= (((parseInt(valorA)*parseInt(numeroLetra))+parseInt(valorB))%26);				
				
				if(i==0)
				{
					$("#informacionAfin3C").slideToggle(500);								
					await sleep(500);
					$("#informacionAfin3C").empty();
				}
				$("#informacionAfin3C").append("Aplicando la fórmula, el valor obtenido es: "+valorLetraCifrada+ ", buscamos la letra que corresponde al valor obtenido en el alfabeto:");
				$("#informacionAfin3C").slideToggle(500);
				posicion = $("#informacionAfin3C").offset().top;
				$("html, body").animate({scrollTop: posicion}, 1000);
				await sleep(2950);
				
				if(!seguirCifrandoAfin)
				{
					return;
				}
				
				$("#tablaAlfabetoAfin2C").slideToggle(500);
				posicion = $("#tablaAlfabetoAfin2C").offset().top;
				$("html, body").animate({scrollTop: posicion}, 1000);
				await sleep(1000);
				
				if(!seguirCifrandoAfin)
				{
					return;
				}
				
				numeroLetra2= 0;
				
				for(var k=65; k<=90; k++)
				{
					$("#columna2AlfabetoAfinCifrado"+k).css("backgroundColor", "#77DD77");
					$("#columna2NumeroAlfabetoAfinCifrado"+numeroLetra2).css("backgroundColor", "#77DD77");
					await sleep(75*velocidadAnimacionCifraraAfin);
					
					if(!seguirCifrandoAfin)
					{
						return;
					}

					if(numeroLetra2==valorLetraCifrada)					
					{
						$("#columna2AlfabetoAfinCifrado"+k).css("backgroundColor", "#FDFD96 ");
						$("#columna2NumeroAlfabetoAfinCifrado"+numeroLetra2).css("backgroundColor", "#FDFD96 ");											
						
						cifrado[i] = String.fromCharCode(k);
			    		
						cadenaCifrado = cifrado.join("");
						
						$("#textoCifradoAfinC").append('<label id="labelsAfinCifrado'+i+'" class="circulo">'+cifrado[i].toUpperCase()+'</label>');
						$("#labelsAfinCifrado"+i).css("backgroundColor", "#FDFD96");
						await sleep(1000*velocidadAnimacionCifraraAfin);
						
						if(!seguirCifrandoAfin)
						{
							return;
						}
						
						$("#columna2AlfabetoAfinCifrado"+k).css("backgroundColor", "transparent");
						$("#columna2NumeroAlfabetoAfinCifrado"+numeroLetra2).css("backgroundColor", "transparent");	
						$("#labelsAfinCifrado"+i).css("backgroundColor", "transparent");
						
						k= 90;
					}
					else
					{
						$("#columna2AlfabetoAfinCifrado"+k).css("backgroundColor", "transparent");
						$("#columna2NumeroAlfabetoAfinCifrado"+numeroLetra2).css("backgroundColor", "transparent");	
						await sleep(75*velocidadAnimacionCifraraAfin);
						numeroLetra2++;
						
						if(!seguirCifrandoAfin)
						{
							return;
						}
					}
				}
												
				$("#tablaAlfabetoAfin2C").slideToggle(500);
				$("#informacionAfin3C").slideToggle(500);								
				await sleep(1000);
				$("#informacionAfin3C").empty();
				
				if(!seguirCifrandoAfin)
				{
					return;
				}
				
				$("#columnaAlfabetoAfinCifrado"+j).css("backgroundColor", "transparent");
				$("#columnaNumeroAlfabetoAfinCifrado"+numeroLetra).css("backgroundColor", "transparent");			
				
				j= 122;
			}
			else
			{
				$("#columnaAlfabetoAfinCifrado"+j).css("backgroundColor", "transparent");
				$("#columnaNumeroAlfabetoAfinCifrado"+numeroLetra).css("backgroundColor", "transparent");
				
				if(!seguirCifrandoAfin)
				{
					return;
				}
			}										
			
			numeroLetra++;
			await sleep(20*velocidadAnimacionCifraraAfin);
			
			if(!seguirCifrandoAfin)
			{
				return;
			}
		}			
		
		$("#columnaMensajeClaroCifradoAfin"+i).css("backgroundColor", "transparent");
		
		if(!seguirCifrandoAfin)
		{
			return;
		}
	}
	
	posicion = $("#textoMensajeCifradoAfinCifrado").offset().top;
	$("html, body").animate({
		scrollTop: posicion
	}, 1000);
	
	if(!seguirCifrandoAfin)
	{
		return;
	}
	
	$("#btn-velocidadCAfin").show();
	$("#btn-cifrarAfin-cifrado").show();
	$("#btn-cancelarCifrarAfin-cifrado").hide();	
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto cifrado');
	$("#textoMensajeCifradoAfinCifrado").val(cadenaCifrado.toUpperCase());	
}

async function descifrarAfin()
{	
	var textoCifrado = ($("#textoCriptogramaAfinDescifrado").val().toUpperCase().replace(/ /g,"")).split("");
	var valorA= ($("#valorAafinD")).val();	
	var valorB= ($("#valorBafinD")).val()
	var ValorLetraADescifrar;
	var textoDescifrado= [];
	var cadenaDescifrada;	
	var inversoA= tieneInversoMultiplicativo();	
	var numeroLetra= 0, numeroElementoCifrado;
	var numeroLetra2;
	
	obtenerVelocidadAnimacionAfinDescifrar();
	
	limpiaPanelAfinDescifrado();
    $("#textoCriptogramaAfinDescifrado").val(textoCifrado.join(""));
	
	if(!seguirDescifrandoAfin)
	{
		return;
	}
	
	$("#informacionAfin1D").append("Criptograma a descifrar:");
	$("#informacionAfin1D").slideToggle(500);
	await sleep(1000);
	
	if(!seguirDescifrandoAfin)
	{
		return;
	}
	
	for (var i = 0; i <= textoCifrado.length-1; i++)
	{
		if(textoCifrado[i]!=' ')
		{
			$("#filaCriptogramaAfinDescifrado").append('<td id="columnaCriptogramaAfinDescifrado'+i+'">'+String.fromCharCode(textoCifrado[i].charCodeAt())+'</td>');
			await sleep(20*velocidadAnimacionDescifrarAfin);
		}
		
		if(!seguirDescifrandoAfin)
		{
			return;
		}
	}			
	
	$("#informacionAfin2D").append("Para descifrar tomamos en cuenta el alfabeto latino internacional moderno:");
	$("#informacionAfin2D").slideToggle(500);
	await sleep(1750);
	
	if(!seguirDescifrandoAfin)
	{
		return;
	}	
	
	for(var j= 65; j<=90; j++)
	{
		$("#filaAlfabetoAfinDescifrado").append('<td id="columnaAlfabetoAfinDescifrado'+j+'">'+String.fromCharCode(j)+'</td>');
		$("#filaNumeroAlfabetoAfinDescifrado").append('<td id="columnaNumeroAlfabetoAfinDescifrado'+numeroLetra+'">'+numeroLetra+'</td>');
		numeroLetra++;
		await sleep(20*velocidadAnimacionDescifrarAfin);
		
		if(!seguirDescifrandoAfin)
		{
			return;
		}
	}			
	
	numeroLetra= 0;

	for(var j= 97; j<=122; j++)
	{
		$("#fila2AlfabetoAfinDescifrado").append('<td id="columna2AlfabetoAfinDescifrado'+j+'">'+String.fromCharCode(j)+'</td>');
		$("#fila2NumeroAlfabetoAfinDescifrado").append('<td id="columna2NumeroAlfabetoAfinDescifrado'+numeroLetra+'">'+numeroLetra+'</td>');
		numeroLetra++;		
		
		if(!seguirDescifrandoAfin)
		{
			return;
		}
	}
	
	numeroLetra= 0;
	
	for(var i=0; i<=textoCifrado.length-1; i++)
	{
		numeroElementoCifrado= textoCifrado[i].charCodeAt();
		
		if(i==0)
		{
			$("#informacionAfin2D").slideToggle(500);
			await sleep(1000);
			$("#informacionAfin2D").empty();
			$("#informacionAfin2D").append("Buscamos letra por letra del criptograma que se quiere descifrar y obtenemos el valor que le corresponda de acuerdo al alfabeto:");
			$("#informacionAfin2D").slideToggle(500);
			posicion = $("#informacionAfin2D").offset().top;
			$("html, body").animate({scrollTop: posicion}, 1000);
			await sleep(3000);
		}
		
		if(!seguirDescifrandoAfin)
		{
			return;
		}
		
		$("#columnaCriptogramaAfinDescifrado"+i).css("backgroundColor", "#AEC6FC");
		
		numeroLetra= 0;
		
		for(var j=65; j<=90; j++)
		{
			$("#columnaAlfabetoAfinDescifrado"+j).css("backgroundColor", "#77DD77");
			$("#columnaNumeroAlfabetoAfinDescifrado"+numeroLetra).css("backgroundColor", "#77DD77");
			await sleep(20*velocidadAnimacionDescifrarAfin);
			
			if(!seguirDescifrandoAfin)
			{
				return;
			}
			
			if(numeroElementoCifrado==j)
			{
				$("#columnaAlfabetoAfinDescifrado"+j).css("backgroundColor", "#FDFD96");
				$("#columnaNumeroAlfabetoAfinDescifrado"+numeroLetra).css("backgroundColor", "#FDFD96");									

				if(i==0)
				{
					$("#informacionAfin3D").append("Cambiamos en la fórmula: x= (InversoMultiplicativo(a)*(y-b)) MOD m, los valores según corresponda (ver Teoría) para obtener el valor de cada letra descifrada del mensaje claro:");
					$("#informacionAfin3D").slideToggle(500);
					posicion = $("#informacionAfin3D").offset().top;
					$("html, body").animate({scrollTop: posicion}, 1000);
					await sleep(6500);
				}
				
				if(!seguirDescifrandoAfin)
				{
					return;
				}
				
				ValorLetraADescifrar= ((parseInt(inversoA)*(parseInt(numeroLetra)-parseInt(valorB)))%26);							
				
				if(ValorLetraADescifrar<0)
				{									
					ValorLetraADescifrar= ValorLetraADescifrar+26;									
				}
				
				if(i==0)
				{
					$("#informacionAfin3D").slideToggle(500);								
					await sleep(500);
					$("#informacionAfin3D").empty();
				}
				$("#informacionAfin3D").append("Aplicando la fórmula, el valor obtenido es: "+ValorLetraADescifrar+ ", buscamos la letra que corresponde al valor obtenido en el alfabeto:");
				$("#informacionAfin3D").slideToggle(500);
				posicion = $("#informacionAfin3D").offset().top;
				$("html, body").animate({scrollTop: posicion}, 1000);
				await sleep(3600);							
				
				if(!seguirDescifrandoAfin)
				{
					return;
				}
				
				$("#tablaAlfabetoAfin2D").slideToggle(500);
				posicion = $("#tablaAlfabetoAfin2D").offset().top;
				$("html, body").animate({scrollTop: posicion}, 1000);
				await sleep(1000);
				
				if(!seguirDescifrandoAfin)
				{
					return;
				}
				
				numeroLetra2= 0;
				
				for(var k=97; k<=122; k++)
				{
					$("#columna2AlfabetoAfinDescifrado"+k).css("backgroundColor", "#77DD77");
					$("#columna2NumeroAlfabetoAfinDescifrado"+numeroLetra2).css("backgroundColor", "#77DD77");
					await sleep(20*velocidadAnimacionDescifrarAfin);
					
					if(!seguirDescifrandoAfin)
					{
						return;
					}

					if(numeroLetra2==ValorLetraADescifrar)					
					{
						$("#columna2AlfabetoAfinDescifrado"+k).css("backgroundColor", "#FDFD96");
						$("#columna2NumeroAlfabetoAfinDescifrado"+numeroLetra2).css("backgroundColor", "#FDFD96");											
						
						textoDescifrado[i] = String.fromCharCode(k);
						
						cadenaDescifrada = textoDescifrado.join("");
						
						$("#textoDescifradoAfinD").append('<label id="columnatextoDescifradoAfinD'+i+'C" class="circulo">'+textoDescifrado[i].toLowerCase()+'</label>');
						$("#columnatextoDescifradoAfinD"+i+"C").css("backgroundColor", "#FDFD96");
						await sleep(1000*velocidadAnimacionDescifrarAfin);
						
						if(!seguirDescifrandoAfin)
						{
							return;
						}
						
						$("#columna2AlfabetoAfinDescifrado"+k).css("backgroundColor", "transparent");
						$("#columna2NumeroAlfabetoAfinDescifrado"+numeroLetra2).css("backgroundColor", "transparent");
						$("#columnatextoDescifradoAfinD"+i+"C").css("backgroundColor", "transparent");
						
						k= 122;							
					}
					else
					{
						$("#columna2AlfabetoAfinDescifrado"+k).css("backgroundColor", "transparent");
						$("#columna2NumeroAlfabetoAfinDescifrado"+numeroLetra2).css("backgroundColor", "transparent");	
						await sleep(20*velocidadAnimacionDescifrarAfin);						
						numeroLetra2++;
					}
					
					if(!seguirDescifrandoAfin)
					{
						return;
					}
				}												
				
				$("#tablaAlfabetoAfin2D").slideToggle(500);
				$("#informacionAfin3D").slideToggle(500);								
				await sleep(1000);
				$("#informacionAfin3D").empty();
				
				if(!seguirDescifrandoAfin)
				{
					return;
				}
				
				$("#columnaAlfabetoAfinDescifrado"+j).css("backgroundColor", "transparent");
				$("#columnaNumeroAlfabetoAfinDescifrado"+numeroLetra).css("backgroundColor", "transparent");			
				
				j= 90;
			}
			else
			{
				$("#columnaAlfabetoAfinDescifrado"+j).css("backgroundColor", "transparent");
				$("#columnaNumeroAlfabetoAfinDescifrado"+numeroLetra).css("backgroundColor", "transparent");
				
				if(!seguirDescifrandoAfin)
				{
					return;
				}
			}										
			
			numeroLetra++;
			await sleep(20*velocidadAnimacionDescifrarAfin);
			
			if(!seguirDescifrandoAfin)
			{
				return;
			}
		}			
		
		$("#columnaCriptogramaAfinDescifrado"+i).css("backgroundColor", "transparent");
		
		if(!seguirDescifrandoAfin)
		{
			return;
		}
	}	
	
	posicion = $("#textoMensajeClaroAfinDescifrado").offset().top;
	$("html, body").animate({scrollTop: posicion}, 1000);
	
	if(!seguirDescifrandoAfin)
	{
		return;
	}
	
	$("#btn-velocidadDAfin").show();
	$("#btn-descifrarAfin-descifrado").show();
	$("#btn-cancelarDescifrarAfin-descifrado").hide();	
	
	toastr.options.timeOut = "1000";
	toastr['success']('Texto descifrado');
	$("#textoMensajeClaroAfinDescifrado").val(cadenaDescifrada.toLowerCase());	
}

function validarEntradaCifradoAfin()
{
	var mensaje = "";
	var texto = $('#textoMensajePlanoAfinCifrado').val().replace(/ /g,"");	

	if (texto.length < 1 || texto.length > 10)
	{
		mensaje = "El mensaje claro debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El mensaje claro sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}	

	return mensaje;
}

function validarACifradoAfin()
{
	var mensaje = "";	
	var valorA= Number($('#valorAafinC').val());
	var inversoA;
	
	if (valorA<2 || valorA>25)
	{
		mensaje = "El valor de la llave 'a' debe ser un número entre 2 y 25.";
	}
	else
	{
		inversoA= tieneInversoMultiplicativo(valorA);
		
		if(inversoA=="No tiene inverso")
		{
			mensaje = "El valor de la llave 'a' no tiene inverso multiplicativo (recuerda que el máximo común divisor de 'a' y 26 debe ser 1).";
		}
	}

	return mensaje;
}

function validarBCifradoAfin()
{
	var mensaje = "";	
	var valorB= Number($('#valorBafinC').val());
	
	if (valorB<1 || valorB>100)
	{
		mensaje = "El valor de la llave 'b' debe ser un número entre 1 y 100.";		
	}	

	return mensaje;
}

function validarEntradaDescifradoAfin()
{
	var mensaje = "";
	var texto = $('#textoCriptogramaAfinDescifrado').val().replace(/ /g,"");	

	if (texto.length < 1 || texto.length > 10)
	{
		mensaje = "El criptograma debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El criptograma sólo debe contener letras del alfabeto latino internacional moderno (<strong>a</strong> - <strong>z</strong>).";
	}	

	return mensaje;
}

function validarADescifradoAfin()
{
	var mensaje = "";	
	var valorA= Number($('#valorAafinD').val());
	var inversoA;
	
	if (valorA<2 || valorA>25)
	{
		mensaje = "El valor de la llave 'a' debe ser un número entre 2 y 25.";
	}
	else
	{
		inversoA= tieneInversoMultiplicativo(valorA);
		
		if(inversoA=="No tiene inverso")
		{
			mensaje = "El valor de la llave 'a' no tiene inverso multiplicativo (recuerda que el máximo común divisor de 'a' y 26 debe ser 1).";
		}
	}

	return mensaje;
}

function validarBDescifradoAfin()
{
	var mensaje = "";	
	var valorB= Number($('#valorBafinD').val());
	
	if (valorB<1 || valorB>100)
	{
		mensaje = "El valor de la llave 'b' debe ser un número entre 1 y 100.";		
	}	

	return mensaje;
}

$(document).ready(function()
{
	$("#CifradoRapidoAfin").click(function(){
	$("#btn-cifrarAfin-cifrado").html('Cifrado Rápido');
	$("#btn-cifrarAfin-cifrado").val(1);
	});
	$("#CifradoNormalAfin").click(function(){
		$("#btn-cifrarAfin-cifrado").html('Cifrado Normal');
		$("#btn-cifrarAfin-cifrado").val(2);
	});
	$("#CifradoLentoAfin").click(function(){
		$("#btn-cifrarAfin-cifrado").html('Cifrado Lento');
		$("#btn-cifrarAfin-cifrado").val(3);
	});
	
	$("#DescifradoRapidoAfin").click(function(){
		$("#btn-descifrarAfin-descifrado").html('Descifrado Rápido');
		$("#btn-descifrarAfin-descifrado").val(1);
	});
	$("#DescifradoNormalAfin").click(function(){
		$("#btn-descifrarAfin-descifrado").html('Descifrado Normal');
		$("#btn-descifrarAfin-descifrado").val(2);
	});
	$("#DescifradoLentoAfin").click(function(){
		$("#btn-descifrarAtbash-descifrado").html('Descifrado Lento');
		$("#btn-descifrarAfin-descifrado").val(3);
	});
	
	$("#textoMensajePlanoAfinCifrado").keyup(function()
	{
		var mensaje = validarEntradaCifradoAfin();
		
		var mensaje2 = validarACifradoAfin();
		var mensaje3 = validarBCifradoAfin();

		if (mensaje.length != 0)
		{
			$("#textoPlanoAfin-error").remove();
			$("#textoMensajePlanoAfinCifrado").parent().append('<div id="textoPlanoAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoMensajePlanoAfinCifrado").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		} else
		{
			$("#textoPlanoAfin-error").remove();
			$("#textoMensajePlanoAfinCifrado").parent().removeClass('has-error has-feedback');
			
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-cifrarAfin-cifrado").attr("disabled", false);
			}
		}
	});
	
	$("#valorAafinC").keyup(function()
	{
		var mensaje = validarACifradoAfin();
		
		var mensaje2 = validarEntradaCifradoAfin();
		var mensaje3 = validarBCifradoAfin();

		if (mensaje.length != 0) {
			$("#valorAafinCAfin-error").remove();
			$("#valorAafinC").parent().append('<div id="valorAafinCAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorAafinC").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		} else{
			$("#valorAafinCAfin-error").remove();
			$("#valorAafinC").parent().removeClass('has-error has-feedback');
			
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-cifrarAfin-cifrado").attr("disabled", false);
			}			
		}
	});
	
	$("#valorBafinC").keyup(function()
	{
		var mensaje = validarBCifradoAfin();
		
		var mensaje2 = validarEntradaCifradoAfin();
		var mensaje3 = validarACifradoAfin();

		if (mensaje.length != 0)
		{
			$("#valorBafinCAfin-error").remove();
			$("#valorBafinC").parent().append('<div id="valorBafinCAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorBafinC").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		} else
		{
			$("#valorBafinCAfin-error").remove();
			$("#valorBafinC").parent().removeClass('has-error has-feedback');
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-cifrarAfin-cifrado").attr("disabled", false);
			}			
		}
	});
	
	$("#textoCriptogramaAfinDescifrado").keyup(function()
	{
		var mensaje = validarEntradaDescifradoAfin();
		
		var mensaje2 = validarADescifradoAfin();
		var mensaje3 = validarBDescifradoAfin();

		if (mensaje.length != 0)
		{
			$("#criptogramaAfin-error").remove();
			$("#textoCriptogramaAfinDescifrado").parent().append('<div id="criptogramaAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoCriptogramaAfinDescifrado").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		} else
		{
			$("#criptogramaAfin-error").remove();
			$("#textoCriptogramaAfinDescifrado").parent().removeClass('has-error has-feedback');
			
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-descifrarAfin-descifrado").attr("disabled", false);
			}
		}
	});
	
	$("#valorAafinD").keyup(function()
	{
		var mensaje = validarADescifradoAfin();
		
		var mensaje2 = validarEntradaDescifradoAfin();
		var mensaje3 = validarBDescifradoAfin();

		if (mensaje.length != 0) {
			$("#valorAafinDAfin-error").remove();
			$("#valorAafinD").parent().append('<div id="valorAafinDAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorAafinD").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		} else{
			$("#valorAafinDAfin-error").remove();
			$("#valorAafinD").parent().removeClass('has-error has-feedback');
			
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-descifrarAfin-descifrado").attr("disabled", false);
			}			
		}
	});
	
	$("#valorBafinD").keyup(function()
	{
		var mensaje = validarBDescifradoAfin();
		
		var mensaje2 = validarEntradaDescifradoAfin();
		var mensaje3 = validarADescifradoAfin();

		if (mensaje.length != 0) {
			$("#valorBafinDAfin-error").remove();
			$("#valorBafinD").parent().append('<div id="valorBafinDAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorBafinD").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		} else{
			$("#valorBafinDAfin-error").remove();
			$("#valorBafinD").parent().removeClass('has-error has-feedback');
			
			if(mensaje2.length==0&&mensaje3.length==0)
			{
				$("#btn-descifrarAfin-descifrado").attr("disabled", false);
			}			
		}
	});
	
	$("#btn-cifrarAfin-cifrado").click(function()
	{
		var mensaje= validarEntradaCifradoAfin();
		var mensaje2= validarACifradoAfin();
		var mensaje3= validarBCifradoAfin();
		
		if(mensaje.length!=0)
		{
			$("#textoPlanoAfin-error").remove();
			$("#textoMensajePlanoAfinCifrado").parent().append('<div id="textoPlanoAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoMensajePlanoAfinCifrado").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		}
		else if(mensaje2.length!=0)
		{
			$("#valorAafinCAfin-error").remove();
			$("#valorAafinC").parent().append('<div id="valorAafinCAfin-error" class="text-danger">&nbsp;'+mensaje2+'</div>');
			$("#valorAafinC").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		}
		else if(mensaje3.length!=0)
		{
			$("#valorBafinCAfin-error").remove();
			$("#valorBafinC").parent().append('<div id="valorBafinCAfin-error" class="text-danger">&nbsp;'+mensaje3+'</div>');
			$("#valorBafinC").parent().addClass('has-error has-feedback');
			$("#btn-cifrarAfin-cifrado").attr("disabled", true);
		}
		else
		{
			cifrarAfin();
		}
	});
	
	$("#btn-descifrarAfin-descifrado").click(function()
	{
		var mensaje= validarEntradaDescifradoAfin();
		var mensaje2= validarADescifradoAfin();
		var mensaje3= validarBDescifradoAfin();
		
		if(mensaje.length!=0)
		{
			$("#criptogramaAfin-error").remove();
			$("#textoCriptogramaAfinDescifrado").parent().append('<div id="criptogramaAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#textoCriptogramaAfinDescifrado").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		}
		else if(mensaje2.length!=0)
		{
			$("#valorAafinDAfin-error").remove();
			$("#valorAafinD").parent().append('<div id="valorAafinDAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorAafinD").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		}
		else if(mensaje3.length!=0)
		{
			$("#valorBafinDAfin-error").remove();
			$("#valorBafinD").parent().append('<div id="valorBafinDAfin-error" class="text-danger">&nbsp;'+mensaje+'</div>');
			$("#valorBafinD").parent().addClass('has-error has-feedback');
			$("#btn-descifrarAfin-descifrado").attr("disabled", true);
		}
		else
		{
			descifrarAfin();
		}
	});
		
	
	$("#btn-cancelarCifrarAfin-cifrado").click(function()
	{
		seguirCifrandoAfin= false;
		
		limpiaPanelAfinCifrado();

		$("#btn-velocidadCAfin").show();
		$("#btn-cifrarAfin-cifrado").show();
		$("#btn-cancelarCifrarAfin-cifrado").hide();
	});
	
	$("#btn-cancelarDescifrarAfin-descifrado").click(function()
	{		
		seguirDescifrandoAfin= false;
		
		limpiaPanelAfinDescifrado();

		$("#btn-velocidadDAfin").show();
		$("#btn-descifrarAfin-descifrado").show();
		$("#btn-cancelarDescifrarAfin-descifrado").hide();
	});
	
	$("#btn-copiarTextoAfin").click(function()
	{
		if ($("#textoMensajeCifradoAfinCifrado").val()=='')
		{
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		}
		else
		{
			$("#textoCriptogramaAfinDescifrado").val($("#textoMensajeCifradoAfinCifrado").val());
		}
	});
});