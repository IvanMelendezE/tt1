var cols = 0, rows = 0;
var matriz = [];
var cancelado = false;
var velocidad = 1;
var azul = 0, negro = 1;

$.fn.scrollView = function () {
  return this.each(function () {
    $('html, body').animate({
      scrollTop: $(this).offset().top
    }, 1000);
  });
}

function mostrarPanelTransposicionColumnas(){
	$("#pnl-Interactivo5").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function crearPanelCifradoTransposicionColumas(){
	for(var i = 0 ; i < rows ; i++){
		$("#table-transposicionColumnas").append('<tr id="TCrow'+i+'"></tr>');
	}
	
	for(var i = 0 ; i < rows*cols ; i++){
        $('#textoCifradoColumnas').append('<label class="circulo" id="TC-Ccell1'+i+'"></label>');
    }

	$("#table-transposicionColumnas").css("text-align","center");
}

function crearPanelDescifradoTransposicionColumas(){
	for(var i = 0 ; i < rows ; i++){
		$("#table-transposicionColumnas2").append('<tr id="TCrow2'+i+'"></tr>');
	}

	for(var i = 0 ; i < rows*cols ; i++){
        $('#textoDescifradoColumnas').append('<label class="circulo" id="TC-MCcell1'+i+'"></label>');
    }

	$("#table-transposicionColumnas2").css("text-align","center");
}

function cerrarPanelTransposicionColumnas(){
	$("#pnl-Interactivo5").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);

	limpiaPanelCifradoTransposicionColumnas();
	limpiaPanelDescifradoTransposicionColumnas();
}

function limpiaPanelCifradoTransposicionColumnas(){
	$("#table-transposicionColumnas").empty();
	$('#textoCifradoColumnas').empty();
	$("#in-textoPlanoTransposicionColumnas").val("");
	$('#in-claveCifradoTransposicionColumnas').val("");
	$("#out-textoCifradoTransposicionColumnas").val("");

	if($('#TCdiv1').is(':visible')) {
        $("#TCdiv1").slideToggle(1000);
    }
}

function limpiaPanelDescifradoTransposicionColumnas(){
	$("#table-transposicionColumnas2").empty();
	$('#textoDescifradoColumnas').empty();
	$("#in-textoPlanoCifradoTransposicionColumnas").val("");
	$('#in-claveDescifradoTransposicionColumnas').val("");
	$("#out-textoDescifradoTransposicionColumnas").val("");

	if($('#TCdiv2').is(':visible')) {
        $("#TCdiv2").slideToggle(1000);
    }
}

function burbuja(clave){
	var aux;
	var ordenados = new Array(clave.length);

	for(var i = 0 ; i < clave.length ; i++){
		ordenados[i] = i;
	}

	for(var i = 0 ; i < (clave.length-1) ; i++){
	    for(j = 0 ; j < (clave.length-i) ; j++){
	        if(clave[j] > clave[j+1]){
				aux = clave[j];
				clave[j] = clave[j+1];
				clave[j+1] = aux;

				aux = ordenados[j];
				ordenados[j] = ordenados[j+1];
				ordenados[j+1] = aux;
	        }
 		}
    }

    return ordenados;
}

async function cifrarTransposicionColumnas(){
	var plano = ($("#in-textoPlanoTransposicionColumnas").val().toLowerCase().replace(/ /g,"")).split("");
    var clave = ($("#in-claveCifradoTransposicionColumnas").val().toLowerCase().replace(/ /g,"")).split("");
    var texto_length = plano.length;

    var cadenaCifrado = "";
    var k = 0, l = 0;
    var matriz = new Array(12);
    var orden;
    
    limpiaPanelCifradoTransposicionColumnas();
    $("#in-textoPlanoTransposicionColumnas").val(plano.join(""));
    $("#in-claveCifradoTransposicionColumnas").val(clave.join(""));

	cols = clave.length;
	rows = (((texto_length/cols - Math.round(texto_length/cols)) < 0.5 && (texto_length/cols - Math.round(texto_length/cols)) > 0)?Math.round(texto_length/cols)+1:Math.round(texto_length/cols)) + 2;

	crearPanelCifradoTransposicionColumas();

	$('#TCdiv1').html('Se toman los caracteres que conforman la llave y se enumeran por orden alfabético: a=0 , b=1, ..., z=25.');
	$('#TCdiv1').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(4000);
	
	if(cancelado){
        return;
    }

    $('#TCdiv1').scrollView();

	for (var i = 0 ; i < rows && !cancelado ; i++) {
		for(var j = 0 ; j < cols && !cancelado ; j++){
			$("#TCrow"+i).append('<td id="TCcell' + i + '-' + j + '"></td>');
		}
	}

	if(cancelado){
        return;
    }

	for(var i = 0 ; i < cols && !cancelado ; i++){
		$("#TCcell0-"+i).addClass('title-table');
		$("#TCcell1-"+i).addClass('title-table2');
	}

	if(cancelado){
        return;
    }

	orden = new Array(cols);

	for (var i = 0 ; i < 12 && !cancelado ; i++) {
	   matriz[i] = new Array(cols);
	}

	if(cancelado){
        return;
    }

	//Colocar Llave
	for(var i = 0 ; i < cols && !cancelado ; i++){
		$("#TCcell0-"+i).html(clave[i]);
		parpadeo("#TCcell0-"+i, 1*velocidad, azul);

		await sleep(1000*velocidad);

		removeparpadeo("#TCcell0-"+i, 1*velocidad, azul);
	}

	for(var i = 0 ; i < cols && !cancelado ; i++){
		$("#TCcell1-"+i).html(clave[i].charCodeAt()-97);

		parpadeo("#TCcell1-"+i, 1*velocidad, azul);

		await sleep(1000*velocidad);

		removeparpadeo("#TCcell1-"+i, 1*velocidad, azul);

		matriz[1][i] = clave[i].charCodeAt()-65;
	}

	if(cancelado){
        return;
    }

	orden = burbuja(matriz[1]);

	$('#TCdiv1').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TCdiv1').html('El mensaje en claro se reescribe debajo de la llave enumerada formando varios renglones. Se usa la letra X para completar un renglón.');
	$('#TCdiv1').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(5000);
	
	if(cancelado){
        return;
    }

    $('#TCdiv1').scrollView();

	for(var i = 2 ; i < rows && !cancelado ; i++){
		for(var j = 0 ; j < cols && !cancelado ; j++){
			if(plano[k] != undefined){
				$("#TCcell"+i+"-"+j).html(plano[k]);

				parpadeo("#TCcell"+i+"-"+j, 1*velocidad, azul);

				await sleep(1000*velocidad);

				removeparpadeo("#TCcell"+i+"-"+j, 1*velocidad, azul);
console.log(plano[k]);
				matriz[i][j] = plano[k++];console.log("shi");
			}
			else{
				$("#TCcell"+i+"-"+j).html("X");

				parpadeo("#TCcell"+i+"-"+j, 1*velocidad, azul);

				await sleep(1000*velocidad);

				removeparpadeo("#TCcell"+i+"-"+j, 1*velocidad, azul);

				matriz[i][j] = "x";	
			}
		}
	}

	$('#TCdiv1').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TCdiv1').html('El criptograma se obtiene escribiendo columna por columna en orden numérico.');
	$('#TCdiv1').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(4000);
	
	if(cancelado){
        return;
    }
    $('#TCdiv1').scrollView();

	for(var i = 0 ; i < cols && !cancelado ; i++){
		//$("#TCcell0-"+orden[i]).addClass('seleccionado');
		$("#TCcell1-"+orden[i]).addClass('seleccionado');

		for(var j = 2 ; j < rows && !cancelado ; j++){
			cadenaCifrado = cadenaCifrado + matriz[j][orden[i]].toUpperCase();

			parpadeo("#TCcell"+j+"-"+orden[i], 1*velocidad, azul);
			parpadeo("#TC-Ccell1"+l, 1*velocidad,negro);
        	$("#TC-Ccell1"+l).html(matriz[j][orden[i]].toUpperCase());

			await sleep(1000*velocidad);

			removeparpadeo("#TCcell"+j+"-"+orden[i], 1*velocidad, azul);
			removeparpadeo("#TC-Ccell1"+l, 1*velocidad,negro);
			l++;
		}

		//$("#TCcell0-"+orden[i]).removeClass('seleccionado');
		$("#TCcell1-"+orden[i]).removeClass('seleccionado');
	}

	if(cancelado){
        return;
    }

    $("#out-textoCifradoTransposicionColumnas").val(cadenaCifrado);
    $("#btn-velocidadCTransposicionColumnas").show();
    $("#btn-cifrarColumnas").show();
    $("#btn-cancelarCifrarColumnas").hide();

    if(!cancelado){
        $('#TCdiv1').slideToggle(1000);
        toastr.options.timeOut = "1000";
        toastr['success']('Texto cifrado');
        cancelado = true;
    }
}

async function descifrarTransposicionColumnas(){
	var cifrado = ($("#in-textoPlanoCifradoTransposicionColumnas").val().toUpperCase()).split("");
    var clave = ($("#in-claveDescifradoTransposicionColumnas").val().toUpperCase()).split("");
    var texto_length = cifrado.length;

    var cadenaDescifrado = "";
    var k = 0, l = 0;
    var matriz = new Array(12);
    var orden;
    
	limpiaPanelDescifradoTransposicionColumnas();
	$("#in-textoPlanoCifradoTransposicionColumnas").val(cifrado.join(""));
	$("#in-claveDescifradoTransposicionColumnas").val(clave.join(""));

	cols = $('#in-claveDescifradoTransposicionColumnas').val().split("").length;
	rows = (((texto_length/cols - Math.round(texto_length/cols)) < 0.5 && (texto_length/cols - Math.round(texto_length/cols)) > 0)?Math.round(texto_length/cols)+1:Math.round(texto_length/cols)) + 2;
	
	crearPanelDescifradoTransposicionColumas();

	if(cancelado){
        return;
    }

    $('#TCdiv2').html('Se toman los caracteres que conforman la clave y se enumeran por orden alfabético: a=0 , b=1, ..., z=25.');
	$('#TCdiv2').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(6000);
	
	if(cancelado){
        return;
    }

    $('#TCdiv2').scrollView();

	if(cancelado){
        return;
    }

    for (var i = 0 ; i < rows && !cancelado ; i++) {
		for(var j = 0 ; j < cols && !cancelado ; j++){
			$("#TCrow2"+i).append('<td id="TCcell2' + i + '-' + j + '"></td>');
		}
	}

	if(cancelado){
        return;
    }

	for(var i = 0 ; i < cols && !cancelado ; i++){
		$("#TCcell20-"+i).addClass('title-table');
		$("#TCcell21-"+i).addClass('title-table2');
	}

	if(cancelado){
        return;
    }

	orden = new Array(cols);

	if(cancelado){
        return;
    }

    for (var i = 0 ; i < 12 && !cancelado ; i++) {
	   matriz[i] = new Array(cols);
	}

	if(cancelado){
        return;
    }

	//Colocar Clave
	for(var i = 0 ; i < cols && !cancelado; i++){
		$("#TCcell20-"+i).html(clave[i]);
		parpadeo("#TCcell20-"+i, 1*velocidad, azul);

		await sleep(1000*velocidad);

		removeparpadeo("#TCcell20-"+i, 1*velocidad, azul);
	}

	if(cancelado){
        return;
    }

	for(var i = 0 ; i < cols && !cancelado ; i++){
		$("#TCcell21-"+i).html(clave[i].charCodeAt()-65);
		parpadeo("#TCcell21-"+i, 1*velocidad, azul);

		await sleep(1000*velocidad);

		removeparpadeo("#TCcell21-"+i, 1*velocidad, azul);

		matriz[1][i] = clave[i].charCodeAt()-65;
	}

	if(cancelado){
        return;
    }

	orden = burbuja(matriz[1]);

	$('#TCdiv2').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TCdiv2').html('Debajo de la clave se escribe el criptograma por columnas en orden numerico. Se escriben n caracteres en cada columna (n = criptograma/clave = '+cifrado.length+'/'+clave.length+'= '+(cifrado.length/clave.length)+').');
	$('#TCdiv2').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(7000);
	
	if(cancelado){
        return;
    }

    $('#TCdiv2').scrollView();

	if(cancelado){
        return;
    }

    for(var i = 0 ; i < cols && !cancelado ; i++){
    	$("#TCcell21-"+orden[i]).addClass('seleccionado');

		for(var j = 2 ; j < rows && !cancelado ; j++){
			$("#TCcell2"+j+"-"+orden[i]).html(cifrado[k]);

			parpadeo("#TCcell2"+j+"-"+orden[i], 1*velocidad, azul);

			await sleep(1000*velocidad);

			removeparpadeo("#TCcell2"+j+"-"+orden[i], 1*velocidad, azul);

			matriz[j][orden[i]] = cifrado[k++];
		}

		$("#TCcell21-"+orden[i]).removeClass('seleccionado');
	}

	if(cancelado){
        return;
    }

	$('#TCdiv2').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TCdiv2').html('El mensaje en claro se obtiene leyendo fila por fila.');
	$('#TCdiv2').slideToggle(1000);

	if(cancelado){
        return;
    }

    await sleep(4000);
	
	if(cancelado){
        return;
    }

    $('#TCdiv2').scrollView();

	for(var i = 2 ; i < rows && !cancelado ; i++){
		for(var j = 0 ; j < cols && !cancelado; j++){
			cadenaDescifrado = cadenaDescifrado + matriz[i][j].toLowerCase();

			parpadeo("#TCcell2"+i+"-"+j, 1*velocidad, azul);
			parpadeo("#TC-MCcell1"+l, 1*velocidad, negro);
            $("#TC-MCcell1"+l).html(matriz[i][j].toLowerCase());

			await sleep(1000*velocidad);

			removeparpadeo("#TCcell2"+i+"-"+j, 1*velocidad, azul);
			removeparpadeo("#TC-MCcell1"+l, 1*velocidad, negro);
			l++;
		}
	}

	if(cancelado){
        return;
    }

    $("#out-textoDescifradoTransposicionColumnas").val(cadenaDescifrado);
    $("#btn-velocidadDTransposicionColumnas").show();
    $("#btn-descifrarColumnas").show();
    $("#btn-cancelarDescifrarColumnas").hide();

    if(!cancelado){
        $('#TCdiv2').slideToggle(1000);
        toastr.options.timeOut = "1000";
        toastr['success']('Texto descifrado');
    }
}

function validarEntradaTextoCTransposicionColumnas(){
	var mensaje = "";
	var texto = $('#in-textoPlanoTransposicionColumnas').val().replace(/ /g,"");

	if (texto.length < 1 || texto.length > 10) {
		mensaje = "El mensaje claro debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaLlaveCTransposicionColumnas(){
	var mensaje = "";
	var clave = $('#in-claveCifradoTransposicionColumnas').val();

	if(clave.indexOf(' ') >= 0){
		mensaje = "La llave no debe contener espacios.";
	}
	else if (clave.length < 1 || clave.length > 10) {
		mensaje = "La llave debe contener entre 1 y 10 caracteres.";
	}
	else if(!clave.match(/^[a-zA-Z]+$/)){
		mensaje = "La llave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaTextoDTransposicionColumnas(){
	var mensaje = "";
	var texto = $('#in-textoPlanoCifradoTransposicionColumnas').val();

	if(texto.indexOf(' ') >= 0){
		mensaje = "El criptograma no debe contener espacios.";
	}
	else if (texto.length < 1 || texto.length > 10) {
		mensaje = "El criptograma debe contener entre 1 y 10 caracteres.";
	}
	else if(!texto.match(/^[a-zA-Z]+$/)){
		mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

function validarEntradaLlaveDTransposicionColumnas(){
	var mensaje = "";
	var clave = $('#in-claveDescifradoTransposicionColumnas').val();

	if(clave.indexOf(' ') >= 0){
		mensaje = "La clave no debe contener espacios.";
	}
	else if (clave.length < 1 || clave.length > 10) {
		mensaje = "La clave debe contener entre 1 y 10 caracteres.";
	}
	else if(!clave.match(/^[a-zA-Z]+$/)){
		mensaje = "La clave solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
	}

	return mensaje;
}

$(document).ready(function(){
	$("#tipoTransposicionColumnasC1").click(function(){
        $("#btn-cifrarColumnas").html('Cifrado Rápido');
        $("#btn-cifrarColumnas").val(1);
    });
    $("#tipoTransposicionColumnasC2").click(function(){
        $("#btn-cifrarColumnas").html('Cifrado Normal');
        $("#btn-cifrarColumnas").val(2);
    });
    $("#tipoTransposicionColumnasC3").click(function(){
        $("#btn-cifrarColumnas").html('Cifrado Lento&nbsp;');
        $("#btn-cifrarColumnas").val(3);
    });

    $("#tipoTransposicionColumnasD1").click(function(){
        $("#btn-descifrarColumnas").html('Descifrado Rápido');
        $("#btn-descifrarColumnas").val(1);
    });
    $("#tipoTransposicionColumnasD2").click(function(){
        $("#btn-descifrarColumnas").html('Descifrado Normal');
        $("#btn-descifrarColumnas").val(2);
    });
    $("#tipoTransposicionColumnasD3").click(function(){
        $("#btn-descifrarColumnas").html('Descifrado Lento&nbsp;');
        $("#btn-descifrarColumnas").val(3);
    });

    $("#in-textoPlanoTransposicionColumnas").keyup(function(){
        var mensaje = validarEntradaTextoCTransposicionColumnas();

        if (mensaje.length != 0) {
            $("#textoPlanoTransposicionColumnas-error").remove();
            $("#in-textoPlanoTransposicionColumnas").parent().parent().append('<div id="textoPlanoTransposicionColumnas-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoTransposicionColumnas").addClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", true);
        } else{
            $("#textoPlanoTransposicionColumnas-error").remove();
            $("#in-textoPlanoTransposicionColumnas").removeClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", false);
        }
    });

    $("#in-claveCifradoTransposicionColumnas").keyup(function(){
        var mensaje = validarEntradaLlaveCTransposicionColumnas();

        if (mensaje.length != 0) {
            $("#claveCTransposicionColumnas-error").remove();
            $("#in-claveCifradoTransposicionColumnas").parent().parent().append('<div id="claveCTransposicionColumnas-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-claveCifradoTransposicionColumnas").addClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", true);
        } else{
            $("#claveCTransposicionColumnas-error").remove();
            $("#in-claveCifradoTransposicionColumnas").removeClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", false);
        }
    });

    $("#in-textoPlanoCifradoTransposicionColumnas").keyup(function(){
        var mensaje = validarEntradaTextoDTransposicionColumnas();

        $("#textoPlanoCifradoTransposicionColumnas-info").remove();

        if (mensaje.length != 0) {
            $("#textoPlanoCifradoTransposicionColumnas-error").remove();
            $("#in-textoPlanoCifradoTransposicionColumnas").parent().parent().append('<div id="textoPlanoCifradoTransposicionColumnas-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoCifradoTransposicionColumnas").addClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", true);
        } else{
            $("#textoPlanoCifradoTransposicionColumnas-error").remove();
            $("#in-textoPlanoCifradoTransposicionColumnas").removeClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", false);
        }
    });

    $("#in-claveDescifradoTransposicionColumnas").keyup(function(){
        var mensaje = validarEntradaLlaveDTransposicionColumnas();

        $("#textoPlanoCifradoTransposicionColumnas-info").remove();

        if (mensaje.length != 0) {
            $("#claveDTransposicionColumnas-error").remove();
            $("#in-claveDescifradoTransposicionColumnas").parent().parent().append('<div id="claveDTransposicionColumnas-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-claveDescifradoTransposicionColumnas").addClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", true);
        } else{
            $("#claveDTransposicionColumnas-error").remove();
            $("#in-claveDescifradoTransposicionColumnas").removeClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", false);
        }
    });

	$("#btn-cifrarColumnas").click(function(){
		var mensajetexto = validarEntradaTextoCTransposicionColumnas();
		var mensajeclave = validarEntradaLlaveCTransposicionColumnas();

		if(mensajetexto.length > 0){
			$("#textoPlanoTransposicionColumnas-error").remove();
            $("#in-textoPlanoTransposicionColumnas").parent().parent().append('<div id="textoPlanoTransposicionColumnas-error" class="text-danger">&nbsp;'+mensajetexto+'</div>');
            $("#in-textoPlanoTransposicionColumnas").addClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", true);
		}
		else if(mensajeclave.length > 0){
			$("#claveCTransposicionColumnas-error").remove();
            $("#in-claveCifradoTransposicionColumnas").parent().parent().append('<div id="claveCTransposicionColumnas-error" class="text-danger">&nbsp;'+mensajeclave+'</div>');
            $("#in-claveCifradoTransposicionColumnas").addClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", true);
		}
		else{
			$("#textoPlanoTransposicionColumnas-error").remove();
            $("#claveCTransposicionColumnas-error").remove();
            $("#in-textoPlanoTransposicionColumnas").removeClass('input-error');
            $("#in-claveCifradoTransposicionColumnas").removeClass('input-error');
            $("#btn-cifrarColumnas").attr("disabled", false);

            if($('#btn-cifrarColumnas').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-cifrarColumnas').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-velocidadCTransposicionColumnas").hide();
            $("#btn-cifrarColumnas").hide();
            $("#btn-cancelarCifrarColumnas").show();
            cancelado = false;
            
            cifrarTransposicionColumnas();
		}
	});

	$("#btn-cancelarCifrarColumnas").click(function(){
        cancelado = true;

        limpiaPanelCifradoTransposicionColumnas();

        $("#btn-cifrarColumnas").show();
        $("#btn-velocidadCTransposicionColumnas").show();
        $("#btn-cancelarCifrarColumnas").hide();
    });

    $("#btn-cancelarDescifrarColumnas").click(function(){
        cancelado = true;

        limpiaPanelDescifradoTransposicionColumnas();

        $("#btn-descifrarColumnas").show();
        $("#btn-velocidadDTransposisionColumnas").show();
        $("#btn-cancelarDescifrarColumnas").hide();
    });

    $("#btn-copiarTextoColumnas").click(function(){
        if ($("#out-textoCifradoTransposicionColumnas").val()==''){
            $("#textoPlanoCifradoTransposicionColumnas-info").remove();
            $("#in-textoPlanoCifradoTransposicionColumnas").parent().parent().append('<div id="textoPlanoCifradoTransposicionColumnas-info" class="text-info">Primero debes cifrar un mensaje</div>');
        } else {
            $("#textoPlanoCifradoTransposicionColumnas-info").remove();
            $("#in-textoPlanoCifradoTransposicionColumnas").val($("#out-textoCifradoTransposicionColumnas").val());
            $("#in-claveDescifradoTransposicionColumnas").val($("#in-claveCifradoTransposicionColumnas").val());
        }
    });

    $("#btn-descifrarColumnas").click(function(){
        var mensajetexto = validarEntradaTextoDTransposicionColumnas();
		var mensajeclave = validarEntradaLlaveDTransposicionColumnas();

		if(mensajetexto.length > 0){
			$("#textoPlanoCTransposicionColumnas-error").remove();
            $("#in-textoPlanoTransposicionColumnas").parent().parent().append('<div id="textoPlanoCTransposicionColumnas-error" class="text-danger">&nbsp;'+mensajetexto+'</div>');
            $("#in-textoPlanoCifradoTransposicionColumnas").addClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", true);
		}
		else if(mensajeclave.length > 0){
			$("#claveDTransposicionColumnas-error").remove();
            $("#in-claveDescifradoTransposicionColumnas").parent().parent().append('<div id="claveDTransposicionColumnas-error" class="text-danger">&nbsp;'+mensajeclave+'</div>');
            $("#in-claveDescifradoTransposicionColumnas").addClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", true);
		}
		else{
			$("#textoPlanoCifradoTransposicionColumnas-error").remove();
            $("#claveDTransposicionColumnas-error").remove();
            $("#in-textoPlanoCifradoTransposicionColumnas").removeClass('input-error');
            $("#in-claveDescifradoTransposicionColumnas").removeClass('input-error');
            $("#btn-descifrarColumnas").attr("disabled", false);

            if($('#btn-descifrarColumnas').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-descifrarColumnas').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-velocidadDTransposicionColumnas").hide();
            $("#btn-descifrarColumnas").hide();
            $("#btn-cancelarDescifrarColumnas").show();
            cancelado = false;
            
            descifrarTransposicionColumnas();
		}
    });
});