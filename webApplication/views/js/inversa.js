function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var ic=0, idc=0;
var tCAdd=600, tDcAdd=600;
var tCRemove=100, tDcRemove=100;

function mostrarPanelInversa(){
	$("#pnl-InteractivoInversa").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelInversa(){
	$("#pnl-InteractivoInversa").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelInversa();
}

function limpiaPanelInversa(){
	$("#textoPlanoInversaC").empty();
	$("#textoCifradoInversaC").empty();
	$("#textoPlanoInversaD").empty();
	$("#textoCifradoInversaD").empty();
	$("#in-txtPlanoInversa").val("");
	$("#out-txtCifradoInversa").val("");
	$("#in-txtCifradoInversa").val("");
	$("#out-txtPlanoInversa").val("");
	$("#txtPlanoInversa-error").remove();
    $("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
    $("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoInversa-error").remove();
    $("#btn-cifrarInversa").show();
    $("#btn-tipoCiInversa").show();
    $("#btn-cancelarCifrarInversa").hide();
    $("#btn-descifrarInversa").show();
    $("#btn-tipoDeInversa").show();
    $("#btn-cancelarDescifrarInversa").hide();
}

function pararAnimacionInversa(){
	ic=999;
	idc=999;
    $("#btn-copiarTextoInversa").removeAttr("disabled");
    $("#textoPlanoInversaD").empty();
	$("#textoCifradoInversaD").empty();
	$("#textoPlanoInversaC").empty();
	$("#textoCifradoInversaC").empty();
	$("#infoAnimacionCiInversa").hide();
	$("#infoAnimacionDeInversa").hide();
	$("#txtPlanoInversa-error").remove();
    $("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
    $("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoInversa-error").remove();
    $("#btn-cifrarInversa").show();
    $("#btn-tipoCiInversa").show();
    $("#btn-cancelarCifrarInversa").hide();
    $("#btn-descifrarInversa").show();
    $("#btn-tipoDeInversa").show();
    $("#btn-cancelarDescifrarInversa").hide();
}

async function cifrarInversa(){
    var plano = ($("#in-txtPlanoInversa").val().toUpperCase()).split("");
    var cifrado = [];
    var cadenaCifrado;
    var numeroInverso = plano.length-1;
    ic = 0;
    for (var i = 0; i <= plano.length-1; i++) {
        if(plano[i] == ' ') {
           plano.splice(i, 1);
        }
    }
	$("#infoAnimacionCiInversa").fadeIn();
	await sleep(tDcAdd);
    for (ic = 0; ic <= plano.length-1; ic++) {
		$("#textoPlanoInversaC").append('<label id="abcPlano'+numeroInverso+'C" class="circulo">'+plano[ic].toLowerCase()+'</label>');
		numeroInverso = numeroInverso - 1;
    }
    // ANIMATION
    $("#textoPlanoInversaC").addClass('parpadeo');
    await sleep(tCAdd);
    $("#textoPlanoInversaC").removeClass('parpadeo');
    await sleep(tCRemove);
    // END ANIMATION
    ic = 0;
	cifrado = plano.reverse();
	for (ic = 0; ic <= cifrado.length-1; ic++) {
		$("#textoCifradoInversaC").append('<label id="abcCifrado'+ic+'C" class="circulo" style="display:none">'+cifrado[ic]+'</label>');
    }
    ic = 0;
	while (ic <= plano.length-1) {
     	$("#abcCifrado"+ic+"C").show();
     	
    	// ANIMATION
    	$("#abcPlano"+ic+"C").addClass('parpadeo');
    	$("#abcCifrado"+ic+"C").addClass('parpadeo');
    	await sleep(tCAdd);
    	$("#abcPlano"+ic+"C").removeClass('parpadeo');
    	$("#abcCifrado"+ic+"C").removeClass('parpadeo');
    	await sleep(tCRemove);
    	// END ANIMATION
		ic++;
    }
    if (ic <= plano.length && ic!=999) {
	    cadenaCifrado = cifrado.join("");
		$("#out-txtCifradoInversa").val(cadenaCifrado);
	    toastr.options.timeOut = "1000";
		toastr['success']('Texto cifrado');
        $("#btn-cifrarInversa").show();
        $("#btn-tipoCiInversa").show();
        $("#btn-cancelarCifrarInversa").hide();
	}
}

async function descifrarInversa(){
    var cifrado = ($("#in-txtCifradoInversa").val().toUpperCase()).split("");
    var plano = [];
    var cadenaDescifrado;
    var numeroInverso = cifrado.length-1;
    idc = 0;
    for (var i = 0; i <= cifrado.length-1; i++) {
        if(cifrado[i] == ' ') {
           cifrado.splice(i, 1);
        }
    }
    $("#infoAnimacionDeInversa").fadeIn();
    await sleep(tDcAdd);
    for (idc = 0; idc <= cifrado.length-1; idc++) {
		$("#textoCifradoInversaD").append('<label id="abcCifrado'+numeroInverso+'D" class="circulo">'+cifrado[idc]+'</label>');
		numeroInverso = numeroInverso - 1;
    }
    // ANIMATION
    $("#textoCifradoInversaD").addClass('parpadeo');
    await sleep(tDcAdd);
    $("#textoCifradoInversaD").removeClass('parpadeo');
    await sleep(tDcRemove);
    // END ANIMATION
    idc = 0;
	plano = cifrado.reverse();
	for (idc = 0; idc <= plano.length-1; idc++) {
		$("#textoPlanoInversaD").append('<label id="abcPlano'+idc+'D" class="circulo" style="display:none">'+plano[idc].toLowerCase()+'</label>');
    }
    idc = 0;
	while (idc <= cifrado.length-1) {
     	$("#abcPlano"+idc+"D").show();
     	
    	// ANIMATION
    	$("#abcCifrado"+idc+"D").addClass('parpadeo');
    	$("#abcPlano"+idc+"D").addClass('parpadeo');
    	await sleep(tDcAdd);
    	$("#abcCifrado"+idc+"D").removeClass('parpadeo');
    	$("#abcPlano"+idc+"D").removeClass('parpadeo');
    	await sleep(tDcRemove);
    	// END ANIMATION
		idc++;
    }
    if (idc <= cifrado.length && idc!=999) {
	    cadenaDecifrado = plano.join("");
		$("#out-txtPlanoInversa").val(cadenaDecifrado.toLowerCase());
	    $("#btn-copiarTextoInversa").removeAttr("disabled");
	    toastr.options.timeOut = "1000";
		toastr['success']('Texto descifrado');
        $("#btn-descifrarInversa").show();
        $("#btn-tipoDeInversa").show();
        $("#btn-cancelarDescifrarInversa").hide();
	}
}

function validarEntradaCifradoInversa(){
    var mensaje = "";
    var texto = $('#in-txtPlanoInversa').val();
    if (texto.length < 1 || texto.length > 20) {
        mensaje = "El mensaje claro debe contener entre 1 y 20 caracteres.";
    } else if(!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaDescifradoInversa(){
    var mensaje = "";
    var texto = $('#in-txtCifradoInversa').val();
    if (texto.length < 1 || texto.length > 20) {
        mensaje = "El criptograma debe contener entre 1 y 20 caracteres.";
    } else if(!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$("#btn-mostrarPanelInversa").click(function(){
		mostrarPanelInversa();
	});
	$("#btn-cerrarPanelInversa").click(function(){
		pararAnimacionInversa();
		cerrarPanelInversa();
	});
	$("#btn-teoriaInversa").click(function(){
		pararAnimacionInversa();
	});
	$("#btn-fundamentosMatematicosInversa").click(function(){
		pararAnimacionInversa();
	});
	$("#btn-cifrarAnimacionInversa").click(function(){
		pararAnimacionInversa();
	});
	$("#btn-descifrarAnimacionInversa").click(function(){
		pararAnimacionInversa();
	});
	$("#btn-cancelarCifrarInversa").click(function(){
        pararAnimacionInversa();
    });
    $("#btn-cancelarDescifrarInversa").click(function(){
        pararAnimacionInversa();
    });
    $("#tipoCiInversa1").click(function(){
        $("#btn-cifrarInversa").html('Cifrado Rápido');
        $("#btn-cifrarInversa").val(1);
    });
    $("#tipoCiInversa2").click(function(){
        $("#btn-cifrarInversa").html('Cifrado Normal');
        $("#btn-cifrarInversa").val(2);
    });
    $("#tipoCiInversa3").click(function(){
        $("#btn-cifrarInversa").html('Cifrado Lento');
        $("#btn-cifrarInversa").val(3);
    });
    $("#tipoDeInversa1").click(function(){
        $("#btn-descifrarInversa").html('Descifrado Rápido');
        $("#btn-descifrarInversa").val(1);
    });
    $("#tipoDeInversa2").click(function(){
        $("#btn-descifrarInversa").html('Descifrado Normal');
        $("#btn-descifrarInversa").val(2);
    });
    $("#tipoDeInversa3").click(function(){
        $("#btn-descifrarInversa").html('Descifrado Lento');
        $("#btn-descifrarInversa").val(3);
    });

    $("#in-txtPlanoInversa").change(function(){
        $("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoInversa-error").remove();
        if ($("#in-txtPlanoInversa").val()=='') {
            $("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
            $("#txtPlanoInversa-error").remove();
        } else{
            var mensaje = validarEntradaCifradoInversa();
            if (mensaje.length == 0){
                $("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
                $("#txtPlanoInversa-error").remove();
            } else {
                $("#in-txtPlanoInversa").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoInversa").parent().parent().append('<div id="txtPlanoInversa-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-txtCifradoInversa").change(function(){
        $("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoInversa-error").remove();
        if ($("#in-txtCifradoInversa").val()=='') {
            $("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
            $("#txtCifradoInversa-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoInversa();
            if (mensaje.length == 0){
                $("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
                $("#txtCifradoInversa-error").remove();
            } else {
                $("#in-txtCifradoInversa").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoInversa").parent().parent().append('<div id="txtCifradoInversa-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

	$("#btn-cifrarInversa").click(function(){
		$("#in-txtPlanoInversa").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoInversa-error").remove();
		$("#out-txtCifradoInversa").val("");
		var mensaje = validarEntradaCifradoInversa();
		if ($("#in-txtPlanoInversa").val()!='' && mensaje.length == 0){
			$("#textoPlanoInversaC").empty();
			$("#textoCifradoInversaC").empty();
			$("#infoAnimacionCiInversa").hide();
			$("#btn-cifrarInversa").hide();
            $("#btn-tipoCiInversa").hide();
            $("#btn-cancelarCifrarInversa").show();
			cifrarInversa();
		} else{
            $("#in-txtPlanoInversa").parent().parent().addClass('has-error has-feedback');
            $("#in-txtPlanoInversa").parent().parent().append('<div id="txtPlanoInversa-error" class="text-danger">&nbsp;'+mensaje+'</div>');
        }
	});

	$("#btn-copiarTextoInversa").click(function(){
		if ($("#out-txtCifradoInversa").val()==''){
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		} else {
			$("#in-txtCifradoInversa").val($("#out-txtCifradoInversa").val());
		}
	});

	$("#btn-descifrarInversa").click(function(){
		$("#in-txtCifradoInversa").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoInversa-error").remove();
		$("#out-txtPlanoInversa").val("");
		var mensaje = validarEntradaDescifradoInversa();
		if ($("#in-txtCifradoInversa").val()!='' && mensaje.length == 0){
			$("#btn-copiarTextoInversa").attr("disabled","disabled");
			$("#textoPlanoInversaD").empty();
			$("#textoCifradoInversaD").empty();
			$("#infoAnimacionDeInversa").hide();
			$("#btn-descifrarInversa").hide();
            $("#btn-tipoDeInversa").hide();
            $("#btn-cancelarDescifrarInversa").show();
			descifrarInversa();
		} else{
            $("#in-txtCifradoInversa").parent().parent().addClass('has-error has-feedback');
            $("#in-txtCifradoInversa").parent().parent().append('<div id="txtCifradoInversa-error" class="text-danger">&nbsp;'+mensaje+'</div>');
        }
	});
	    
});