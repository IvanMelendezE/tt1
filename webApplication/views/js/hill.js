function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var ic=0, idc=0;
var tCAdd=800, tDcAdd=800;
var tCRemove=300, tDcRemove=300;

function mostrarPanelHill(){
	crearPanelHill();
	$("#pnl-InteractivoHill").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelHill(){
	$("#pnl-InteractivoHill").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelHill();
}

function crearPanelHill(){
	for (var i = 1; i <= 3; i++) {
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
		for (var j = 1; j <= 3; j++) {
			$("#m"+i+"ch").append('<td><label id="m'+i+j+'ch" class="circulo" fila="'+i+'" columna="'+j+'">[]</label></td>');
			$("#m"+i+"dh").append('<td><label id="m'+i+j+'dh" class="circulo" fila="'+i+'" columna="'+j+'">[]</label></td>');
		}
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
	}
	for (var i = 1; i <= 3; i++) {
		if (i == 2) {
			$("#m"+i+"ch").append('<td><label class="circulo">*</label></td>');
			$("#m"+i+"dh").append('<td><label class="circulo">*</label></td>');
		} else {
			$("#m"+i+"ch").append('<td><label class="circulo">&nbsp;</label></td>');
			$("#m"+i+"dh").append('<td><label class="circulo">&nbsp;</label></td>');
		}
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"ch").append('<td><label id="mtp'+i+'ch" class="circulo" fila="'+i+'">[]</label></td>');
		$("#m"+i+"dh").append('<td><label id="mtc'+i+'dh" class="circulo" fila="'+i+'">[]</label></td>');
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
		if (i == 2) {
			$("#m"+i+"ch").append('<td><label class="circulo">=</label></td>');
			$("#m"+i+"dh").append('<td><label class="circulo">=</label></td>');
		} else {
			$("#m"+i+"ch").append('<td><label class="circulo">&nbsp;</label></td>');
			$("#m"+i+"dh").append('<td><label class="circulo">&nbsp;</label></td>');
		}
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"ch").append('<td><label id="mrtp'+i+'ch" class="circulo" fila="'+i+'">[]</label></td>');
		$("#m"+i+"dh").append('<td><label id="mrtc'+i+'dh" class="circulo" fila="'+i+'">[]</label></td>');
		$("#m"+i+"ch").append('<td><label class="circulo">|</label></td>');
		$("#m"+i+"dh").append('<td><label class="circulo">|</label></td>');
	}
}

function limpiaPanelHill(){
	$("#textoPlanoHillC").empty();
	$("#textoCifradoHillC").empty();
	$("#textoPlanoHillD").empty();
	$("#textoCifradoHillD").empty();
	$("#in-txtPlanoHill").val("");
	$("#out-txtCifradoHill").val("");
	$("#in-txtCifradoHill").val("");
	$("#out-txtPlanoHill").val("");
    $("#in-txtPlanoHill").removeClass('error');
    $("#txtPlanoHill-error").remove();
    $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
    $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoHill-error").remove();
    $("#btn-cifrarHill").show();
    $("#btn-tipoCiHill").show();
    $("#btn-cancelarCifrarHill").hide();
    $("#btn-descifrarHill").show();
    $("#btn-tipoDeHill").show();
    $("#btn-cancelarDescifrarHill").hide();
	limpiaMatricesHill();
}

function limpiaMatricesHill(){
    for (var i = 1; i <= 3; i++) {
        $("#m"+i+"ch").empty();
        $("#m"+i+"dh").empty();
        $("#mr"+i+"ch").empty();
        $("#mr"+i+"dh").empty();
    }
}

function pararAnimacionHill(){
    ic=999;
    idc=999;
    $("#btn-copiarTextoHill").removeAttr("disabled");
    $("#textoPlanoHillD").empty();
    $("#textoCifradoHillD").empty();
    $("#textoPlanoHillC").empty();
    $("#textoCifradoHillC").empty();
    $("#infoAnimacionCiHill").hide();
    $("#infoAnimacionCiHillExtra").hide();
    $("#infoAnimacionDeHill").hide();
    $("#infoAnimacionDeHillExtra").hide();
    $("#infoLlaveAnimacionCiHill").hide();
    $("#infoLlaveAnimacionDeHill").hide();
    $("#in-txtPlanoHill").removeClass('error');
    $("#txtPlanoHill-error").remove();
    $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoHill-error").remove();
    $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
    $("#btn-cifrarHill").show();
    $("#btn-tipoCiHill").show();
    $("#btn-cancelarCifrarHill").hide();
    $("#btn-descifrarHill").show();
    $("#btn-tipoDeHill").show();
    $("#btn-cancelarDescifrarHill").hide();
    limpiaMatricesHill();
    crearPanelHill();
}

async function cifrarHill(){
    var plano = ($("#in-txtPlanoHill").val().toUpperCase()).split("");
    var cifrado = [];
    var cadenaCifrado;
    var matriz = [5,17,20,9,23,3,2,11,13];
    var matrizPlano = [];
    var matrizRes = [];
    var k = 0;
    var aux = 0;
    ic = 0;
    for (var i = 0; i <= plano.length-1; i++) {
        if(plano[i] == ' ') {
           plano.splice(i, 1);
        }
    }
    var matrizCifrado = new Array(3);
    if (plano.length % 3 == 1) {
        plano[plano.length] ='X';
        plano[plano.length] ='X';
        aux = 1;
    } else if (plano.length % 3 == 2) {
        plano[plano.length] ='X';
        aux = 1;
    }
    if (aux==1) {
        $("#infoAnimacionCiHillExtra").fadeIn();
        await sleep(tCAdd);
    }
    $("#infoLlaveAnimacionCiHill").fadeIn();
    $("#infoAnimacionCiHill").fadeIn();
    await sleep(tCAdd);
	for (var i = 1; i <= 3; i++) {
		matrizCifrado[i] = new Array(3);
	}
	for (var i = 1; i <=3; i++) {
    	for (var j = 1; j <= 3; j++) {
    		matrizCifrado[i][j]=matriz[k];
    		k++;
    	}
    }
	for (var i = 1; i <=3; i++) {
    	for (var j = 1; j <= 3; j++) {
    		$("#m"+i+j+"ch").html(matrizCifrado[i][j]);
    	}
    }
    for (var m = 0; m <= plano.length-1; m++) {
		$("#textoPlanoHillC").append('<label id="abcPlano'+m+'C" class="circulo">'+plano[m].toLowerCase()+'</label>');
    }
    for (var n = 0; n <= plano.length-1; n++) {
		$("#textoCifradoHillC").append('<label id="abcCifrado'+n+'C" class="circulo">&nbsp;</label>');
    }
    // ANIMATION
    $("#textoPlanoHillC").addClass('parpadeo');
    await sleep(tCAdd);
    $("#textoPlanoHillC").removeClass('parpadeo');
    await sleep(tCRemove);
    // END ANIMATION
    while (ic <= plano.length-1) {
    	for (var i = 1; i <= 3; i++) {
    		$("#mtp"+i+"ch").html(plano[ic].toLowerCase());
    		matrizPlano[i] = plano[ic].charCodeAt()-65;
    		ic++;
    	}
    	ic=ic-3;
		for(var i = 1; i <= 3; i++){
			matrizRes[i] = 0;
            for(var j = 1; j <= 3; j++){
               	matrizRes[i] = matrizRes[i] + (matrizCifrado[i][j] * matrizPlano[j]);
               	matrizRes[i] = matrizRes[i]%26;
            }
    	}
	    for (var i = 1; i <= 3; i++) {
    		cifrado[ic] = String.fromCharCode(matrizRes[i]+65);
    		ic++;
    	}
    	ic=ic-3;
    	$("#mrtp1ch").html(cifrado[ic]);
    	$("#mrtp2ch").html(cifrado[ic+1]);
    	$("#mrtp3ch").html(cifrado[ic+2]);
	    $("#abcCifrado"+ic+"C").html(cifrado[ic]);
    	$("#abcCifrado"+(ic+1)+"C").html(cifrado[ic+1]);
    	$("#abcCifrado"+(ic+2)+"C").html(cifrado[ic+2]);
    	// ANIMATION
    	$("#abcPlano"+ic+"C").addClass('parpadeo');
    	$("#abcPlano"+(ic+1)+"C").addClass('parpadeo');
    	$("#abcPlano"+(ic+2)+"C").addClass('parpadeo');
    	$("#mtp1ch").addClass("parpadeo");
    	$("#mtp2ch").addClass("parpadeo");
    	$("#mtp3ch").addClass("parpadeo");
    	$("#mrtp1ch").addClass("parpadeoNext");
    	$("#mrtp2ch").addClass("parpadeoNext");
    	$("#mrtp3ch").addClass("parpadeoNext");
    	$("#abcCifrado"+ic+"C").addClass('parpadeoNext');
    	$("#abcCifrado"+(ic+1)+"C").addClass('parpadeoNext');
    	$("#abcCifrado"+(ic+2)+"C").addClass('parpadeoNext');
    	await sleep(tCAdd);
    	$("#abcPlano"+ic+"C").removeClass('parpadeo');
    	$("#abcPlano"+(ic+1)+"C").removeClass('parpadeo');
    	$("#abcPlano"+(ic+2)+"C").removeClass('parpadeo');
    	$("#mtp1ch").removeClass("parpadeo");
    	$("#mtp2ch").removeClass("parpadeo");
    	$("#mtp3ch").removeClass("parpadeo");
    	$("#mrtp1ch").removeClass("parpadeoNext");
    	$("#mrtp2ch").removeClass("parpadeoNext");
    	$("#mrtp3ch").removeClass("parpadeoNext");
    	$("#abcCifrado"+ic+"C").removeClass('parpadeoNext');
    	$("#abcCifrado"+(ic+1)+"C").removeClass('parpadeoNext');
    	$("#abcCifrado"+(ic+2)+"C").removeClass('parpadeoNext');
    	await sleep(tCRemove);
    	// END ANIMATION
    	ic=ic+3;
    }
    if (ic <= plano.length && ic!=999) {
        cadenaCifrado = cifrado.join("");
        $("#out-txtCifradoHill").val(cadenaCifrado);
        toastr.options.timeOut = "1000";
    	toastr['success']('Texto cifrado');
        $("#btn-cifrarHill").show();
        $("#btn-tipoCiHill").show();
        $("#btn-cancelarCifrarHill").hide();
    }
}

async function descifrarHill(){
    var cifrado = ($("#in-txtCifradoHill").val().toUpperCase()).split("");
    var plano = [];
    var cadenaDescifrado;
    var matriz = [18,23,21,5,23,1,3,15,16];
    var matrizCifrado = [];
    var matrizRes = [];
    var k = 0;
    idc = 0;
    for (var i = 0; i <= cifrado.length-1; i++) {
        if(cifrado[i] == ' ') {
           cifrado.splice(i, 1);
        }
    }
    var matrizDescifrado = new Array(3);
    if (cifrado.length % 3 == 1) {
        cifrado[cifrado.length]='X';
        cifrado[cifrado.length]='X';
    } else if (cifrado.length % 3 == 2) {
        cifrado[cifrado.length]='X';
    }
    $("#infoAnimacionDeHill").fadeIn();
    await sleep(tCAdd);
	for (var i = 1; i <= 3; i++) {
		matrizDescifrado[i] = new Array(3);
	}
	for (var i = 1; i <=3; i++) {
    	for (var j = 1; j <= 3; j++) {
    		matrizDescifrado[i][j]=matriz[k];
    		k++;
    	}
    }
	for (var i = 1; i <=3; i++) {
    	for (var j = 1; j <= 3; j++) {
    		$("#m"+i+j+"dh").html(matrizDescifrado[i][j]);
    	}
    }
    for (var m = 0; m <= cifrado.length-1; m++) {
		$("#textoCifradoHillD").append('<label id="abcCifrado'+m+'D" class="circulo">'+cifrado[m]+'</label>');
    }
    for (var n = 0; n <= cifrado.length-1; n++) {
		$("#textoPlanoHillD").append('<label id="abcPlano'+n+'D" class="circulo">&nbsp;</label>');
    }
    // ANIMATION
    $("#textoCifradoHillD").addClass('parpadeo');
    await sleep(tCAdd);
    $("#textoCifradoHillD").removeClass('parpadeo');
    await sleep(tCRemove);
    // END ANIMATION
    while (idc <= cifrado.length-1) {
    	for (var i = 1; i <= 3; i++) {
    		$("#mtc"+i+"dh").html(cifrado[idc]);
    		matrizCifrado[i] = cifrado[idc].charCodeAt()-65;
    		idc++;
    	}
    	idc=idc-3;
		for(var i = 1; i <= 3; i++){
			matrizRes[i] = 0;
            for(var j = 1; j <= 3; j++){
               	matrizRes[i] = matrizRes[i] + (matrizDescifrado[i][j] * matrizCifrado[j]);
               	matrizRes[i] = matrizRes[i]%26;
            }
    	}
	    for (var i = 1; i <= 3; i++) {
    		plano[idc] = String.fromCharCode(matrizRes[i]+65).toLowerCase();
    		idc++;
    	}
    	idc=idc-3;
    	$("#mrtc1dh").html(plano[idc]);
    	$("#mrtc2dh").html(plano[idc+1]);
    	$("#mrtc3dh").html(plano[idc+2]);
	    $("#abcPlano"+idc+"D").html(plano[idc]);
    	$("#abcPlano"+(idc+1)+"D").html(plano[idc+1]);
    	$("#abcPlano"+(idc+2)+"D").html(plano[idc+2]);
    	// ANIMATION
    	$("#abcCifrado"+idc+"D").addClass('parpadeo');
    	$("#abcCifrado"+(idc+1)+"D").addClass('parpadeo');
    	$("#abcCifrado"+(idc+2)+"D").addClass('parpadeo');
    	$("#mtc1dh").addClass("parpadeo");
    	$("#mtc2dh").addClass("parpadeo");
    	$("#mtc3dh").addClass("parpadeo");
    	$("#mrtc1dh").addClass("parpadeoNext");
    	$("#mrtc2dh").addClass("parpadeoNext");
    	$("#mrtc3dh").addClass("parpadeoNext");
    	$("#abcPlano"+idc+"D").addClass('parpadeoNext');
    	$("#abcPlano"+(idc+1)+"D").addClass('parpadeoNext');
    	$("#abcPlano"+(idc+2)+"D").addClass('parpadeoNext');
    	await sleep(tCAdd);
    	$("#abcCifrado"+idc+"D").removeClass('parpadeo');
    	$("#abcCifrado"+(idc+1)+"D").removeClass('parpadeo');
    	$("#abcCifrado"+(idc+2)+"D").removeClass('parpadeo');
    	$("#mtc1dh").removeClass("parpadeo");
    	$("#mtc2dh").removeClass("parpadeo");
    	$("#mtc3dh").removeClass("parpadeo");
    	$("#mrtc1dh").removeClass("parpadeoNext");
    	$("#mrtc2dh").removeClass("parpadeoNext");
    	$("#mrtc3dh").removeClass("parpadeoNext");
    	$("#abcPlano"+idc+"D").removeClass('parpadeoNext');
    	$("#abcPlano"+(idc+1)+"D").removeClass('parpadeoNext');
    	$("#abcPlano"+(idc+2)+"D").removeClass('parpadeoNext');
    	await sleep(tCRemove);
    	// END ANIMATION
    	idc=idc+3;
    }
    if (idc <= cifrado.length && idc!=999) {
        if (plano[plano.length-1]=='x' && plano[plano.length-2]=='x') {
            plano[plano.length-1]='';
            plano[plano.length-2]='';
            $("#infoAnimacionDeHillExtra").show();
        } else if (plano[plano.length-1]=='x') {
            plano[plano.length-1]='';
            $("#infoAnimacionDeHillExtra").show();
        }
        cadenaDescifrado = plano.join("");
        $("#out-txtPlanoHill").val(cadenaDescifrado);
        $("#btn-copiarTextoHill").removeAttr("disabled");
        toastr.options.timeOut = "1000";
    	toastr['success']('Texto descifrado');
        $("#btn-descifrarHill").show();
        $("#btn-tipoDeHill").show();
        $("#btn-cancelarDescifrarHill").hide();
    }
}

function validarEntradaCifradoHill(){
    var mensaje = "";
    var texto = $('#in-txtPlanoHill').val();
    if (texto.length < 1 || texto.length > 21) {
        mensaje = "El mensaje claro debe contener entre 1 y 21 caracteres.";
    } else if (!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaDescifradoHill(){
    var mensaje = "";
    var texto = $('#in-txtCifradoHill').val();
    if (texto.length < 1 || texto.length > 21) {
        mensaje = "El criptograma debe contener entre 1 y 21 caracteres.";
    } else if (!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$("#btn-mostrarPanelHill").click(function(){
		mostrarPanelHill();
	});
	$("#btn-cerrarPanelHill").click(function(){
        pararAnimacionHill();
		cerrarPanelHill();
	});
    $("#btn-teoriaHill").click(function(){
        pararAnimacionHill();
    });
    $("#btn-fundamentosHill").click(function(){
        pararAnimacionHill();
    });
    $("#btn-animacionCifradoHill").click(function(){
        pararAnimacionHill();
    });
    $("#btn-animacionDescifradoHill").click(function(){
        pararAnimacionHill();
    });
    $("#btn-cancelarCifrarHill").click(function(){
        pararAnimacionHill();
    });
    $("#btn-cancelarDescifrarHill").click(function(){
        pararAnimacionHill();
    });
    $("#tipoCiHill1").click(function(){
        $("#btn-cifrarHill").html('Cifrado Rápido');
        $("#btn-cifrarHill").val(1);
    });
    $("#tipoCiHill2").click(function(){
        $("#btn-cifrarHill").html('Cifrado Normal');
        $("#btn-cifrarHill").val(2);
    });
    $("#tipoCiHill3").click(function(){
        $("#btn-cifrarHill").html('Cifrado Lento');
        $("#btn-cifrarHill").val(3);
    });
    $("#tipoDeHill1").click(function(){
        $("#btn-descifrarHill").html('Descifrado Rápido');
        $("#btn-descifrarHill").val(1);
    });
    $("#tipoDeHill2").click(function(){
        $("#btn-descifrarHill").html('Descifrado Normal');
        $("#btn-descifrarHill").val(2);
    });
    $("#tipoDeHill3").click(function(){
        $("#btn-descifrarHill").html('Descifrado Lento');
        $("#btn-descifrarHill").val(3);
    });

    $("#in-txtPlanoHill").change(function(){
        $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoHill-error").remove();
        if ($("#in-txtPlanoHill").val()=='') {
            $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
            $("#txtPlanoHill-error").remove();
        } else{
            var mensaje = validarEntradaCifradoHill();
            if (mensaje.length == 0){
                $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
                $("#txtPlanoHill-error").remove();
            } else {
                $("#in-txtPlanoHill").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoHill").parent().parent().append('<div id="txtPlanoHill-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-txtCifradoHill").change(function(){
        $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoHill-error").remove();
        if ($("#in-txtCifradoHill").val()=='') {
            $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
            $("#txtCifradoHill-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoHill();
            if (mensaje.length == 0){
                $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
                $("#txtCifradoHill-error").remove();
            } else {
                $("#in-txtCifradoHill").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoHill").parent().parent().append('<div id="txtCifradoHill-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

	$("#btn-cifrarHill").click(function(){
        $("#in-txtPlanoHill").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoHill-error").remove();
        $("#out-txtCifradoHill").val("");
        var mensaje = validarEntradaCifradoHill();
		if ($("#in-txtPlanoHill").val()!='' && mensaje.length == 0){
			$("#textoPlanoHillC").empty();
			$("#textoCifradoHillC").empty();
			$("#infoAnimacionCiHill").hide();
            $("#infoAnimacionCiHillExtra").hide();
            $("#infoLlaveAnimacionCiHill").hide();
            $("#btn-cifrarHill").hide();
            $("#btn-tipoCiHill").hide();
            $("#btn-cancelarCifrarHill").show();
            limpiaMatricesHill();
            crearPanelHill();
			cifrarHill();
		} else{
            $("#in-txtPlanoHill").parent().parent().addClass('has-error has-feedback');
            $("#in-txtPlanoHill").parent().parent().append('<div id="txtPlanoHill-error" class="text-danger">&nbsp;'+mensaje+'</div>');
        }
	});

	$("#btn-copiarTextoHill").click(function(){
		if ($("#out-txtCifradoHill").val()==''){
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		} else {
			$("#in-txtCifradoHill").val($("#out-txtCifradoHill").val());
		}
	});

	$("#btn-descifrarHill").click(function(){
        $("#in-txtCifradoHill").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoHill-error").remove();
        $("#out-txtPlanoHill").val("");
        var mensaje = validarEntradaDescifradoHill();
		if ($("#in-txtCifradoHill").val()!='' && mensaje.length == 0){
			$("#textoPlanoHillD").empty();
			$("#textoCifradoHillD").empty();
			$("#btn-copiarTextoHill").attr("disabled","disabled");
            $("#infoAnimacionDeHill").hide();
            $("#infoAnimacionDeHillExtra").hide();
            $("#btn-descifrarHill").hide();
            $("#btn-tipoDeHill").hide();
            $("#btn-cancelarDescifrarHill").show();
            limpiaMatricesHill();
            crearPanelHill();
			descifrarHill();
		} else{
            $("#in-txtCifradoHill").parent().parent().addClass('has-error has-feedback');
            $("#in-txtCifradoHill").parent().parent().append('<div id="txtCifradoHill-error" class="text-danger">&nbsp;'+mensaje+'</div>');
        }
	});
	    
});