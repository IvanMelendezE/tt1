var cancelado = false;
var velocidad = 1;
var azul = 0, negro = 1;

$.fn.scrollView = function () {
  return this.each(function () {
    $('html, body').animate({
      scrollTop: $(this).offset().top
    }, 1000);
  });
}

function mostrarPanelTransposicionSimple(){
    $("#pnl-Interactivo4").slideToggle(1000);
    $("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelTransposicionSimple(){
    $("#pnl-Interactivo4").slideToggle(1000);
    $("#contenidoPagina").slideToggle(1000);
    
    limpiaPanelCifradoTransposicionSimple();
    limpiaPanelDescifradoTransposicionSimple();
}

function crearPanelCifradoTransposicionSimple(){
    var criptograma_length = $('#in-textoPlanoTransposicionSimple').val().length;

    for(var i = 0 ; i < criptograma_length ; i++){
        $('#textoCifradoSimple').append('<label class="circulo" id="TS-Ccell1'+i+'"></label>');
    }

    $("#table-transposicionSimple").css("text-align","center");
}

function crearPanelDescifradoTransposicionSimple(){
    var mensaje_claro_length = $('#in-textoPlanoCifradoTransposicionSimple').val().length;

    for(var i = 0 ; i < mensaje_claro_length ; i++){
        $('#textoDescifradoSimple').append('<label class="circulo" id="TS-MCcell1'+i+'"></label>');
    }

    $("#table-transposicionSimple2").css("text-align","center");
}

function limpiaPanelCifradoTransposicionSimple(){
    $("#TSrow1").empty();
    $("#TSrow2").empty();
    $('#textoCifradoSimple').empty();
    $("#in-textoPlanoTransposicionSimple").val("");
    $("#out-textoCifradoTransposicionSimple").val("");

    if($('#TSdiv1').is(':visible')) {
        $("#TSdiv1").slideToggle(1000);
    }
}

function limpiaPanelDescifradoTransposicionSimple(){
    $("#TSrow12").empty();
    $("#TSrow22").empty();
    $('#textoDescifradoSimple').empty();
    $("#in-textoPlanoCifradoTransposicionSimple").val("");
    $("#out-textoDescifradoTransposicionSimple").val("");

    if($('#TSdiv2').is(':visible')) {
        $("#TSdiv2").slideToggle(1000);
    }
}

async function cifrarTransposicionSimple(){
    var plano = ($("#in-textoPlanoTransposicionSimple").val().toLowerCase().replace(/ /g,"")).split("");
    var cadenaCifrado = "";
    var j = 1, k = 1, l = 0;
    
    limpiaPanelCifradoTransposicionSimple();
    $("#in-textoPlanoTransposicionSimple").val(plano.join(""));

    crearPanelCifradoTransposicionSimple();

    $('#TSdiv1').html('El mensaje en claro se reescribe en dos renglones: la primera letra en el primer renglón, la segunda en el segundo renglón, la tercera en el primer renglón, la cuarta en el segundo renglón y así uno y uno hasta acabar con todos los caracteres del mensaje claro.');
    $('#TSdiv1').slideToggle(1000);

    if(cancelado){
        return;
    }

    await sleep(8000);
    
    if(cancelado){
        return;
    }

    $('#TSdiv1').scrollView();

    $("#TSrow1").append('<td id="TSR1-0" class="title-table">R1</td>');
    $("#TSrow2").append('<td id="TSR2-0" class="title-table">R2</td>');

    for (var i = 1; i < 6 && !cancelado ; i++) {
        $("#TSrow1").append('<td id="TSR1-'+i+'"> </td>');
        $("#TSrow2").append('<td id="TSR2-'+i+'"> </td>');
    }

    if(cancelado){
        return;
    }

    for (var i = 0 ; i < plano.length && !cancelado ; i++) {
        if(i%2 == 0){
            $("#TSR1-" + j).html(plano[i]);
            parpadeo("#TSR1-" + j, 1*velocidad, azul);
            await sleep(1000*velocidad);
            
            removeparpadeo("#TSR1-" + j, 1*velocidad, azul);

            j++;
        }
        else if(i%2 == 1){
            $("#TSR2-"+ k).html(plano[i]);
            parpadeo("#TSR2-" + k, 1*velocidad, azul);
            await sleep(1000*velocidad);
            
            removeparpadeo("#TSR2-" + k, 1*velocidad, azul);

            k++;
        }
    }

    if(cancelado){
        return;
    }

    $('#TSdiv1').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TSdiv1').html('Se reescribe el mensaje por renglones: primero R1 seguido de R2.');
    $('#TSdiv1').slideToggle(1000);

    if(cancelado){
        return;
    }

    await sleep(4000);
    
    if(cancelado){
        return;
    }

    $('#TSdiv1').scrollView();

    for (var i = 1 ; i < j && !cancelado ; i++, l++){
        cadenaCifrado = cadenaCifrado + plano[(i-1)*2].toUpperCase();
        
        parpadeo("#TSR1-" + i, 1*velocidad, azul);
        parpadeo("#TS-Ccell1"+l, 1*velocidad, negro);
        $("#TS-Ccell1"+l).html(plano[(i-1)*2].toUpperCase());
        await sleep(1000*velocidad);
        
        removeparpadeo("#TSR1-" + i, 1*velocidad, azul);
        removeparpadeo("#TS-Ccell1"+l, 1*velocidad, negro);
    }

    for (var i = 1 ; i < k && !cancelado ; i++, l++){
        cadenaCifrado = cadenaCifrado + plano[i*2 - 1].toUpperCase();

        parpadeo("#TSR2-" + i, 1*velocidad, azul);
        parpadeo("#TS-Ccell1"+l, 1*velocidad, negro);
        $("#TS-Ccell1"+l).html(plano[i*2 - 1].toUpperCase());
        await sleep(1000*velocidad);
        
        removeparpadeo("#TSR2-" + i, 1*velocidad, azul);
        removeparpadeo("#TS-Ccell1"+l, 1*velocidad, negro);
    }

    if(cancelado){
        return;
    }

    $("#out-textoCifradoTransposicionSimple").val(cadenaCifrado);
    $("#btn-velocidadCTransposicionSimple").show();
    $("#btn-cifrarSimple").show();
    $("#btn-cancelarCifrarSimple").hide();

    if(!cancelado){
        $('#TSdiv1').slideToggle(1000);
        toastr.options.timeOut = "1000";
        toastr['success']('Texto cifrado');
        cancelado = true;
    }
}

async function descifrarTransposicionSimple(){
    var cifrado = ($("#in-textoPlanoCifradoTransposicionSimple").val().toUpperCase().replace(/ /g,"")).split("");
    var cadenaDescifrado = "";
    var j = 1, k = 1, l = 0, m = 0;

    limpiaPanelDescifradoTransposicionSimple();
    $("#in-textoPlanoCifradoTransposicionSimple").val(cifrado.join(""));

    crearPanelDescifradoTransposicionSimple();
    
    $('#TSdiv2').html('Se toma la primera mitad del criptograma y se coloca en R1, la última mitad se coloca en R2.');
    $('#TSdiv2').slideToggle(1000);

    if(cancelado){
        return;
    }

    await sleep(5000);
    
    if(cancelado){
        return;
    }

    $('#TSdiv2').scrollView();

    $("#TSrow12").append('<td id="TSR1-20" class="title-table">R1</td>');
    $("#TSrow22").append('<td id="TSR2-20" class="title-table">R2</td>');

    for (var i = 1; i < 6 && !cancelado ; i++) {
        $("#TSrow12").append('<td id="TSR1-2'+i+'"> </td>');
        $("#TSrow22").append('<td id="TSR2-2'+i+'"> </td>');
    }

    if(cancelado){
        return;
    }

    for (var i = 0 ; i < cifrado.length && !cancelado ; i++) {
        if(i < Math.round(cifrado.length / 2)){
            $("#TSR1-2"+j).html(cifrado[i]);

            parpadeo("#TSR1-2"+j, 1*velocidad, azul);
            await sleep(1000*velocidad);

            removeparpadeo("#TSR1-2"+(j++), 1*velocidad, azul);
        }
        else{
            $("#TSR2-2"+k).html(cifrado[i]);

            parpadeo("#TSR2-2"+k, 1*velocidad, azul);
            await sleep(1000*velocidad);

            removeparpadeo("#TSR2-2"+(k++), 1*velocidad, azul);
        }
    }

    if(cancelado){
        return;
    }

    j = 1;
    k = 1;
    
    $('#TSdiv2').slideToggle(1000);
    
    if(cancelado){
        return;
    }

    await sleep(1000);

    if(cancelado){
        return;
    }

    $('#TSdiv2').html('El mensaje en claro se conforma tomando uno y uno de los caracteres de cada renglón: la primera letra de R1, luego la primera letra de R2, luego la segunda de R1, luego la segunda de R2 y así sucesivamente.');
    $('#TSdiv2').slideToggle(1000);

    if(cancelado){
        return;
    }

    await sleep(8000);
    
    if(cancelado){
        return;
    }

    $('#TSdiv2').scrollView();

    for (var i = 0 ; i < cifrado.length && !cancelado; i++) {
        if(i%2 == 0){
            cadenaDescifrado = cadenaDescifrado + cifrado[l].toLowerCase();

            parpadeo("#TSR1-2" + j, 1*velocidad, azul);
            parpadeo("#TS-MCcell1"+i, 1*velocidad, negro);
            $("#TS-MCcell1"+i).html(cifrado[l].toLowerCase());
            await sleep(1000*velocidad);
            
            removeparpadeo("#TSR1-2" + j++, 1*velocidad, azul);
            removeparpadeo("#TS-MCcell1"+i, 1*velocidad, negro);

            l = l + Math.round(cifrado.length/2);
        }
        else if(i%2 == 1){
            cadenaDescifrado = cadenaDescifrado + cifrado[l].toLowerCase();

            parpadeo("#TSR2-2" + k, 1*velocidad, azul);
            parpadeo("#TS-MCcell1"+i, 1*velocidad, negro);
            $("#TS-MCcell1"+i).html(cifrado[l].toLowerCase());
            await sleep(1000*velocidad);
            
            removeparpadeo("#TSR2-2" + k++, 1*velocidad, azul);
            removeparpadeo("#TS-MCcell1"+i, 1*velocidad, negro);

            l = l - Math.round(cifrado.length/2) + 1;
        }
    }

    if(cancelado){
        return;
    }

    $("#out-textoDescifradoTransposicionSimple").val(cadenaDescifrado);
    $("#btn-velocidadDTransposicionSimple").show();
    $("#btn-descifrarSimple").show();
    $("#btn-cancelarDescifrarSimple").hide();

    if(!cancelado){
        $('#TSdiv2').slideToggle(1000);
        toastr.options.timeOut = "1000";
        toastr['success']('Texto cifrado');
        cancelado = true;
    }
}

function validarEntradaCifradoTransposicionSimple(){
    var mensaje = "";
    var texto = $('#in-textoPlanoTransposicionSimple').val().replace(/ /g,"");

    if (texto.length < 1 || texto.length > 10) {
        mensaje = "El mensaje claro debe contener entre 1 y 10 caracteres.";
    }
    else if(!texto.match(/^[a-zA-Z]+$/)){
        mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }

    return mensaje;
}

function validarEntradaDescifradoTransposicionSimple(){
    var mensaje = "";
    var texto = $('#in-textoPlanoCifradoTransposicionSimple').val();

    if(texto.indexOf(' ') >= 0){
        mensaje = "El criptograma no debe contener espacios.";
    }
    else if (texto.length < 1 || texto.length > 10) {
        mensaje = "El criptograma debe contener entre 1 y 10 caracteres.";
    }
    else if(!texto.match(/^[a-zA-Z]+$/)){
        mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }

    return mensaje;
}

$(document).ready(function(){
    $("#tipoTransposicionSimpleC1").click(function(){
        $("#btn-cifrarSimple").html('Cifrado Rápido');
        $("#btn-cifrarSimple").val(1);
    });
    $("#tipoTransposicionSimpleC2").click(function(){
        $("#btn-cifrarSimple").html('Cifrado Normal');
        $("#btn-cifrarSimple").val(2);
    });
    $("#tipoTransposicionSimpleC3").click(function(){
        $("#btn-cifrarSimple").html('Cifrado Lento&nbsp;');
        $("#btn-cifrarSimple").val(3);
    });

    $("#tipoTransposicionSimpleD1").click(function(){
        $("#btn-descifrarSimple").html('Descifrado Rápido');
        $("#btn-descifrarSimple").val(1);
    });
    $("#tipoTransposicionSimpleD2").click(function(){
        $("#btn-descifrarSimple").html('Descifrado Normal');
        $("#btn-descifrarSimple").val(2);
    });
    $("#tipoTransposicionSimpleD3").click(function(){
        $("#btn-descifrarSimple").html('Descifrado Lento&nbsp;');
        $("#btn-descifrarSimple").val(3);
    });

    $("#in-textoPlanoTransposicionSimple").keyup(function(){
        var mensaje = validarEntradaCifradoTransposicionSimple();

        if (mensaje.length != 0) {
            $("#textoPlanoTransposicionSimple-error").remove();
            $("#in-textoPlanoTransposicionSimple").parent().parent().append('<div id="textoPlanoTransposicionSimple-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoTransposicionSimple").parent().parent().addClass('has-error has-feedback');
            $("#btn-cifrarSimple").attr("disabled", true);
        } else{
            $("#textoPlanoTransposicionSimple-error").remove();
            $("#in-textoPlanoTransposicionSimple").parent().parent().removeClass('has-error has-feedback');
            $("#btn-cifrarSimple").attr("disabled", false);
        }
    });

    $("#in-textoPlanoCifradoTransposicionSimple").keyup(function(){
        var mensaje = validarEntradaDescifradoTransposicionSimple();

        $("#textoPlanoCifradoTransposicionSimple-info").remove();

        if (mensaje.length != 0) {
            $("#textoPlanoCifradoTransposicionSimple-error").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().append('<div id="textoPlanoCifradoTransposicionSimple-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().addClass('has-error has-feedback');
            $("#btn-descifrarSimple").attr("disabled", true);
        } else{
            $("#textoPlanoCifradoTransposicionSimple-error").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().removeClass('has-error has-feedback');
            $("#btn-descifrarSimple").attr("disabled", false);
        }
    });

    $("#btn-cifrarSimple").click(function(){
        var mensaje = validarEntradaCifradoTransposicionSimple();
        
        if(mensaje.length == 0){
            $("#textoPlanoTransposicionSimple-error").remove();
            $("#in-textoPlanoTransposicionSimple").parent().parent().removeClass('has-error has-feedback');
            $("#btn-cifrarSimple").attr("disabled", false);

            if($('#btn-cifrarSimple').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-cifrarSimple').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-velocidadCTransposicionSimple").hide();
            $("#btn-cifrarSimple").hide();
            $("#btn-cancelarCifrarSimple").show();
            cancelado = false;
            
            cifrarTransposicionSimple();   
        }
        else{
            $("#textoPlanoTransposicionSimple-error").remove();
            $("#in-textoPlanoTransposicionSimple").parent().parent().append('<div id="textoPlanoTransposicionSimple-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoTransposicionSimple").parent().parent().addClass('has-error has-feedback');
            $("#btn-cifrarSimple").attr("disabled", true);
        }
    });

    $("#btn-cancelarCifrarSimple").click(function(){
        cancelado = true;

        limpiaPanelCifradoTransposicionSimple();

        $("#btn-cifrarSimple").show();
        $("#btn-velocidadCTransposicionSimple").show();
        $("#btn-cancelarCifrarSimple").hide();
    });

    $("#btn-cancelarDescifrarSimple").click(function(){
        cancelado = true;

        limpiaPanelDescifradoTransposicionSimple();

        $("#btn-descifrarSimple").show();
        $("#btn-velocidadDTransposisionSimple").show();
        $("#btn-cancelarDescifrarSimple").hide();
    });

    $("#btn-copiarTextoSimple").click(function(){
        if ($("#out-textoCifradoTransposicionSimple").val()==''){
            $("#textoPlanoCifradoTransposicionSimple-info").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().append('<div id="textoPlanoCifradoTransposicionSimple-info" class="text-info">Primero debes cifrar un mensaje</div>');
        } else {
            $("#textoPlanoCifradoTransposicionSimple-info").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").val($("#out-textoCifradoTransposicionSimple").val());
        }
    });

    $("#btn-descifrarSimple").click(function(){
        var mensaje = validarEntradaDescifradoTransposicionSimple();
        
        if(mensaje.length == 0){
            $("#textoPlanoCifradoTransposicionSimple-error").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().removeClass('has-error has-feedback');
            $("#btn-descifrarSimple").attr("disabled", false);

            if($('#btn-descifrarSimple').val() == 1) {
                velocidad = 0.5;
            }
            else if($('#btn-descifrarSimple').val() == 2) {
                velocidad = 1;
            }
            else{
                velocidad = 2;
            }

            $("#btn-descifrarSimple").hide();
            $("#btn-velocidadDTransposisionSimple").hide();
            $("#btn-cancelarDescifrarSimple").show();
            cancelado = false;
            
            descifrarTransposicionSimple();
        }
        else{
            $("#textoPlanoCifradoTransposicionSimple-error").remove();
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().append('<div id="textoPlanoCifradoTransposicionSimple-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            $("#in-textoPlanoCifradoTransposicionSimple").parent().parent().addClass('has-error has-feedback');
            $("#btn-descifrarSimple").attr("disabled", true);
        }
    });        
});