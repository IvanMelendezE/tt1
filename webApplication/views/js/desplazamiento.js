function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var ic=0, idc=0;
var tCAdd=800, tDcAdd=800;
var tCRemove=300, tDcRemove=300;

function mostrarPanelDespla(){
	crearPanelDespla();
	$("#pnl-InteractivoDespla").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
}

function cerrarPanelDespla(){
	$("#pnl-InteractivoDespla").slideToggle(1000);
	$("#contenidoPagina").slideToggle(1000);
	limpiaPanelDespla();
}

function crearPanelDespla(){
	for (var i = 65; i <= 90; i++) {
		$("#textoPlanoDesplaC").append('<label id="abcPlano'+i+'C" class="circulo">'+String.fromCharCode(i).toLowerCase()+'&nbsp;</label>');
		$("#textoCifradoDesplaC").append('<label id="abcCifrado'+i+'C" class="circulo">'+String.fromCharCode(i)+'</label>');
		$("#textoPlanoDesplaD").append('<label id="abcPlano'+i+'D" class="circulo">'+String.fromCharCode(i).toLowerCase()+'&nbsp;</label>');
		$("#textoCifradoDesplaD").append('<label id="abcCifrado'+i+'D" class="circulo">'+String.fromCharCode(i)+'</label>');
	}
}

function limpiaPanelDespla(){
	$("#textoPlanoDesplaC").empty();
	$("#textoCifradoDesplaC").empty();
	$("#textoPlanoDesplaD").empty();
	$("#textoCifradoDesplaD").empty();
	$("#in-txtPlanoDespla").val("");
	$("#out-txtCifradoDespla").val("");
	$("#in-txtCifradoDespla").val("");
	$("#out-txtPlanoDespla").val("");
	$("#in-keyDesplaC").val("");
	$("#in-keyDesplaD").val("");
	$("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
    $("#txtPlanoDespla-error").remove();
    $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
    $("#keyDesplaC-error").remove();
    $("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoDespla-error").remove();
    $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
    $("#keyDesplaD-error").remove();
    $("#btn-cifrarDespla").show();
    $("#btn-tipoCiDespla").show();
    $("#btn-cancelarCifrarDespla").hide();
    $("#btn-descifrarDespla").show();
    $("#btn-tipoDeDespla").show();
    $("#btn-cancelarDescifrarDespla").hide();
}

function pararAnimacionDespla(){
	ic=999;
	idc=999;
    $("#btn-copiarTextoDespla").removeAttr("disabled");
    $("#palabraDesplaC").empty();
	$("#palabraDesplaD").empty();
	$("#palabraCifradaDesplaC").empty();
	$("#palabraDescifradaDesplaD").empty();
	$("#infoAnimacionCiDespla").hide();
	$("#infoAnimacionDeDespla").hide();
	$("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
    $("#txtPlanoDespla-error").remove();
    $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
    $("#keyDesplaC-error").remove();
    $("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
    $("#txtCifradoDespla-error").remove();
    $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
    $("#keyDesplaD-error").remove();
    $("#btn-cifrarDespla").show();
    $("#btn-tipoCiDespla").show();
    $("#btn-cancelarCifrarDespla").hide();
    $("#btn-descifrarDespla").show();
    $("#btn-tipoDeDespla").show();
    $("#btn-cancelarDescifrarDespla").hide();
}

async function cifrarDespla(){
    var plano = ($("#in-txtPlanoDespla").val().toUpperCase()).split("");
    var desplazamiento = parseInt($("#in-keyDesplaC").val())%26;
    var cifrado = [];
    var cadenaCifrado;
    ic = 0;
    for (var i = 0; i <= plano.length-1; i++) {
        if(plano[i] == ' ') {
           plano.splice(i, 1);
        }
    }
    $("#infoAnimacionCiDespla").fadeIn();
    await sleep(tCAdd);
    for (var m = 0; m <= plano.length-1; m++) {
		$("#palabraDesplaC").append('<label id="palabra'+m+'C" class="circulo">'+plano[m].toLowerCase()+'</label>');
    }
    for (var n = 0; n <= plano.length-1; n++) {
		$("#palabraCifradaDesplaC").append('<label id="palabraCifrado'+n+'C" class="circulo">&nbsp;</label>');
    }
    while (ic <= plano.length-1) {
		if (plano[ic].charCodeAt()+desplazamiento <= 90){
			cifrado[ic] = String.fromCharCode(plano[ic].charCodeAt()+desplazamiento);
			numeroElementoPlano = plano[ic].charCodeAt();
    		numeroElementoCifrado = plano[ic].charCodeAt()+desplazamiento;
		} else {
			cifrado[ic] = String.fromCharCode((plano[ic].charCodeAt()+desplazamiento)-26);
			numeroElementoPlano = plano[ic].charCodeAt();
    		numeroElementoCifrado = (plano[ic].charCodeAt()+desplazamiento)-26;
		}
		$("#palabraCifrado"+ic+"C").html(cifrado[ic]).show();
    	
    	// ANIMATION
    	$("#infoCiDespla").html('<br><strong>'+cifrado[ic]+'</strong> es la letra <strong>'+plano[ic].toLowerCase()+'</strong> recorrida <strong>'+desplazamiento+'</strong> posiciones');
    	$("#palabra"+ic+"C").addClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"C").addClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"C").addClass('parpadeoNext');
    	$("#palabraCifrado"+ic+"C").addClass('parpadeoNext');
    	await sleep(tCAdd);
    	$("#palabra"+ic+"C").removeClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"C").removeClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"C").removeClass('parpadeoNext');
    	$("#palabraCifrado"+ic+"C").removeClass('parpadeoNext');
    	await sleep(tCRemove);
    	// END ANIMATION
		ic++;
    }
    if (ic <= plano.length && ic!=999) {
	    cadenaCifrado = cifrado.join("");
	    $("#out-txtCifradoDespla").val(cadenaCifrado);
	    toastr.options.timeOut = "1000";
		toastr['success']('Texto cifrado');
        $("#btn-cifrarDespla").show();
        $("#btn-tipoCiDespla").show();
        $("#btn-cancelarCifrarDespla").hide();
	}
}

async function descifrarDespla(){
    var cifrado = ($("#in-txtCifradoDespla").val().toUpperCase()).split("");
    var desplazamiento = parseInt($("#in-keyDesplaD").val())%26;
    var plano = [];
    var cadenaDescifrado;
    for (var i = 0; i <= cifrado.length-1; i++) {
        if(cifrado[i] == ' ') {
           cifrado.splice(i, 1);
        }
    }
    idc = 0;
    $("#infoAnimacionDeDespla").fadeIn();
    await sleep(tCAdd);
    for (var m = 0; m <= cifrado.length-1; m++) {
		$("#palabraDesplaD").append('<label id="palabra'+m+'D" class="circulo">'+cifrado[m]+'</label>');
    }
    for (var n = 0; n <= cifrado.length-1; n++) {
		$("#palabraDescifradaDesplaD").append('<label id="palabraDescifrado'+n+'D" class="circulo">&nbsp;</label>');
    }
    while (idc <= cifrado.length-1) {
		if (cifrado[idc].charCodeAt()-desplazamiento >= 65){
			plano[idc] = String.fromCharCode(cifrado[idc].charCodeAt()-desplazamiento).toLowerCase();
			numeroElementoCifrado = cifrado[idc].charCodeAt();
    		numeroElementoPlano = cifrado[idc].charCodeAt()-desplazamiento;
		} else {
			plano[idc] = String.fromCharCode((cifrado[idc].charCodeAt()-desplazamiento)+26).toLowerCase();
			numeroElementoCifrado = cifrado[idc].charCodeAt();
    		numeroElementoPlano = (cifrado[idc].charCodeAt()-desplazamiento)+26;
		}
		$("#palabraDescifrado"+idc+"D").html(plano[idc]).show();
    	
    	// ANIMATION
    	$("#infoDeDespla").html('<br><strong>'+plano[idc].toLowerCase()+'</strong> es la letra <strong>'+cifrado[idc]+'</strong> recorrida <strong>'+desplazamiento+'</strong> posiciones');
    	$("#palabra"+idc+"D").addClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"D").addClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"D").addClass('parpadeoNext');
    	$("#palabraDescifrado"+idc+"D").addClass('parpadeoNext');
    	await sleep(tDcAdd);
    	$("#palabra"+idc+"D").removeClass('parpadeo');
    	$("#abcCifrado"+numeroElementoCifrado+"D").removeClass('parpadeo');
    	$("#abcPlano"+numeroElementoPlano+"D").removeClass('parpadeoNext');
    	$("#palabraDescifrado"+idc+"D").removeClass('parpadeoNext');
    	await sleep(tDcRemove);
    	// END ANIMATION
		idc++;
    }
    if (idc <= cifrado.length && idc!=999) {
		cadenaDescifrado = plano.join("");
		$("#out-txtPlanoDespla").val(cadenaDescifrado);
		$("#btn-copiarTextoDespla").removeAttr("disabled");
		toastr.options.timeOut = "1000";
		toastr['success']('Texto descifrado');
        $("#btn-descifrarDespla").show();
        $("#btn-tipoDeDespla").show();
        $("#btn-cancelarDescifrarDespla").hide();
	}
}

function validarEntradaCifradoDespla(){
    var mensaje = "";
    var texto = $('#in-txtPlanoDespla').val();
	if (texto.length < 1 || texto.length > 20) {
        mensaje = "El mensaje claro debe contener entre 1 y 20 caracteres.";
    } else if (!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El mensaje claro solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaCifradoDesplaLlave(){
    var mensaje = "";
    var clave = $('#in-keyDesplaC').val();
    if(!clave.match(/^[0-9]+$/)){
        mensaje = "La llave debe ser un numero entero.";
    }
    return mensaje;
}

function validarEntradaDescifradoDespla(){
    var mensaje = "";
    var texto = $('#in-txtCifradoDespla').val();
    if (texto.length < 1 || texto.length > 20) {
        mensaje = "El criptograma debe contener entre 1 y 20 caracteres.";
    } else if(!texto.match(/^[a-zA-Z \s]+$/)){
        mensaje = "El criptograma solo puede contener caracteres de la <strong>a</strong> a la <strong>z</strong>.";
    }
    return mensaje;
}

function validarEntradaDescifradoDesplaLlave(){
    var mensaje = "";
    var clave = $('#in-keyDesplaD').val();
    if(!clave.match(/^[0-9]+$/)){
        mensaje = "La llave debe ser un numero entero.";
    }
    return mensaje;
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$("#btn-mostrarPanelDespla").click(function(){
		mostrarPanelDespla();
	});
	$("#btn-cerrarPanelDespla").click(function(){
		pararAnimacionDespla();
		cerrarPanelDespla();
	});
	$("#btn-teoriaDespla").click(function(){
		pararAnimacionDespla();
	});
	$("#btn-fundamentosDespla").click(function(){
		pararAnimacionDespla();
	});
	$("#btn-animacionCifradoDespla").click(function(){
		pararAnimacionDespla();
	});
	$("#btn-animacionDesifradoDespla").click(function(){
		pararAnimacionDespla();
	});
	$("#btn-cancelarCifrarDespla").click(function(){
        pararAnimacionDespla();
    });
    $("#btn-cancelarDescifrarDespla").click(function(){
        pararAnimacionDespla();
    });
    $("#tipoCiDespla1").click(function(){
        $("#btn-cifrarDespla").html('Cifrado Rápido');
        $("#btn-cifrarDespla").val(1);
    });
    $("#tipoCiDespla2").click(function(){
        $("#btn-cifrarDespla").html('Cifrado Normal');
        $("#btn-cifrarDespla").val(2);
    });
    $("#tipoCiDespla3").click(function(){
        $("#btn-cifrarDespla").html('Cifrado Lento');
        $("#btn-cifrarDespla").val(3);
    });
    $("#tipoDeDespla1").click(function(){
        $("#btn-descifrarDespla").html('Descifrado Rápido');
        $("#btn-descifrarDespla").val(1);
    });
    $("#tipoDeDespla2").click(function(){
        $("#btn-descifrarDespla").html('Descifrado Normal');
        $("#btn-descifrarDespla").val(2);
    });
    $("#tipoDeDespla3").click(function(){
        $("#btn-descifrarDespla").html('Descifrado Lento');
        $("#btn-descifrarDespla").val(3);
    });

    $("#in-txtPlanoDespla").change(function(){
        $("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoDespla-error").remove();
        if ($("#in-txtPlanoDespla").val()=='') {
            $("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
            $("#txtPlanoDespla-error").remove();
        } else{
            var mensaje = validarEntradaCifradoDespla();
            if (mensaje.length == 0){
                $("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
                $("#txtPlanoDespla-error").remove();
            } else {
                $("#in-txtPlanoDespla").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoDespla").parent().parent().append('<div id="txtPlanoDespla-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-keyDesplaC").change(function(){
        $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
        $("#keyDesplaC-error").remove();
        if ($("#in-keyDesplaC").val()=='') {
            $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
            $("#keyDesplaC-error").remove();
        } else{
            var mensaje = validarEntradaCifradoDesplaLlave();
            if (mensaje.length == 0){
                $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
                $("#keyDesplaC-error").remove();
            } else {
                $("#in-keyDesplaC").parent().parent().addClass('has-error has-feedback');
                $("#in-keyDesplaC").parent().parent().append('<div id="keyDesplaC-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-txtCifradoDespla").change(function(){
        $("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoDespla-error").remove();
        if ($("#in-txtCifradoDespla").val()=='') {
            $("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
            $("#txtCifradoDespla-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoDespla();
            if (mensaje.length == 0){
                $("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
                $("#txtCifradoDespla-error").remove();
            } else {
                $("#in-txtCifradoDespla").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoDespla").parent().parent().append('<div id="txtCifradoDespla-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

    $("#in-keyDesplaD").change(function(){
        $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
        $("#keyDesplaD-error").remove();
        if ($("#in-keyDesplaD").val()=='') {
            $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
            $("#keyDesplaD-error").remove();
        } else{
            var mensaje = validarEntradaDescifradoDesplaLlave();
            if (mensaje.length == 0){
                $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
                $("#keyDesplaD-error").remove();
            } else {
                $("#in-keyDesplaD").parent().parent().addClass('has-error has-feedback');
                $("#in-keyDesplaD").parent().parent().append('<div id="keyDesplaD-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
        }
    });

	$("#btn-cifrarDespla").click(function(){
		$("#in-txtPlanoDespla").parent().parent().removeClass('has-error has-feedback');
        $("#txtPlanoDespla-error").remove();
        $("#in-keyDesplaC").parent().parent().removeClass('has-error has-feedback');
        $("#keyDesplaC-error").remove();
		$("#out-txtCifradoDespla").val("");
		var mensaje = validarEntradaCifradoDespla();
		var llave = validarEntradaCifradoDesplaLlave();
		if ($("#in-txtPlanoDespla").val()!='' && $("#in-keyDesplaC").val()!='' && mensaje.length == 0 && llave.length == 0){
			$("#palabraDesplaC").empty();
			$("#palabraCifradaDesplaC").empty();
			$("#infoAnimacionCiDespla").hide();
			$("#infoCiDespla").html('');
			$("#btn-cifrarDespla").hide();
            $("#btn-tipoCiDespla").hide();
            $("#btn-cancelarCifrarDespla").show();
			cifrarDespla();
		} else{
			if (mensaje.length != 0) {
                $("#in-txtPlanoDespla").parent().parent().addClass('has-error has-feedback');
                $("#in-txtPlanoDespla").parent().parent().append('<div id="txtPlanoDespla-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
            if (llave.length != 0) {
                $("#in-keyDesplaC").parent().parent().append('<div id="keyDesplaC-error" class="text-danger">&nbsp;'+llave+'</div>');
                $("#in-keyDesplaC").parent().parent().addClass('has-error has-feedback');
            }
		}
	});

	$("#btn-copiarTextoDespla").click(function(){
		if ($("#out-txtCifradoDespla").val()==''){
			toastr.options.timeOut = "1500";
			toastr.options.closeButton = true;
			toastr['info']('Primero debes cifrar un mensaje');
		} else {
			$("#in-txtCifradoDespla").val($("#out-txtCifradoDespla").val());
			$("#in-keyDesplaD").val($("#in-keyDesplaC").val());
		}
	});

	$("#btn-descifrarDespla").click(function(){
		$("#in-txtCifradoDespla").parent().parent().removeClass('has-error has-feedback');
        $("#txtCifradoDespla-error").remove();
        $("#in-keyDesplaD").parent().parent().removeClass('has-error has-feedback');
        $("#keyDesplaD-error").remove();
		$("#out-txtPlanoDespla").val("");
		var mensaje = validarEntradaDescifradoDespla();
		var llave = validarEntradaDescifradoDesplaLlave();
		if ($("#in-txtCifradoDespla").val()!='' && $("#in-keyDesplaD").val()!='' && mensaje.length == 0 && llave.length == 0){
			$("#palabraDesplaD").empty();
			$("#palabraDescifradaDesplaD").empty();
			$("#btn-copiarTextoDespla").attr("disabled","disabled");
			$("#infoAnimacionDeDespla").hide();
			$("#infoDeDespla").html('');
			$("#btn-descifrarDespla").hide();
            $("#btn-tipoDeDespla").hide();
            $("#btn-cancelarDescifrarDespla").show();
			descifrarDespla();
		} else{
			if (mensaje.length != 0) {
                $("#in-txtCifradoDespla").parent().parent().addClass('has-error has-feedback');
                $("#in-txtCifradoDespla").parent().parent().append('<div id="txtCifradoDespla-error" class="text-danger">&nbsp;'+mensaje+'</div>');
            }
            if (llave.length != 0) {
                $("#in-keyDesplaD").parent().parent().addClass('has-error has-feedback');
                $("#in-keyDesplaD").parent().parent().append('<div id="keyDesplaD-error" class="text-danger">&nbsp;'+llave+'</div>');
            }
		}
	});
	    
});